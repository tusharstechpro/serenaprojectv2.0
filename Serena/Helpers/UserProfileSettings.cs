﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Serena.Helpers
{
  public static  class UserProfileSettings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string ProfileSettingsKey = "ProfileSettings_key";
        private static readonly string ProfileSettingsDefault = string.Empty;

        #endregion


        public static string ProfileSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(ProfileSettingsKey, ProfileSettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ProfileSettingsKey, value);
            }
        }
    }
}
