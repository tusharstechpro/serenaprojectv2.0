﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Serena.Helpers
{
    public class MyStackLayout : StackLayout
    {
        public delegate void DrawImageHandler(Action<byte[]> action);
        public DrawImageHandler OnDrawing;
    }
}
