﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Serena.Helpers
{
    public static class SegmentSession
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }


        #region Setting Constants

        private const string SegmentSessionKey = "segmentSession_key";
        private static readonly string SegmentSessionDefault = string.Empty;

        #endregion

        public static string segmentsessionValue
        {
            get
            {
                return AppSettings.GetValueOrDefault(SegmentSessionKey, SegmentSessionDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SegmentSessionKey, value);
            }
        }
    }
}
