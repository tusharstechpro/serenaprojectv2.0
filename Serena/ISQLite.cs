﻿using SQLite;
namespace Serena
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
