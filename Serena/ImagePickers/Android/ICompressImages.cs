﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serena.ImagePickers.Android
{
    public interface ICompressImages
    {
        string CompressImage(string path);
    }
}
