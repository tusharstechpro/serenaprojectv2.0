﻿using SQLite;
using System;

namespace Serena.Model
{
    public class SerenaReviews
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string UserName { get; set; }
        public string Review { get; set; }
        public double Rating { get; set; }
        public string DateAdded { get; set; }
    }

    public class SerenaReviews_Out
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string UserName { get; set; }
        public string Review { get; set; }
        public double Rating { get; set; }
        public string DateAdded { get; set; }
        public DateTime LastSyncDate { get; set; }
    }

}
