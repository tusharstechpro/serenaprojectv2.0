﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Json;
using System.Text;

namespace Serena.Model
{
    public class Topics
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string TopicName { get; set; }
    }
}
