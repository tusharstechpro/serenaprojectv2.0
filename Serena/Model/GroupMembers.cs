﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serena.Model
{
  public  class GroupMembers
    {
        public string SuggestedUserEmailId { get; set; }
        public string UserName { get; set; }
        public string UserProfileImage { get; set; }
    }

    public class ScheduleDateTime
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Date { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public string Title { get; set; }
    }
}
