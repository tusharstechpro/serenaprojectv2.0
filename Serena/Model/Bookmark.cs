﻿using SQLite;
using System;

namespace Serena.Model
{
    public class Bookmark
    {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string UserEmailId { get; set; }
            public string Guid { get; set; }
            public string Category { get; set; }
            public string Comment { get; set; }
            public DateTime DateCreated { get; set; }

    }
}
