﻿using SQLite;
using System;
using System.ComponentModel;

namespace Serena.Model
{
    [Table("Serena_UserProfileData")]
    public class UserProfileData
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserName { get; set; }
        //Social Profile Fields
        [DefaultValue(0)]
        public int IsPro { get; set; }
        [DefaultValue("NO")]
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public DateTime LastUpdated { get; set; }
    }

    public class UserProfileData_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserName { get; set; }
        //Social Profile Fields
        [DefaultValue(0)]
        public int IsPro { get; set; }
        [DefaultValue("NO")]
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string Status { get; set; }//0-save, 1-Edit
    }
    public class ProMemberToolsModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string Categories { get; set; }
        public string Tagline { get; set; }
        public string About { get; set; }
        public string Coverphoto { get; set; }
    }
}
