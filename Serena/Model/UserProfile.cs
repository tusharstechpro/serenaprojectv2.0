﻿using SQLite;
using System;

namespace Serena.Model
{
    [Table("Serena_UserProfile")]
    public class UserProfile
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserFollowerJson { get; set; }
        public string UserFollowingJson { get; set; }
        public string UserPostJson { get; set; }
        public string UserName { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserRequestJson { get; set; } // new field
    }
    public class UploadMediaModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
    }

    public class UploadMediaModel_Out
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public DateTime LastSyncDate { get; set; }
    }
    public class FindUploadClassModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public double AvgRating { get; set; }
        public string TotalReviews { get; set; }

    }
    public class SearchClassModel
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Mentions { get; set; }
    }
}
