﻿using SQLite;
using System;

namespace Serena.Model
{
    [Table("Serena_UserProfileData_Tran")]
    public class UserProfileData_Tran
    {
        [AutoIncrement, PrimaryKey]
        public int UserTranId { get; set; }
        public string UserEmailId { get; set; }
        public string RequestTo { get; set; }
        public int Status { get; set; }
        public string Guid { get; set; }
        public DateTime LastUpdated { get; set; }
    }

    [Table("Serena_UserProfileData_TranOut")]
    public class UserProfileDataTran_Out
    {
        [AutoIncrement, PrimaryKey]
        public int UserTranOutId { get; set; }
        public string UserEmailId { get; set; }
        public string RequestTo { get; set; }
        public int Status { get; set; }
        public string Guid { get; set; }
        public DateTime LastSyncDate { get; set; }
    }
}
