﻿using SQLite;
using System.ComponentModel;

namespace Serena.Model
{
    public class Tophashtags
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }
        public string Hashtags { get; set; }
        public string Count { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
    }
    public class TophashtagsImg
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Hashtags { get; set; }
        public string Images { get; set; }
        public string Guid { get; set; }
        public string PostText { get; set; }
        public string PostColor { get; set; }
        public string BackgroundColor { get; set; }
        public string PostType { get; set; }
    }
    public class SearchHashTag
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Hashtags { get; set; }
        public string Count { get; set; }
        public string HashType { get; set; }
        public string Location { get; set; }
    }
    public class PostByHashTag
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string Description { get; set; }
        public string ShareType { get; set; }
        public string Location { get; set; }
        public string Topics { get; set; }
        public string ImageUrl { get; set; }
        public string Tags { get; set; }
        public string HashTags { get; set; }
        public string PostText { get; set; }
        public string PostColor { get; set; }
        public string LastCreated { get; set; }
        public string LastUpdated { get; set; }
        public string TimeOfPost { get; set; }
        public string PostBackgroundColor { get; set; }
        public double Opacity { get; set; }
        public string PlaceAddress { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string PostType { get; set; }
        public string Guid { get; set; }
        [DefaultValue(0)]
        public int Favourite { get; set; }
        public string FavCount { get; set; }
    }
}
