﻿using SQLite;
using System.ComponentModel;
using System;

namespace Serena.Model
{
    [Table("Serena_Post")]
    public class Serena_Post
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string UserEmailId { get; set; }
        public string Description { get; set; }
        public string ShareType { get; set; }
        public string Location { get; set; }
        public string Topics { get; set; }
        public string ImageUrl { get; set; }
        public string Tags { get; set; }
        public string HashTags { get; set; }
        public string PostText { get; set; }
        public string PostColor { get; set; }
        public DateTime LastCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public string TimeOfPost { get; set; }
        public string PostBackgroundColor { get; set; }
        public double Opacity { get; set; }
        public string PlaceAddress { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string PostType { get; set; }
        public string Guid { get; set; }
        [DefaultValue(0)]
        public int Favourite{ get; set; }
        public string FavCount { get; set; }
        public string VideoThumbnailUrl { get; set; }

        //Added on 17Feb2021 by shraddhap
        public string UserName { get; set; }
        public string UserProfileImage { get; set; }
        public string FavouriteImage { get; set; }
    }
}
