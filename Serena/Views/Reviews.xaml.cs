﻿using Newtonsoft.Json;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Plugin.Toast;
using Serena.Data;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Reviews : ContentPage
    {
        string reviewGuid = "";
        string prevPageName = "";
        string hashTag = "";
        public Reviews(string selectedGuid, string prevPage, string Htag)
        {
            InitializeComponent();
            try
            {
                reviewGuid = selectedGuid;
                prevPageName = prevPage;
                hashTag = Htag;

                #region review
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var serenaDataSync = new SerenaDataSync();
                    Task.Run(() => serenaDataSync.GetSerenaReviews(reviewGuid)).Wait();

                    //reviewsDB.DeleteAllReviews();
                    var guidReviews = App.DBIni.ReviewsDB.GetAllReviewsByGuid(reviewGuid);
                    if (guidReviews != null)
                        reviewList.ItemsSource = guidReviews;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                #endregion
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Reviews.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private async void OnImageNameTapped(object sender, EventArgs e)
        {
            if (prevPageName == "TodaysHomePage")
                await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
            if (prevPageName == "Post")
                await Navigation.PushAsync(new NavigationPage(new Post()));
            if (prevPageName == "ExplorePost" && !string.IsNullOrEmpty(hashTag))
                await Navigation.PushAsync(new NavigationPage(new ExplorePost(hashTag)));
            if (prevPageName == "ViewAllPosts")
                await Navigation.PushAsync(new NavigationPage(new ViewAllPosts()));

        }
        private void ReviewBtnClicked(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = true;
        }


        private void SaveBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    #region adding reviews for particular post

                    var serenaPostReview = new SerenaReviews();

                    serenaPostReview.Guid = reviewGuid;
                    serenaPostReview.Review = txtReview.Text;
                    serenaPostReview.Rating = 0;
                    serenaPostReview.UserEmailId = Settings.GeneralSettings;
                    serenaPostReview.DateAdded = DateTime.Now.ToString("MM/dd/yyyy");

                    var userProfileData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Settings.GeneralSettings);
                    if (userProfileData != null)
                        serenaPostReview.UserName = userProfileData != null ? userProfileData.FULLNAME : string.Empty;


                    App.DBIni.ReviewsDB.AddReview(serenaPostReview);

                    #endregion

                    #region binding list of reviews for particualr post

                    var guidReviews = App.DBIni.ReviewsDB.GetAllReviewsByGuid(reviewGuid);
                    if (guidReviews != null)
                        reviewList.ItemsSource = guidReviews;

                    #endregion

                    #region saving the data to rds
                    var current1 = Connectivity.NetworkAccess;
                    if (current1 == NetworkAccess.Internet)
                        if (current1 == NetworkAccess.Internet)
                        {
                            var serenaDataSync = new SerenaDataSync();
                            Task.Run(() => serenaDataSync.PostSerenaReviewsByGuid(serenaPostReview));
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);


                    #endregion

                    popupLoadingView.IsVisible = false;
                    txtReview.Text = "";
                    //sfRating2.Value = 0;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Reviews.Xaml";
                addlogFile.ExceptionEventName = "SaveBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void CloseBtnClicked(object sender, EventArgs e)
        {
            txtReview.Text = "";
            //sfRating2.Value = 0;
            popupLoadingView.IsVisible = false;
        }
    }
}