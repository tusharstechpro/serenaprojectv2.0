﻿using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Serena.ViewModel;
using Syncfusion.DataSource.Extensions;
using Syncfusion.XForms.Buttons;
using Syncfusion.XForms.Graphics;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Plugin.Toast;
using Serena.Data;
using Xamarin.Essentials;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FollowUsersPage : ContentPage
    {
        string sessionEmailId;
        string userfollowEmailId;
        string sessionSegmentValue;

        public FollowUsersPage(string followEmailId)
        {
            InitializeComponent();
            LoadInitialData(followEmailId);
        }

        private void LoadInitialData(string followEmailId)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //Get the login user EmailId from Session
                    sessionEmailId = Settings.GeneralSettings;
                    userfollowEmailId = followEmailId;
                    sessionSegmentValue = SegmentSession.segmentsessionValue;
                    if (!string.IsNullOrEmpty(followEmailId))
                    {
                        //Get specific user by EmailID from SQLite
                        var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(followEmailId);
                        if (UserData != null)
                        {
                            //set UserName and HandleName
                            UserNameLbl.Text = UserData.FULLNAME;
                            HandleNameLbl.Text = "@" + UserData.USERNAME;
                        }
                        //Get specific userProfileData by EmailID from SQLite
                        var userprofileData = App.DBIni.UserProfileDataDB.GetSpecificUser(followEmailId);
                        if (userprofileData != null)
                        {
                            //set UserName and HandleName
                            UserNameLbl.Text = userprofileData.UserName;
                            HandleNameLbl.Text = "@" + userprofileData.UserName;
                            //Set ImageCircle, LocationLbl, BioLbl, LinkLbl
                            ImageCircle.Source = userprofileData.UserProfileImage;
                            LocationLbl.Text = userprofileData.UserLocation;
                            BioLbl.Text = userprofileData.UserBio;
                            LinkLbl.Text = userprofileData.UserWebsite;

                            #region Followers and Following Tabs code
                            var followUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(3);//Followers List
                            var followCount = followUsers.Count().ToString();
                            if (followCount != "0")
                            {
                                var addUserProfileFollowList = new List<UserProfileData>();
                                foreach (var user in followUsers)
                                {
                                    if (followEmailId != user.UserEmailId)
                                    {
                                        var followUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                        UserProfileData addFollowUserProfile = new UserProfileData();
                                        addFollowUserProfile.UserEmailId = followUsersdata.UserEmailId;
                                        addFollowUserProfile.UserName = followUsersdata.UserName;
                                        addFollowUserProfile.UserProfileImage = followUsersdata.UserProfileImage;
                                        addUserProfileFollowList.Add(addFollowUserProfile);
                                    }
                                }
                                Followerlist.ItemsSource = null;
                                Followerlist.ItemsSource = addUserProfileFollowList;
                                if (Followerlist.ItemsSource == null)
                                {
                                    FollowersListMessageLbl.IsVisible = true;
                                }
                                else if (Followerlist.ItemsSource != null)
                                {
                                    FollowersListMessageLbl.IsVisible = false;
                                }
                                FollowerTab.Title = addUserProfileFollowList.Count().ToString() + " " + "followers";
                            }

                            var followingUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(1);//followingUsers List
                            var followingCount = followingUsers.Count().ToString();
                            if (followingCount != "0")
                            {
                                var addUserProfileFollowingList = new List<UserProfileData>();
                                foreach (var user in followingUsers)
                                {
                                    var followingUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                    UserProfileData addFollowingUserProfile = new UserProfileData();
                                    addFollowingUserProfile.UserEmailId = followingUsersdata.UserEmailId;
                                    addFollowingUserProfile.UserName = followingUsersdata.UserName;
                                    addFollowingUserProfile.UserProfileImage = followingUsersdata.UserProfileImage;
                                    addUserProfileFollowingList.Add(addFollowingUserProfile);
                                }
                                FollowingList.ItemsSource = addUserProfileFollowingList;
                                FollowingTab.Title = followingUsers.Count().ToString() + " " + "following";
                            }

                            var suggestionsUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(3);//suggestions List
                            var suggestionCount = suggestionsUsers.Count().ToString();
                            if (suggestionCount != "0")
                            {
                                var addUserProfileSuggestionList = new List<UserProfileData>();
                                foreach (var user in suggestionsUsers)
                                {
                                    if (followEmailId != user.UserEmailId)
                                    {
                                        var suggestionsUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);
                                        UserProfileData addsuggestionsUsersProfile = new UserProfileData();
                                        addsuggestionsUsersProfile.UserEmailId = suggestionsUsersdata.UserEmailId;
                                        addsuggestionsUsersProfile.UserName = suggestionsUsersdata.UserName;
                                        addsuggestionsUsersProfile.UserProfileImage = suggestionsUsersdata.UserProfileImage;
                                        addUserProfileSuggestionList.Add(addsuggestionsUsersProfile);
                                    }
                                }
                                SuggestionsList.ItemsSource = addUserProfileSuggestionList;
                            }
                        }
                        #endregion

                        #region Profile posts tab
                        var postdata = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(userfollowEmailId);
                        if (postdata != null)
                        {
                            var postImage = new List<Serena_Post>();
                            foreach (var postDetails in postdata)
                            {
                                if (postDetails.PostType == "Text")//Textpost
                                {
                                    var userTextPost = new Serena_Post();
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    var bgcolor = postDetails.PostBackgroundColor.Replace("#FF", "#");
                                    userTextPost.PostText = postDetails.PostText;
                                    userTextPost.PostColor = textcolor;
                                    userTextPost.PostBackgroundColor = bgcolor;
                                    postImage.Add(userTextPost);

                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText == "-")//ImagePost
                                {
                                    var userImagePost = new Serena_Post();
                                    userImagePost.ImageUrl = postDetails.ImageUrl;
                                    userImagePost.Opacity = postDetails.Opacity;
                                    postImage.Add(userImagePost);
                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText != "-")//ImagePost text overlay
                                {
                                    PostList.ItemsSource = null;
                                    var userImageTextPost = new Serena_Post();
                                    userImageTextPost.ImageUrl = postDetails.ImageUrl;
                                    userImageTextPost.PostText = postDetails.PostText;
                                    userImageTextPost.Opacity = postDetails.Opacity;
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    userImageTextPost.PostColor = textcolor;

                                    postImage.Add(userImageTextPost);
                                }
                            }
                            PostList.ItemsSource = postImage;
                            PostTab.Title = postImage.Count().ToString() + " " + "posts";
                        }
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "FollowUsersPage.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        #region Search in lists for followers/following/suggestionlist
        //Search present above followers list
        string FollowsearchBar = null;
        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            FollowsearchBar = FollowersSearch.Text;
            if (Followerlist.DataSource != null)
            {
                this.Followerlist.DataSource.Filter = SearchUserName;
                this.Followerlist.DataSource.RefreshFilter();
            }

        }
        private bool SearchUserName(object obj)
        {
            if (FollowsearchBar == null || FollowsearchBar == null)
                return true;

            var username = obj as UserProfileData;
            if (username.UserName.ToLower().Contains(FollowsearchBar.ToLower())
                 || username.UserName.ToLower().Contains(FollowsearchBar.ToLower()))
            {
                FollowersListMessageLbl.IsVisible = false;
                return true;
            }
            else
            {
                FollowersListMessageLbl.IsVisible = true;
                return false;
            }

        }

        //Search present above following list
        string followingSearchBar = null;
        private void FollowingSearchBar_TextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            followingSearchBar = FollowingSearch.Text;
            if (FollowingList.DataSource != null)
            {
                this.FollowingList.DataSource.Filter = FollowingSearchUserName;
                this.FollowingList.DataSource.RefreshFilter();
            }
        }
        private bool FollowingSearchUserName(object obj)
        {
            if (followingSearchBar == null || followingSearchBar == null)
                return true;

            var username = obj as UserProfileData;


            if (username.UserName.ToLower().Contains(followingSearchBar.ToLower())
                 || username.UserName.ToLower().Contains(followingSearchBar.ToLower()))
                return true;
            else
                return false;
        }

        //Search present above Suggestion list
        string suggSearchBar = null;
        private void SuggestionsSearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            suggSearchBar = SuggestionsSearch.Text;
            if (SuggestionsList.DataSource != null)
            {
                this.SuggestionsList.DataSource.Filter = SuggSearchUserName;
                this.SuggestionsList.DataSource.RefreshFilter();
            }
        }
        private bool SuggSearchUserName(object obj)
        {
            if (suggSearchBar == null || suggSearchBar == null)
                return true;

            var username = obj as UserProfileData;
            if (username.UserName.ToLower().Contains(suggSearchBar.ToLower())
                 || username.UserName.ToLower().Contains(suggSearchBar.ToLower()))
                return true;
            else
                return false;
        }
        #endregion

        void profileTab_SelectionChanged(object sender, Syncfusion.XForms.TabView.SelectionChangedEventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var selectedIndex = e.Index;
                    if (selectedIndex == 0)
                    {
                        //PostList.IsVisible = true;
                        //PlacesPostList.IsVisible = false;
                        var postdata = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(userfollowEmailId);
                        if (postdata != null)
                        {
                            var postImage = new List<Serena_Post>();
                            foreach (var postDetails in postdata)
                            {
                                if (postDetails.PostType == "Text")//Textpost
                                {
                                    var userTextPost = new Serena_Post();
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    var bgcolor = postDetails.PostBackgroundColor.Replace("#FF", "#");
                                    userTextPost.PostText = postDetails.PostText;
                                    userTextPost.PostColor = textcolor;
                                    userTextPost.PostBackgroundColor = bgcolor;
                                    postImage.Add(userTextPost);

                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText == "-")//ImagePost
                                {
                                    var userImagePost = new Serena_Post();
                                    userImagePost.ImageUrl = postDetails.ImageUrl;
                                    userImagePost.Opacity = postDetails.Opacity;
                                    postImage.Add(userImagePost);
                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText != "-")//ImagePost text overlay
                                {
                                    PostList.ItemsSource = null;
                                    var userImageTextPost = new Serena_Post();
                                    userImageTextPost.ImageUrl = postDetails.ImageUrl;
                                    userImageTextPost.PostText = postDetails.PostText;
                                    userImageTextPost.Opacity = postDetails.Opacity;
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    userImageTextPost.PostColor = textcolor;

                                    postImage.Add(userImageTextPost);
                                }
                            }
                            PostList.ItemsSource = postImage;
                            PostTab.Title = postImage.Count().ToString() + " " + "posts";
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "FollowUsersPage.Xaml";
                addlogFile.ExceptionEventName = "profileTab_SelectionChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}