﻿using Plugin.Toast;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Explore : ContentPage
    {
        Tophashtags tophashtags = new Tophashtags();
        string sessionEmailId;

        public Explore()
        {
            InitializeComponent();
        }
      
        protected override void OnAppearing()
        {
            try
            {
                PeopleInitialData();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
            //base.OnAppearing();
        }

        private void PeopleInitialData()
        {
            try
            {
                sessionEmailId = Settings.GeneralSettings;

                #region toppeople api 
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    exploreTabs.SelectedIndex = 0;

                    var topPeople = App.DBIni.TophashtagDB.GetTopPeopleData();
                    if (topPeople.Count() > 0)
                        listview.ItemsSource = topPeople;
               }
                //else
                    //CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                #endregion
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void Rating_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var test = "Rating :" + " " + e.NewValue + " /5";
        }

        void exploreTab_SelectionChanged(object sender, Syncfusion.XForms.TabView.SelectionChangedEventArgs e)
        {
            try
            {
                var selectedIndex = e.Index;

                #region HashTag Tab
                if (selectedIndex == 0)
                {
                    #region toppeople
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                         
                        var topPeople = App.DBIni.TophashtagDB.GetTopPeopleData();
                        if (topPeople.Count() > 0)
                            listview.ItemsSource = topPeople;
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                    #endregion
                }
                else if (selectedIndex == 1)//CLASSES
                {
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                        
                        var findclass = App.DBIni.UserProfileDataDB.GetTopFindClass();
                        if (findclass.Count() > 0)
                            listClasses.ItemsSource = findclass;
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                }
                else if (selectedIndex == 2)
                {

                    //var userprofileData = userProfileDB.GetSpecificUser(Settings.GeneralSettings);
                    //if (userprofileData != null)
                    //    lblLocationName.Text = userprofileData.UserLocation;
                    //else
                    //    lblLocationName.Text = "";
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {

                        #region top hastags and images binding
                     
                        //Task.Run(() => serenaDataSync.GetSerenaExploreSearchHashTag(lblLocationName.Text)).Wait();

                        var hashtag = App.DBIni.TophashtagDB.GetTophashtagData();
                        if (hashtag != null)
                        {
                            var addtophashtag = new List<Tophashtags>();
                            foreach (var hashtags in hashtag)
                            {
                                var addhashtag = new Tophashtags();
                                addhashtag.Hashtags = hashtags.Hashtags;
                                addhashtag.Count = hashtags.Count + " tags  > ";
                                addhashtag.Img1 = hashtags.Img1;
                                addhashtag.Img2 = hashtags.Img2;
                                addhashtag.Img3 = hashtags.Img3;
                                addtophashtag.Add(addhashtag);
                            }
                            hashtaglist.ItemsSource = addtophashtag;
                        }
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                    #endregion

                    //#region trending in your area
                    //if (!string.IsNullOrEmpty(Settings.GeneralSettings))
                    //{
                    //    var areahashtag = tophashtagsDB.GetSearchTophashtagDataToDelete().Where(x => x.HashType.ToLower() == "l");
                    //    if (areahashtag != null)
                    //    {
                    //        var addareahashtag = new List<SearchHashTag>();
                    //        foreach (var hashtags in areahashtag)
                    //        {
                    //            var addhashtag = new SearchHashTag();
                    //            addhashtag.Hashtags = hashtags.Hashtags;
                    //            addhashtag.Count = hashtags.Count + " tags  > ";

                    //            addareahashtag.Add(addhashtag);
                    //        }
                    //        areaHashtaglist.ItemsSource = addareahashtag;
                    //        areaHashtaglist.IsVisible = true;

                    //        lblLocationName.IsVisible = true;
                    //        imgPin.IsVisible = true;
                    //    }
                    //}
                    //#endregion

                    #region visibility 

                    hashtaglist.IsVisible = true;
                    lblTopHashTagsTitle.IsVisible = true;
                    //lblareaTags.IsVisible = true;
                    lbltopResults.IsVisible = false;
                    searchHashtaglist.ItemsSource = null;
                    searchHashtaglist.IsVisible = false;
                    txtSearch.Text = "";
                    #endregion
                }

                #endregion
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "exploreTab_SelectionChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Top people search
        private void txtPeopleSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtPeopleSearch.Text))
                {

                    var Topics = App.DBIni.TophashtagDB.GetTopPeopleData().Where(p => p.Topics.ToLower().Contains(txtPeopleSearch.Text.ToLower()));
                    if (Topics != null)
                    {
                        var addtopclassTitle = new List<People>();
                        foreach (var classTitle in Topics)
                        {
                            var addClass = new People();
                            addClass.Topics = classTitle.Topics;
                            addClass.TotalReviews = classTitle.TotalReviews + " tags  > ";

                            addtopclassTitle.Add(addClass);
                        }

                        #region visibility

                        searchPeoplelist.ItemsSource = addtopclassTitle;

                        searchPeoplelist.IsVisible = true;
                        lblpeopletopResults.IsVisible = true;

                        listview.IsVisible = false;
                        lblTPeoplename.IsVisible = false;

                        #endregion
                    }

                }
                else if (string.IsNullOrEmpty(txtPeopleSearch.Text))
                {
                    searchPeoplelist.ItemsSource = null;
                    searchPeoplelist.IsVisible = false;
                    listview.IsVisible = true;
                    lblTPeoplename.IsVisible = true;
                    lblpeopletopResults.IsVisible = false;

                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "txtPeopleSearch_TextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Top Class search
        private void OnClassSearchTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtClassSearch.Text))
                {

                    var Searchtitle = App.DBIni.UserProfileDataDB.GetTopFindClass().Where(p => p.Title.ToLower().Contains(txtClassSearch.Text.ToLower()));
                    if (Searchtitle != null)
                    {
                        var addtopclassTitle = new List<SearchClassModel>();
                        foreach (var classTitle in Searchtitle)
                        {
                            var addClass = new SearchClassModel();
                            addClass.Title = classTitle.Title;
                            addClass.TotalReviews = classTitle.TotalReviews + " tags  > ";

                            addtopclassTitle.Add(addClass);
                        }

                        #region visibility

                        searchClasslist.ItemsSource = addtopclassTitle;

                        searchClasslist.IsVisible = true;
                        lblclasstopResults.IsVisible = true;

                        listClasses.IsVisible = false;
                        lblTopClassTitle.IsVisible = false;

                        #endregion
                    }

                }
                else if (string.IsNullOrEmpty(txtClassSearch.Text))
                {
                    searchClasslist.ItemsSource = null;
                    searchClasslist.IsVisible = false;
                    listClasses.IsVisible = true;
                    lblTopClassTitle.IsVisible = true;
                    lblclasstopResults.IsVisible = false;

                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "OnSearchTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Top hashtag search
        private void OnSearchTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {

                    //var hashtag = tophashtagsDB.GetSearchTophashtagDataToDelete().Where(p => p.Hashtags.ToLower().Contains(txtSearch.Text.ToLower()) && p.HashType == "A");
                    var hashtag = App.DBIni.TophashtagDB.GetTophashtagData().Where(p => p.Hashtags.ToLower().Contains(txtSearch.Text.ToLower()));

                    if (hashtag != null)
                    {
                        var addtophashtag = new List<Tophashtags>();
                        foreach (var hashtags in hashtag)
                        {
                            var addhashtag = new Tophashtags();
                            addhashtag.Hashtags = hashtags.Hashtags;
                            addhashtag.Count = hashtags.Count + " tags  > ";

                            addtophashtag.Add(addhashtag);
                        }

                        #region visibility

                        searchHashtaglist.ItemsSource = addtophashtag;

                        searchHashtaglist.IsVisible = true;
                        lbltopResults.IsVisible = true;

                        hashtaglist.IsVisible = false;
                        lblTopHashTagsTitle.IsVisible = false;

                        //areaHashtaglist.IsVisible = false;
                        //lblareaTags.IsVisible = false;
                        //lblLocationName.IsVisible = false;
                        //imgPin.IsVisible = false;

                        #endregion
                    }

                }
                else if (string.IsNullOrEmpty(txtSearch.Text))
                {
                    searchHashtaglist.ItemsSource = null;
                    searchHashtaglist.IsVisible = false;
                    hashtaglist.IsVisible = true;
                    lblTopHashTagsTitle.IsVisible = true;
                    lbltopResults.IsVisible = false;
                    //areaHashtaglist.IsVisible = true;
                    //lblareaTags.IsVisible = true;
                    //lblLocationName.IsVisible = true;
                    //imgPin.IsVisible = true;

                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "OnSearchTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void ClearBtnClicked(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            searchHashtaglist.ItemsSource = null;
            searchHashtaglist.IsVisible = false;
            hashtaglist.IsVisible = true;
            lblTopHashTagsTitle.IsVisible = true;
            lbltopResults.IsVisible = false;
            //areaHashtaglist.IsVisible = true;
            //lblareaTags.IsVisible = true;
            //lblLocationName.IsVisible = true;
            //imgPin.IsVisible = true;
        }

        //Redirecting on Post page using selected specific HashTag
        async void explorePostClicked(object sender, EventArgs e)
        {
            try
            {
                var labelExplorePost = (Label)sender;
                var item = (TapGestureRecognizer)labelExplorePost.GestureRecognizers[0];
                string selectedHashTag = item.CommandParameter.ToString();

                if (!string.IsNullOrEmpty(Settings.GeneralSettings))
                    await Navigation.PushAsync(new ExploreSearchHashtag(selectedHashTag));
                else
                {
                    bool value = await DisplayAlert("", "You can only see the hashtag posts as a member", "OK", "Cancel");
                    if (value == true)
                        await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "searchedHashTagClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Redirecting on ExploreSearchHashtag using selected specific HashTag
        async void searchedHashTagClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var labelExplorePost = (Label)sender;
                    var item = (TapGestureRecognizer)labelExplorePost.GestureRecognizers[0];
                    string selectedHashTag = item.CommandParameter.ToString();

                    if (!string.IsNullOrEmpty(Settings.GeneralSettings))
                        await Navigation.PushAsync(new ExploreSearchHashtag(selectedHashTag));
                    else
                    {
                        bool value = await DisplayAlert("", "You can only see the hashtag posts as a member", "OK", "Cancel");
                        if (value == true)
                            await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "searchedHashTagClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //class video
        async void ThumbNail_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    var thumbNailImage = (Xamarin.Forms.Image)sender;
                    var item = (TapGestureRecognizer)thumbNailImage.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenClassVideo(selectedGuid));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "ThumbNail_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var anyWhereTap = (Xamarin.Forms.StackLayout)sender;
                    var item = (TapGestureRecognizer)anyWhereTap.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenClassVideo(selectedGuid));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "TapGestureRecognizer_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void classoverlay1_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var thumbNailImage = (Xamarin.Forms.Image)sender;
                    var item = (TapGestureRecognizer)thumbNailImage.GestureRecognizers[0];
                    string selectedCategory = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new ExploreByCategory(selectedCategory));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "ThumbNail_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

    }
}