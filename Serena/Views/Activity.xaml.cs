﻿using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Serena.Views.ActivityTab;
using Serena.Views.ProMember;
using Syncfusion.XForms.Buttons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;
using Syncfusion.SfBusyIndicator.XForms;
using Serena.Interface;
using System.Globalization;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Activity : ContentPage
    {

        string sessionEmailId, WatchClassGUID;
        DateTime SheduleDate;
        SfBusyIndicator busyIndicator = new SfBusyIndicator();
        INotificationManager notificationManager;
        public Activity()
        {
            InitializeComponent();
            try
            {
                busyIndicator.IsBusy = false;
                //Get the login user EmailId from Session
                sessionEmailId = Settings.GeneralSettings;
                NewInvitationLayout.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        protected override void OnAppearing()
        {

            try
            {
                LoadInitialData();
                // base.OnAppearing();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "OnAppearing";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    #region New Invitation
                    sessionEmailId = Settings.GeneralSettings;
                    #region Invitation Count
                    var TotalInvites = App.DBIni.NotificationDB.GetUpcomingNotificationsBystatus("0", sessionEmailId);
                    InviteCountLbl.Text = "(" + TotalInvites.Count().ToString() + ")";
                    #endregion
                    var Notifications = App.DBIni.NotificationDB.GetInviteBycategory("0", sessionEmailId);
                    if (Notifications != null)
                    {
                        NewInvitationLayout.IsVisible = true;
                        //To get userProfileData by HostEmailID from SQLite
                        var userProfileData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Notifications.HostEmailID);
                        if (userProfileData != null)
                        {
                            CoverPhotoImg.Source = Notifications.CoverPhotoUrl;
                            JoinLbl.Text = "Join " + userProfileData.FULLNAME + " For A " + Notifications.Category + " Class!";
                            MentionsBtn.Text = Notifications.Mentions;
                            DurationBtn.Text = Notifications.Duration.Replace("Time: ", "");
                            CategoryBtn.Text = Notifications.Category;
                            aboutLbl.Text = Notifications.About;
                            Acceptbtn.CommandParameter = Notifications.WatchPartyGuid;
                            Declinebtn.CommandParameter = Notifications.WatchPartyGuid;
                            WatchClassGUID = Notifications.WatchPartyGuid;
                            SheduleDate = Notifications.ScheduledDate;
                        }
                    }
                    else
                        NewInvitationLayout.IsVisible = false;
                    #endregion

                    #region Upcoming List
                    var RequestNotify = new List<Upcoming_Notifications>();
                    var NewInviteData = App.DBIni.NotificationDB.GetUpcomingNotifications();
                    foreach (var Notification in NewInviteData)
                    {
                        if (Notification.Accepted == "1")//To show all watch party details in upcoming list
                        {

                            if (!string.IsNullOrEmpty(Notification.AvgRating))
                            {
                                double rating = Convert.ToDouble(Notification.AvgRating);
                                rating = Math.Round(rating, 1);
                                Notification.AvgRating = Convert.ToString(rating);
                            }
                            else
                            {
                                Notification.AvgRating = "0";
                            }
                            if (Notification.ScheduledDate != null)
                            {
                                SheduleDate = Notification.ScheduledDate;
                                #region Schedule Date to display in list
                                if (!string.IsNullOrEmpty(Notification.ScheduledTime))
                                {
                                    var Time = Convert.ToDateTime(Notification.ScheduledTime);
                                    var shTime = Time.Hour + "/" + Time.Minute;
                                    Notification.Scheduleday = Notification.ScheduledDate.DayOfWeek + ", " + shTime + " ";
                                }
                                #endregion
                                #region starting now btn Text
                                TimeSpan Difference = Notification.ScheduledDate.Date - DateTime.Now.Date;
                                var Days = Difference.Days;
                                if (Days > 0)
                                {
                                    Notification.ButtonText = "STARTING IN " + Days + " DAYS";
                                    RequestNotify.Add(Notification);
                                }
                                else if (Days == 0)
                                {
                                    Notification.ButtonText = "STARTING SOON";
                                    RequestNotify.Add(Notification);
                                }
                                #endregion
                            }
                        }
                        else if (Notification.Accepted == "0" && Notification.HostEmailID == sessionEmailId)//To show watch party details to host
                        {
                            if (!string.IsNullOrEmpty(Notification.AvgRating))
                            {
                                double rating = Convert.ToDouble(Notification.AvgRating);
                                rating = Math.Round(rating, 1);
                                Notification.AvgRating = Convert.ToString(rating);
                            }
                            else
                            {
                                Notification.AvgRating = "0";
                            }

                            if (Notification.ScheduledDate != null)
                            {
                                SheduleDate = Notification.ScheduledDate;
                                #region Schedule Date to display in list
                                if (!string.IsNullOrEmpty(Notification.ScheduledTime))
                                {
                                    var Time = Convert.ToDateTime(Notification.ScheduledTime);
                                    var shTime = Time.Hour + "/" + Time.Minute;
                                    Notification.Scheduleday = Notification.ScheduledDate.DayOfWeek + ", " + shTime;
                                }
                                #endregion
                                #region starting now btn Text
                                TimeSpan Difference = Notification.ScheduledDate.Date - DateTime.Now.Date;
                                var Days = Difference.Days;
                                if (Days > 0)
                                {
                                    Notification.ButtonText = "STARTING IN " + Days + " DAYS";
                                    RequestNotify.Add(Notification);
                                }
                                else if (Days == 0)
                                {
                                    Notification.ButtonText = "STARTING SOON";
                                    RequestNotify.Add(Notification);
                                }
                                #endregion
                            }
                        }
                    }
                    Upcominglistview.ItemsSource = RequestNotify;
                    #endregion

                    //Notification service init
                    notificationManager = DependencyService.Get<INotificationManager>();
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void BellIconTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new Notification());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "BellIconTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void onDetailsLblClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new HostView(WatchClassGUID));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "onDetailsLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void PartyReqAcceptbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    Acceptbtn.Text = "ACCEPTED";
                    Acceptbtn.IsEnabled = false;
                    Declinebtn.IsEnabled = false;
                    var sfButton = sender as SfButton;
                    string WatchPartyGuid = sfButton.CommandParameter.ToString();
                    var notifyDatabycategory = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(WatchPartyGuid);
                    if (notifyDatabycategory != null)
                    {
                        #region To generate notification via app
                        //To get username from useremailid
                        var UserData = App.DBIni.UserProfileDataDB.GetSpecificUser(notifyDatabycategory.HostEmailID);
                        if (UserData != null)
                        {
                            string title = UserData.UserName + $" invited you to join a " + notifyDatabycategory.Title + " class at " + notifyDatabycategory.ScheduledTime + notifyDatabycategory.AMPM + " today!";
                            string message = $"Open up your app to see details about this upcoming class and accept the invitation.";
                            //create a datetime value
                            var dates = notifyDatabycategory.ScheduledDate.Year + "/" + notifyDatabycategory.ScheduledDate.Month + "/" + notifyDatabycategory.ScheduledDate.Day + " " + notifyDatabycategory.ScheduledTime + ":" + "00" + " " + notifyDatabycategory.AMPM;
                            //To convert string to datetime
                            CultureInfo culture = new CultureInfo("en-US");
                            //To convert string to datetime
                            DateTime notifyDate = Convert.ToDateTime(dates, culture);
                            //To get 5 minutes prior datetime
                            DateTime notifyTime = notifyDate.AddMinutes(-5);
                            //To pass notification details to SendNotification
                            notificationManager.SendNotification(title, message, notifyTime);
                        }
                        #endregion

                        #region update members in upcoming_notification table Sqlite
                        var oldMem = notifyDatabycategory.Members;
                        //If member is null or empty
                        if (string.IsNullOrEmpty(oldMem))
                        {
                            var newMem = sessionEmailId + ",";
                            notifyDatabycategory.Members = newMem;
                        }
                        //If member already has another user
                        else
                        {
                            var newMem1 = oldMem + "," + sessionEmailId;
                            notifyDatabycategory.Members = newMem1;
                        }
                        notifyDatabycategory.Accepted = "1";
                        var returns = App.DBIni.NotificationDB.AddUpcomingNotifications(notifyDatabycategory);
                        #endregion

                        #region update status of WatchPartyTran and WatchPartyTran_Out  table in sqlite
                        WatchPartyTran WPTran = new WatchPartyTran();
                        WatchPartyTran_Out WPTranout = new WatchPartyTran_Out();

                        WPTran.WatchPartyGuid = WPTranout.WatchPartyGuid = WatchPartyGuid;
                        WPTran.Members = WPTranout.Members = sessionEmailId;
                        WPTran.Accepted = WPTranout.Accepted = "1";
                        WPTranout.LastSyncDate = DateTime.Now;

                        App.DBIni.WatchPartyDB.AddWatchPartyTran(WPTran);

                        App.DBIni.WatchPartyDB.AddWatchPartyTran_Out(WPTranout);

                        #endregion

                    }
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "PartyReqAcceptbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void PartyReqRejectbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    Declinebtn.Text = "DECLINEED";
                    Acceptbtn.IsEnabled = false;
                    Declinebtn.IsEnabled = false;

                    var sfButton = sender as SfButton;
                    string WatchPartyGuid = sfButton.CommandParameter.ToString();

                    var notifyDatabycategory = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(WatchPartyGuid);
                    if (notifyDatabycategory != null)
                    {

                        #region update members in upcoming_notification table Sqlite
                        notifyDatabycategory.Accepted = "2";
                        var returns = App.DBIni.NotificationDB.AddUpcomingNotifications(notifyDatabycategory);
                        #endregion

                        #region update status of WatchPartyTran and WatchPartyTran_Out  table in sqlite
                        WatchPartyTran WPTran = new WatchPartyTran();
                        WatchPartyTran_Out WPTranout = new WatchPartyTran_Out();

                        WPTran.WatchPartyGuid = WPTranout.WatchPartyGuid = WatchPartyGuid;
                        WPTran.Members = WPTranout.Members = sessionEmailId;
                        WPTran.Accepted = WPTranout.Accepted = "2";
                        WPTranout.LastSyncDate = DateTime.Now;

                        App.DBIni.WatchPartyDB.AddWatchPartyTran(WPTran);

                        App.DBIni.WatchPartyDB.AddWatchPartyTran_Out(WPTranout);

                        #endregion
                    }
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "PartyReqAcceptbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void SoonBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var sfButton = sender as SfButton;
                    string NewWatchPartyGuid = sfButton.CommandParameter.ToString();
                    string buttontext = sfButton.Text;
                    if (buttontext == "STARTING SOON")
                    {
                        await Navigation.PushAsync(new WaitingRoom(NewWatchPartyGuid));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "SoonBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void UpcomingEvent_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //SfBusyIndicator();
                    var stacklayoutTap = (Xamarin.Forms.StackLayout)sender;
                    var item = (TapGestureRecognizer)stacklayoutTap.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();
                    if (selectedGuid != null)
                    {
                        await Navigation.PushAsync(new HostView(selectedGuid));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "UpcomingEvent_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        void ActivityTab_SelectionChanged1(object sender, Syncfusion.XForms.TabView.SelectionChangedEventArgs e)
        {
            try
            {
                var selectedIndex = e.Index;
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (selectedIndex == 0)//Saved Tab
                    {

                    }
                    else if (selectedIndex == 1)//Upcoming Tab
                    {
                        #region Invitation Count
                        var TotalInvites = App.DBIni.NotificationDB.GetUpcomingNotificationsBystatus("0", sessionEmailId);
                        InviteCountLbl.Text = "(" + TotalInvites.Count().ToString() + ")";
                        #endregion

                        #region New Invitation
                        sessionEmailId = Settings.GeneralSettings;
                        var Notifications = App.DBIni.NotificationDB.GetInviteBycategory("0", sessionEmailId);
                        if (Notifications != null)
                        {
                            #region Button Text
                            TimeSpan Difference = Notifications.ScheduledDate.Date - DateTime.Now.Date;
                            var Days = Difference.Days;
                            #endregion
                            NewInvitationLayout.IsVisible = true;
                            //To get userProfileData by HostEmailID from SQLite
                            var userProfileData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Notifications.HostEmailID);
                            if (userProfileData != null)
                            {
                                CoverPhotoImg.Source = Notifications.CoverPhotoUrl;
                                JoinLbl.Text = "Join " + userProfileData.FULLNAME + " For A " + Notifications.Category + " Class!";
                                MentionsBtn.Text = Notifications.Mentions;
                                DurationBtn.Text = Notifications.Duration.Replace("Time: ", "");
                                CategoryBtn.Text = Notifications.Category;
                                aboutLbl.Text = Notifications.About;
                                Acceptbtn.CommandParameter = Notifications.WatchPartyGuid;
                                Declinebtn.CommandParameter = Notifications.WatchPartyGuid;
                                WatchClassGUID = Notifications.WatchPartyGuid;
                                SheduleDate = Notifications.ScheduledDate;
                            }
                        }
                        else
                            NewInvitationLayout.IsVisible = false;
                        #endregion

                        #region Upcoming List
                        var RequestNotify = new List<Upcoming_Notifications>();
                        var NewInviteData = App.DBIni.NotificationDB.GetUpcomingNotifications();
                        foreach (var Notification in NewInviteData)
                        {
                            if (Notification.Accepted == "1")//To show all watch party details in upcoming list
                            {
                                if (!string.IsNullOrEmpty(Notification.AvgRating))
                                {
                                    double rating = Convert.ToDouble(Notification.AvgRating);
                                    rating = Math.Round(rating, 1);
                                    Notification.AvgRating = Convert.ToString(rating);
                                }
                                else
                                {
                                    Notification.AvgRating = "0";
                                }
                                if (Notification.ScheduledDate != null)
                                {
                                    SheduleDate = Notification.ScheduledDate;
                                    #region Schedule Date to display in list
                                    if (!string.IsNullOrEmpty(Notification.ScheduledTime))
                                    {
                                        var Time = Convert.ToDateTime(Notification.ScheduledTime);
                                        var shTime = Time.Hour + "/" + Time.Minute;
                                        Notification.Scheduleday = Notification.ScheduledDate.DayOfWeek + ", " + shTime + " ";
                                    }
                                    #endregion
                                    #region starting now btn Text
                                    TimeSpan Difference = Notification.ScheduledDate.Date - DateTime.Now.Date;
                                    var Days = Difference.Days;
                                    if (Days > 0)
                                    {
                                        Notification.ButtonText = "STARTING IN " + Days + " DAYS";
                                        RequestNotify.Add(Notification);
                                    }
                                    else if (Days == 0)
                                    {
                                        Notification.ButtonText = "STARTING SOON";
                                        RequestNotify.Add(Notification);
                                    }
                                    #endregion
                                }

                            }
                            else if (Notification.Accepted == "0" && Notification.HostEmailID == sessionEmailId)//To show watch party details to host
                            {

                                if (!string.IsNullOrEmpty(Notification.AvgRating))
                                {
                                    double rating = Convert.ToDouble(Notification.AvgRating);
                                    rating = Math.Round(rating, 1);
                                    Notification.AvgRating = Convert.ToString(rating);
                                }
                                else
                                {
                                    Notification.AvgRating = "0";
                                }
                                if (Notification.ScheduledDate != null)
                                {
                                    SheduleDate = Notification.ScheduledDate;
                                    #region Schedule Date to display in list
                                    if (!string.IsNullOrEmpty(Notification.ScheduledTime))
                                    {
                                        var Time = Convert.ToDateTime(Notification.ScheduledTime);
                                        var shTime = Time.Hour + "/" + Time.Minute;
                                        Notification.Scheduleday = Notification.ScheduledDate.DayOfWeek + ", " + shTime;
                                    }
                                    #endregion
                                    #region strting soon btn Text
                                    TimeSpan Difference = Notification.ScheduledDate.Date - DateTime.Now.Date;
                                    var Days = Difference.Days;
                                    if (Days > 0)
                                    {
                                        Notification.ButtonText = "STARTING IN " + Days + " DAYS";
                                        RequestNotify.Add(Notification);
                                    }
                                    else if (Days == 0)
                                    {
                                        Notification.ButtonText = "STARTING SOON";
                                        RequestNotify.Add(Notification);
                                    }
                                    #endregion
                                }
                            }
                        }
                        Upcominglistview.ItemsSource = RequestNotify;
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }

            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "ActivityTab_SelectionChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}