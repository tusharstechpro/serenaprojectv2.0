﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Amazon.S3;
using Amazon.S3.Model;
using Plugin.Media.Abstractions;
using Serena.Model;
using static Serena.Data.Constants;
using Serena.DBService;
using Newtonsoft.Json;
using Serena.Helpers;
using Syncfusion.SfBusyIndicator.XForms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Serena.Model.Address;
using System.Net;
using Xam.Forms.VideoPlayer;
using System.Linq;
using Xamarin.Essentials;
using Plugin.Toast;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateVideoPost : ContentPage
    {
        Serena_Post post = new Serena_Post();
        string videoPath;
        ImageSource thumbnailImage;
        MediaFile Selectedvideofile;
        string UserUploadVideoURL = "";
        string thumbnailImageURL = "";
        string sessionEmailId;
        public CreateVideoPost(MediaFile selectedvideoFile, ImageSource thumbnail)
        {
            InitializeComponent();
            try
            {
                //Get the login user EmailId from Session
                sessionEmailId = Settings.GeneralSettings;
                videoPath = selectedvideoFile.Path;
                //create video URI from path
                UriVideoSource uriVideoSource = new UriVideoSource()
                {
                    Uri = videoPath
                };
                //Bind URI to video Controller
                videoPlayer.Source = uriVideoSource;
                Selectedvideofile = selectedvideoFile;
                thumbnailImage = thumbnail;
                //list is visible false
                AddressList.IsVisible = false;
                PostLbl.IsEnabled = false;
                //Add topic to combolist
                var topicsList = new List<Topics>();
                var alltopicsData = App.DBIni.TopicsDB.GetTopicsData();
                if (alltopicsData != null)
                {
                    foreach (var topicInfo in alltopicsData)
                    {
                        Topics addTopic = new Topics();
                        addTopic.TopicName = topicInfo.TopicName;
                        topicsList.Add(addTopic);
                    }
                    TopicsComboBox.DataSource = topicsList;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateVideoPost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                Navigation.PushAsync(new Post());
            }
        }

        
        void videoPlayer_PlayCompletion(System.Object sender, System.EventArgs e)
        {
        }

        async void PostLblClick(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    //To add loader on click event 
                    SfBusyIndicator busyIndicator = new SfBusyIndicator()
                    {
                        AnimationType = AnimationTypes.Cupertino,
                        ViewBoxHeight = 50,
                        ViewBoxWidth = 50,
                        EnableAnimation = true,
                        Title = "Loading...",
                        TextSize = 12,
                        FontFamily = "Cabin",
                        TextColor = Color.FromHex("#9900CC")
                    };
                    this.Content = busyIndicator;

                    #region If passing value is null or empty
                    if (string.IsNullOrEmpty(LocationText.Text))
                    {
                        LocationText.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TopicsComboBox.Text))
                    {
                        TopicsComboBox.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TagLbl.Text))
                    {
                        TagLbl.Text = "-";
                    }
                    //if (string.IsNullOrEmpty(hashtagTxt.Text))
                    //{
                    //    hashtagTxt.Text = "-";
                    //}

                    #endregion

                    #region push image in s3
                    if (videoPath != null)
                    {
                        string filename = Path.GetFileName(videoPath);
                        //Replace space beween the filename with +
                        string UserFileName = filename.Replace(" ", "+");
                        //string thumbnailFileName = "Thumbnail_" + UserFileName;
                        try
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            #region video upload on s3 bucket
                            if (current == NetworkAccess.Internet)
                            {
                                var putRequest2 = new PutObjectRequest
                                {
                                    BucketName = PostbucketName,
                                    Key = UserFileName,
                                    ContentType = "application/mp4",
                                    InputStream = Selectedvideofile.GetStream(),
                                    CannedACL = S3CannedACL.PublicReadWrite
                                };
                                //To get the video URL
                                UserUploadVideoURL = "https://serenapostimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                                putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                                PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                            }
                            else
                                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                            #endregion
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            if (amazonS3Exception.ErrorCode != null &&
                                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                                ||
                                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                            {
                                throw new Exception("Check the provided AWS Credentials.");
                            }
                            else
                            {
                                throw new Exception("Error occurred: " + amazonS3Exception.Message);
                            }
                        }
                    }
                    #endregion

                    #region create GUID for post
                    Guid guid = Guid.NewGuid();
                    string Guidstr = guid.ToString();
                    Guidstr = (guid + sessionEmailId);
                    if (string.IsNullOrEmpty(Guidstr))
                    {
                        Guidstr = "-";
                    }
                    #endregion

                    #region add data in SQLite Post

                    post.UserEmailId = sessionEmailId;
                    post.Description = descriptionLbl.Text;
                    post.ShareType = "-";
                    post.Location = LocationText.Text;
                    post.Topics = TopicsComboBox.Text;
                    post.ImageUrl = UserUploadVideoURL;
                    post.Tags = TagLbl.Text;
                    post.HashTags = "-";
                    post.LastCreated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.LastUpdated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.TimeOfPost = DateTime.Now.ToString("h:mm tt");
                    post.PostBackgroundColor = Color.Black.ToHex();
                    post.PostType = "Video";
                    post.Guid = Guidstr;
                    App.DBIni.SerenaPostDB.AddPost(post);

                    #endregion

                    #region Mysql DB API 
                    if (current == NetworkAccess.Internet)
                    {
                        var serenaDataSync = new SerenaDataSync();
                        await serenaDataSync.SavePost(post);
                        await Navigation.PushAsync(new Post());
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }

            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateVideoPost.Xaml";
                addlogFile.ExceptionEventName = "PostLblClick";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        //Description change event
        private void DescriptionTextChange(object sender, EventArgs e)
        {
            //Tf description is not null or empty then activate post label
            if (!string.IsNullOrEmpty(descriptionLbl.Text))
            {
                PostLbl.TextColor = Color.FromHex("#9900CC");
                PostLbl.IsEnabled = true;
            }
            else
            {
                PostLbl.TextColor = Color.FromHex("#A6A6A6");
                PostLbl.IsEnabled = false;
            }
        }
        //Clear Button event
        private void ClearBtnClicked(object sender, EventArgs e)
        {
            LocationText.Text = "";
            AddressList.ItemsSource = null;
            //list is visible false
            AddressList.IsVisible = false;
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                AddressList.ItemsSource = "";
                var PlaceAddress = (Xamarin.Forms.Label)sender;
                LocationText.Text = PlaceAddress.Text;
                //list visible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateVideoPost.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(LocationText.Text))
                {
                    //list visible false
                    AddressList.IsVisible = true;
                    await GetPlacesPredictionsAsync();
                    AddressList.ItemsSource = Addresses;

                }
                else if (string.IsNullOrEmpty(LocationText.Text))
                {
                    //list visible false
                    AddressList.IsVisible = false;
                    AddressList.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateVideoPost.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {
            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(LocationText.Text))))
                    { //Be sure to UrlEncode the search term they enter

                        using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                        {
                            if (message.IsSuccessStatusCode)
                            {
                                string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                                PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                                if (predictionList.Status == "OK")
                                {
                                    Addresses.Clear();

                                    if (predictionList.Predictions.Count > 0)
                                    {
                                        foreach (Prediction prediction in predictionList.Predictions)
                                        {
                                            Addresses.Add(new AddressInfo
                                            {
                                                Address = prediction.Description
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    AddressList.ItemsSource = null;
                                }
                            }
                        }
                    }

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateVideoPost.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}