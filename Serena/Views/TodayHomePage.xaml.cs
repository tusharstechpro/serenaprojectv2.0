﻿using Newtonsoft.Json;
using Plugin.Toast;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Data;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodayHomePage : ContentPage
    {
        string sessionEmailId;
        string unfollowUserEmailId;
        UserProfileData_Tran profileDataTran = new UserProfileData_Tran();
        public TodayHomePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            try
            {
                LoadInitialData();
            }
            catch (Exception ex)
            {

            }
            //base.OnAppearing();
        }

        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }

        void moreIconClicked(object sender, EventArgs e)
        {
            try
            {
                //MenuPopup.Show();
                popupLoadingView.IsVisible = true;
                var menuButton = (Xamarin.Forms.ImageButton)sender;
                var unfollowUserName = menuButton.CommandParameter.ToString();
                var getEmailId = App.DBIni.UserProfileDataDB.GetSpecificUserByUserName(unfollowUserName);
                unfollowUserEmailId = getEmailId.UserEmailId;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "moreIconClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        private void ClearImageTapped(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = false;
        }

        #region Overlay menu click event
        void ReportLblClicked(object sender, EventArgs e)
        {
            //templateView = new DataTemplate(() =>
            //{
            //    ReportLbl = new Label();
            //    ReportLbl.Text = "Report";
            //    ReportLbl.TextColor = Color.FromHex("#D84C4C");
            //    ReportLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    FavoriteLbl.Text = "Favorite";
            //    FavoriteLbl.TextColor = Color.Black;
            //    FavoriteLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    CopylinkLbl.Text = "Copylink";
            //    CopylinkLbl.TextColor = Color.Black;
            //    CopylinkLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    UnfollowLbl.Text = "Unfollow";
            //    UnfollowLbl.TextColor = Color.Black;
            //    UnfollowLbl.HorizontalTextAlignment = TextAlignment.Center;

            //    return ReportLbl;
            //});

            //// Adding ContentTemplate of the SfPopupLayout
            //MenuPopup.PopupView.ContentTemplate = templateView;
        }
        void FavoriteLblClicked(object sender, EventArgs e)
        {

        }
        void CopylinkLblClicked(object sender, EventArgs e)
        {

        }
        async void UnfollowLblClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    bool answer = await DisplayAlert("", "You really want to unfollow this user?", "OK", "Cancel");
                    if (answer == true)
                    {

                        #region update status in UserProfile table in SQLite
                        profileDataTran.UserEmailId = sessionEmailId;
                        profileDataTran.RequestTo = unfollowUserEmailId;
                        profileDataTran.Status = 3;
                        App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "UnfollowLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void CommentImageTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new NavigationPage(new Reviews(selectedGuid, "TodaysHomePage", "")));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "CommentImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        #endregion

        #region Pull to refresh events

        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To get current emailId from session
                    sessionEmailId = Settings.GeneralSettings;
                    var postImage = new List<Serena_Post>();
                    var followingList = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(1);
                    foreach (var FollowingUser in followingList)
                    {
                        var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(FollowingUser.UserEmailId);

                        var postData = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(FollowingUser.UserEmailId);
                        foreach (var posts in postData)
                        {
                            if (posts.PostType == "Text")
                            {
                                posts.UserName = userProfileData.UserName;
                                posts.UserProfileImage = userProfileData.UserProfileImage;
                                posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                                #region TimeAgo
                                // Extension method for time ago
                                posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                #endregion
                                postImage.Add(posts);
                            }
                            else if (posts.PostType == "Image" && posts.PostText == "-")//Image
                            {
                                posts.UserName = userProfileData.UserName;
                                posts.UserProfileImage = userProfileData.UserProfileImage;
                                posts.PostBackgroundColor = Color.Black.ToHex();
                                posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                                #region TimeAgo
                                // Extension method for time ago
                                posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                #endregion
                                postImage.Add(posts);
                            }
                            else if (posts.PostType == "Image" && posts.PostText != "-")//Image overlay
                            {
                                posts.UserName = userProfileData.UserName;
                                posts.UserProfileImage = userProfileData.UserProfileImage;
                                posts.PostBackgroundColor = Color.Black.ToHex();
                                posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                                #region TimeAgo
                                // Extension method for time ago
                                posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                #endregion
                                postImage.Add(posts);
                            }
                            #region PlacePost
                            //else if (posts.PostType == "Place")
                            //{
                            //    var userPlacePost = new HomepagePost();
                            //    userPlacePost.UserName = userProfileData.UserName;
                            //    userPlacePost.UserProfileImage = userProfileData.UserProfileImage;
                            //    userPlacePost.Location = posts.Location;
                            //    userPlacePost.Description = posts.Description;
                            //    userPlacePost.Tags = posts.Tags;
                            //    userPlacePost.Guid = posts.Guid;

                            //    userPlacePost.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                            //    userPlacePost.LastCreated = posts.LastCreated;//added on 2/11/2020 to display post list descending
                            //                                                  //userPlacePost.FavCount = posts.FavCount;//added on 2/11/2020 to display like count

                            //    #region TimeAgo

                            //    //DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(posts.LastCreated) ?
                            //    //  posts.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                            //    //  CultureInfo.InvariantCulture);

                            //    // Extension method for time ago
                            //    userPlacePost.TimeOfPost = TimeAgo(posts.LastCreated);

                            //    #endregion

                            //    Pin pin = new Pin
                            //    {
                            //        Type = PinType.Place,
                            //        Position = new Position(posts.Latitude, posts.Longitude),
                            //        Address = posts.PlaceAddress,
                            //        Label = "Address"
                            //    };
                            //    userPlacePost.MapPosition = new Position(posts.Latitude, posts.Longitude);
                            //    userPlacePost.MapPin = pin;
                            //    postImage.Add(userPlacePost);
                            //}
                            #endregion
                        }
                    }
                    HomePostList.ItemsSource = postImage;
                    if (postImage.Count == 0) { HomePostList.IsVisible = false; }
                    else { HomePostList.IsVisible = true; }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        void likeImageTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    int tapCount = 0;
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    if (selectedImage.File.ToLower() == "likeicon.png")
                        tapCount++;
                    else if (selectedImage.File.ToLower() == "PurpleHeart.png")
                        tapCount = 0;

                    if (tapCount % 2 == 0)//For dislike
                    {
                        imageSource.Source = "LikeIcon.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 0);

                        #endregion region

                        #region for dislike post
                        if (current == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();

                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 0;
                            _ = serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                        #endregion
                    }
                    else// For like
                    {
                        imageSource.Source = "PurpleHeart.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 1);

                        #endregion region

                        #region for like post  
                        if (current == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();

                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 1;
                            _ = serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                        #endregion

                        #region like count
                        //HomePostList.ItemTemplate = new DataTemplate(() => {
                        //    var grid = new Grid();
                        //    var name = new Label { FontSize = 14 };
                        //    name.SetBinding(Label.TextProperty, new Binding("FavCount"));
                        //    grid.Children.Add(name);
                        //    return grid;
                        //});
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "ImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        private async void UserNameLblTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var UserNameLbl = (Xamarin.Forms.Label)sender;
                    string followUserName = UserNameLbl.Text.ToString();
                    var userdata = App.DBIni.UserProfileDataDB.GetSpecificUserByUserName(followUserName);
                    //await Navigation.PushModalAsync(new NavigationPage(new FollowUsersPage(userdata.UserEmailId)));
                    await Navigation.PushAsync(new FollowUsersPage(userdata.UserEmailId));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "UserNameLblTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        private async void postTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var poststacklayout = (StackLayout)sender;
                    var item = (TapGestureRecognizer)poststacklayout.GestureRecognizers[0];
                    string selectedPostGuid = item.CommandParameter.ToString();
                    if (!string.IsNullOrEmpty(selectedPostGuid))
                    {
                        await Navigation.PushAsync(new OpenPost(selectedPostGuid));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "postTapped";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        #endregion

        private void RefreshBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (!string.IsNullOrEmpty(sessionEmailId))
                    {
                        var serenaDataSync = new SerenaDataSync();
                        Task.Run(() => serenaDataSync.GetSerenaUsersAsync()).Wait();
                        Task.Run(() => serenaDataSync.GetUsersdataAsync()).Wait();
                        Task.Run(() => serenaDataSync.GetSerenaPostAsync()).Wait();
                        Task.Run(() => serenaDataSync.GetSerenaUpcomingNotifications()).Wait();
                        //LoadInitialData(); commented 5th feb crashing issue fix
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "RefreshBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);

            }
        }
    }
}
