﻿using Serena.Data;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Syncfusion.SfBusyIndicator.XForms;
using Serena.DBService;
using Serena.Helpers;
using Plugin.Toast;
using Xamarin.Essentials;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginUser : ContentPage
    {
        CryptographyASE Crypto = new CryptographyASE();

        public LoginUser()
        {
            InitializeComponent();

        }

        //If User is new then navigate to signUpPage
        async void NewUserLblClicked(object sender, EventArgs e)
        {
            try
            {
                //await Navigation.PushAsync(new SignUpPage());
                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LoginUser.Xaml";
                addlogFile.ExceptionEventName = "NewUserLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        //Sign In Button Click event
        async void SignInClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                // Connection to internet is available
                if (current == NetworkAccess.Internet)
                {
                    //Empty field validations
                    if (string.IsNullOrEmpty(Email.Text) || string.IsNullOrEmpty(Password.Text))
                    {
                        if (string.IsNullOrEmpty(Email.Text))
                        {
                            input1.HasError = true;
                        }
                        if (string.IsNullOrEmpty(Password.Text))
                        {
                            input2.HasError = true;

                        }
                    }
                    else
                    {
                        //Get specific userdata by emailId from Serena_Users table  SQLite
                        var usersData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Email.Text);
                        //var postdata = signupDB.GetUsers();
                        if (usersData != null)
                        {

                            //Compare decrypted password and entered password 
                            string passToCompare = Crypto.Decrypt(usersData.PASSWORD);
                            if (string.IsNullOrEmpty(passToCompare))
                            {
                                input2.HasError = true;
                                input2.ErrorText = "Password Incorrect";
                            }

                            if (usersData.EMAIL == null)
                            {
                                input1.HasError = true;
                                input1.ErrorText = "There is no user registered with this Email";
                            }
                            else if (Password.Text != passToCompare)
                            {
                                input2.HasError = true;
                                input2.ErrorText = "Password Incorrect";
                            }
                            else if (Email.Text == usersData.EMAIL && passToCompare == Password.Text)
                            {
                                //To apply loading... as a busy indicator
                                SfBusyIndicator busyIndicator = new SfBusyIndicator()
                                {
                                    AnimationType = AnimationTypes.Cupertino,
                                    ViewBoxHeight = 50,
                                    ViewBoxWidth = 50,
                                    EnableAnimation = true,
                                    Title = "Loading...",
                                    TextSize = 12,
                                    FontFamily = "Cabin",
                                    TextColor = Color.FromHex("#9900CC")
                                };
                                this.Content = busyIndicator;

                                //add userEmailId to session
                                Settings.GeneralSettings = "";
                                Settings.GeneralSettings = Email.Text;

                                if (current == NetworkAccess.Internet)
                                {
                                    //New Code
                                    //var serenaDataSync = new SerenaDataSync();
                                    //Task.Run(() => serenaDataSync.GetUsersdataAsync()).Wait();
                                    //Task.Run(() => serenaDataSync.GetSerenaPostAsync()).Wait();
                                }
                                else
                                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                                //navigate to ProfileMasterPage
                                await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                            }
                        }
                        else
                        {
                            input1.HasError = true;
                            input1.ErrorText = "Invalid User";
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LoginUser.Xaml";
                addlogFile.ExceptionEventName = "SignInClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //EmailTextChange event
        private void EmailTextChanged(object sender, EventArgs e)
        {
            input1.HasError = false;
            input1.ErrorText = "";
        }
        //PasswordTextChange event
        private void PasswordTextChanged(object sender, EventArgs e)
        {
            input2.HasError = false;
            input2.ErrorText = "";
        }
    }
}