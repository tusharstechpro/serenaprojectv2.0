﻿using Serena.DBService;
using Serena.Model;
using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Plugin.Toast;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenPost : ContentPage
    {
        public OpenPost(string postGuid)
        {
            InitializeComponent();
            try
            {

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var postDetails = App.DBIni.SerenaPostDB.GetSpecificPostByGuid(postGuid);
                    if (postDetails != null)
                    {
                        var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(postDetails.UserEmailId);
                        RecentPostFrame.IsVisible = true;
                        if (postDetails.PostType == "Text")//textpost
                        {
                            ImagePostLayout.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo

                            //DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(postDetails.LastCreated) ?
                            //  postDetails.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                            //  CultureInfo.InvariantCulture);

                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(postDetails.LastCreated);

                            #endregion

                            RecentPostImg.IsVisible = false;
                            PostTextLbl.Text = postDetails.PostText;
                            var textcolor = postDetails.PostColor.Replace("#FF", "#");
                            PostTextLbl.TextColor = Color.FromHex(textcolor);
                            UserLocationLbl.Text = postDetails.Location;
                            var bgcolor = postDetails.PostBackgroundColor.Replace("#FF", "#");
                            TextPostLayout.BackgroundColor = Color.FromHex(bgcolor);
                            DescriptionLbl.Text = postDetails.Description;
                            TagsLbl.Text = postDetails.Tags;

                            //Favourite Functionality Code
                            guid.Text = postDetails.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) ? Convert.ToString(postDetails.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) && Convert.ToString(postDetails.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                        }
                        else if (postDetails.PostType == "Image" && postDetails.PostText == "-")//Imagepost
                        {
                            TextPostLayout.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo

                            //DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(postDetails.LastCreated) ?
                            //  postDetails.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                            //  CultureInfo.InvariantCulture);

                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(postDetails.LastCreated);

                            #endregion

                            RecentPostImg.Source = postDetails.ImageUrl;
                            UserLocationLbl.Text = postDetails.Location;
                            DescriptionLbl.Text = postDetails.Description;
                            TagsLbl.Text = postDetails.Tags;
                            RecentPostImg.Opacity = postDetails.Opacity;
                            //Favourite Functionality Code
                            guid.Text = postDetails.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) ? Convert.ToString(postDetails.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) && Convert.ToString(postDetails.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                            //TextReflectLbl.Text = data.PostText;
                            //var textcolor = data.PostColor.Replace("#FF", "#");
                            //TextReflectLbl.TextColor = Color.FromHex(textcolor);
                        }
                        else if (postDetails.PostType == "Image" && postDetails.PostText != "-")//Image text overlay
                        {
                            TextPostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo

                            //DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(postDetails.LastCreated) ?
                            //  postDetails.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                            //  CultureInfo.InvariantCulture);

                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(postDetails.LastCreated);

                            #endregion

                            RecentPostImg.Source = postDetails.ImageUrl;
                            UserLocationLbl.Text = postDetails.Location;
                            DescriptionLbl.Text = postDetails.Description;
                            TagsLbl.Text = postDetails.Tags;
                            TextReflectLbl.Text = postDetails.PostText;
                            var textcolor = postDetails.PostColor.Replace("#FF", "#");
                            TextReflectLbl.TextColor = Color.FromHex(textcolor);
                            TextReflectLbl.FontFamily = "Cabin";
                            RecentPostImg.Opacity = postDetails.Opacity;
                            //Favourite Functionality Code
                            guid.Text = postDetails.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) ? Convert.ToString(postDetails.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) && Convert.ToString(postDetails.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                        }
                        else if (postDetails.PostType == "Place")//Place post
                        {
                            TextPostLayout.IsVisible = false;
                            ImagePostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo

                            //DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(postDetails.LastCreated) ?
                            //  postDetails.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                            //  CultureInfo.InvariantCulture);

                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(postDetails.LastCreated);

                            #endregion

                            UserLocationLbl.Text = postDetails.Location;
                            DescriptionLbl.Text = postDetails.Description;
                            //Favourite Functionality Code
                            guid.Text = postDetails.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) ? Convert.ToString(postDetails.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(postDetails.Favourite)) && Convert.ToString(postDetails.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                            TagsLbl.Text = postDetails.Tags;
                            //SetMapCordinates();
                            Position loc1 = new Position(postDetails.Latitude, postDetails.Longitude);
                            Pin marker1 = new Pin()
                            {
                                Address = postDetails.PlaceAddress,
                                Label = "Address",
                                Position = loc1,
                                Type = PinType.Place
                            };

                            map.Pins.Add(marker1);
                            map.MoveToRegion(MapSpan.FromCenterAndRadius(marker1.Position, Distance.FromMeters(1000)));
                        }

                        #region postReviews
                        var serenaDataSync = new SerenaDataSync();
                        Task.Run(() => serenaDataSync.GetSerenaReviews(postGuid)).Wait();
                        var guidReviews = App.DBIni.ReviewsDB.GetAllReviewsByGuid(postGuid);
                        if (guidReviews != null)
                            reviewList.ItemsSource = guidReviews;
                        #endregion
                    }
                    else
                    {
                        RecentPostFrame.IsVisible = false;
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "OpenPost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }
    }
}