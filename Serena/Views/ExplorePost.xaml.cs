﻿using Newtonsoft.Json;
using Plugin.Toast;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.SfPullToRefresh.XForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Markup;
using Xamarin.Forms.Xaml;
using Position = Xamarin.Forms.Maps.Position;
using Serena.Data;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExplorePost : ContentPage
    {
        string sessionEmailId;
        string exploreHashTag;

        public ExplorePost(string selectedHashTag)
        {
            InitializeComponent();

            try
            {
                pullToRefresh.Pulling += PullToRefresh_Pulling;
                pullToRefresh.Refreshing += PullToRefresh_Refreshing;

                lblResults.Text = "Results";
                lblShowResults.Text = "Showing results for " + selectedHashTag;
                exploreHashTag = selectedHashTag;

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    LoadInitialData();
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExplorePost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }

        void moreIconClicked(object sender, EventArgs e)
        {
            MenuPopup.Show();
        }

        #region Overlay menu click event
        void ReportLblClicked(object sender, EventArgs e)
        {
            //templateView = new DataTemplate(() =>
            //{
            //    ReportLbl = new Label();
            //    ReportLbl.Text = "Report";
            //    ReportLbl.TextColor = Color.FromHex("#D84C4C");
            //    ReportLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    FavoriteLbl.Text = "Favorite";
            //    FavoriteLbl.TextColor = Color.Black;
            //    FavoriteLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    CopylinkLbl.Text = "Copylink";
            //    CopylinkLbl.TextColor = Color.Black;
            //    CopylinkLbl.HorizontalTextAlignment = TextAlignment.Center;
            //    UnfollowLbl.Text = "Unfollow";
            //    UnfollowLbl.TextColor = Color.Black;
            //    UnfollowLbl.HorizontalTextAlignment = TextAlignment.Center;

            //    return ReportLbl;
            //});

            //// Adding ContentTemplate of the SfPopupLayout
            //MenuPopup.PopupView.ContentTemplate = templateView;
        }
        void FavoriteLblClicked(object sender, EventArgs e)
        {

        }
        void CopylinkLblClicked(object sender, EventArgs e)
        {

        }
        void UnfollowLblClicked(object sender, EventArgs e)
        {

        }


        async void CommentImageTapped(object sender, EventArgs e)
        {
            try
            {

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new NavigationPage(new Reviews(selectedGuid, "ExplorePost", exploreHashTag)));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Explore.Xaml";
                addlogFile.ExceptionEventName = "CommentImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        #endregion

        #region Pull to refresh events

        private void LoadInitialData()
        {
            try
            {
                //To get current emailId from session
                sessionEmailId = Settings.GeneralSettings;
                var postImage = new List<HomepagePost>();
                var userprofileInfo = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                if (userprofileInfo != null)
                {
                    //Added by Vishal to get post data from aws on page load
                    if (!string.IsNullOrEmpty(sessionEmailId))
                    {

                        //delete records from PostsByHashTag
                        App.DBIni.SerenaPostDB.DeletePostByHashTag();
                        var current = Connectivity.NetworkAccess;
                        if (current == NetworkAccess.Internet)
                        {
                            SerenaDataSync Data = new SerenaDataSync();
                            var hashTagPostDsata = App.DBIni.SerenaPostDB.GetAllPostByHashTags();
                            //Adding selected hastag posts to PostsByHashTag table
                            Task.Run(() => Data.GetSerenaExplorePostsByHashTag(exploreHashTag)).Wait();
                        }
                    }

                    var hashTagPostData = App.DBIni.SerenaPostDB.GetAllPostByHashTags();
                    if (hashTagPostData != null)
                    {
                        //All records from filter hashtags
                        foreach (var posts in hashTagPostData)
                        {
                            if (posts.PostType == "Text")
                            {
                                var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(posts.UserEmailId);

                                var userImagePost = new HomepagePost();
                                userImagePost.UserName = userProfileData != null ? userProfileData.UserName : string.Empty;
                                userImagePost.UserProfileImage = userProfileData != null ? userProfileData.UserProfileImage : string.Empty;
                                userImagePost.Location = posts.Location;
                                userImagePost.PostText = posts.PostText;
                                userImagePost.PostColor = posts.PostColor;
                                userImagePost.PostBackgroundColor = posts.PostBackgroundColor;
                                userImagePost.Description = posts.Description;
                                userImagePost.Tags = posts.Tags + " " + posts.HashTags;
                                userImagePost.Location = posts.Location;
                                userImagePost.Guid = posts.Guid;
                                userImagePost.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "Like_Red.png";

                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(posts.LastCreated) ?
                                  posts.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                // Extension method for time ago
                                userImagePost.TimeOfPost = TimeAgo(convertToDatetime);

                                #endregion

                                postImage.Add(userImagePost);
                            }
                            else if (posts.PostType == "Image" && posts.PostText == "-")//Image
                            {
                                var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(posts.UserEmailId);

                                var userImagePost = new HomepagePost();
                                userImagePost.UserName = userProfileData != null ? userProfileData.UserName : string.Empty;
                                userImagePost.UserProfileImage = userProfileData != null ? userProfileData.UserProfileImage : string.Empty;
                                userImagePost.Location = posts.Location;
                                userImagePost.ImageUrl = posts.ImageUrl;
                                userImagePost.PostBackgroundColor = Color.Black.ToHex();
                                userImagePost.Opacity = posts.Opacity;
                                userImagePost.Description = posts.Description;
                                userImagePost.Tags = posts.Tags + " " + posts.HashTags;
                                userImagePost.Location = posts.Location;
                                userImagePost.Guid = posts.Guid;
                                userImagePost.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "Like_Red.png";

                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(posts.LastCreated) ?
                                  posts.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                // Extension method for time ago
                                userImagePost.TimeOfPost = TimeAgo(convertToDatetime);

                                #endregion

                                postImage.Add(userImagePost);
                            }
                            else if (posts.PostType == "Image" && posts.PostText != "-")//Image overlay
                            {
                                var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(posts.UserEmailId);

                                var userImagePost = new HomepagePost();
                                userImagePost.UserName = userProfileData != null ? userProfileData.UserName : string.Empty;
                                userImagePost.UserProfileImage = userProfileData != null ? userProfileData.UserProfileImage : string.Empty;
                                userImagePost.Location = posts.Location;
                                userImagePost.ImageUrl = posts.ImageUrl;
                                userImagePost.PostText = posts.PostText;
                                userImagePost.PostBackgroundColor = Color.Black.ToHex();
                                userImagePost.PostColor = posts.PostColor;
                                userImagePost.Opacity = posts.Opacity;
                                userImagePost.Description = posts.Description;
                                userImagePost.Tags = posts.Tags + " " + posts.HashTags;
                                userImagePost.Guid = posts.Guid;

                                userImagePost.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "Like_Red.png";

                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(posts.LastCreated) ?
                                  posts.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                // Extension method for time ago
                                userImagePost.TimeOfPost = TimeAgo(convertToDatetime);

                                #endregion

                                postImage.Add(userImagePost);
                            }
                            else if (posts.PostType == "Place")
                            {
                                var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(posts.UserEmailId);

                                var userPlacePost = new HomepagePost();
                                userPlacePost.UserName = userProfileData != null ? userProfileData.UserName : string.Empty;
                                userPlacePost.UserProfileImage = userProfileData != null ? userProfileData.UserProfileImage : string.Empty;
                                userPlacePost.Location = posts.Location;
                                userPlacePost.Description = posts.Description;
                                userPlacePost.Tags = posts.Tags + " " + posts.HashTags;
                                userPlacePost.Guid = posts.Guid;

                                userPlacePost.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "Like_Red.png";

                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(posts.LastCreated) ?
                                  posts.LastCreated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                // Extension method for time ago
                                userPlacePost.TimeOfPost = TimeAgo(convertToDatetime);

                                #endregion

                                Pin pin = new Pin
                                {
                                    Type = PinType.Place,
                                    Position = new Position(posts.Latitude, posts.Longitude),
                                    Address = posts.PlaceAddress,
                                    Label = "Address"
                                };
                                userPlacePost.MapPosition = new Position(posts.Latitude, posts.Longitude);
                                userPlacePost.MapPin = pin;
                                postImage.Add(userPlacePost);
                            }
                        }
                    }
                }
                HomePostList.ItemsSource = postImage;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExplorePost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void PullToRefresh_Pulling(object sender, PullingEventArgs args)
        {
            args.Cancel = false;
            var progress = args.Progress;
        }
        private async void PullToRefresh_Refreshing(object sender, EventArgs args)
        {
            pullToRefresh.IsRefreshing = true;
            sessionEmailId = Settings.GeneralSettings;
            //LoadInitialData();

            await Navigation.PushAsync(new ExplorePost(exploreHashTag));
            pullToRefresh.IsRefreshing = false;
        }


        void likeImageTapped(object sender, EventArgs e)
        {
            try
            {

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    int tapCount = 0;
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    if (selectedImage.File.ToLower() == "likeicon.png")
                        tapCount++;
                    else if (selectedImage.File.ToLower() == "like_red.png")
                        tapCount = 0;

                    if (tapCount % 2 == 0)
                    {
                        imageSource.Source = "LikeIcon.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 0);

                        #endregion region

                        #region for dislike post

                       
                        var current1 = Connectivity.NetworkAccess;
                        if (current1 == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();
                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 0;
                            serenaDataSync.PostSerenaFavourite(post);
                        }

                        #endregion
                    }
                    else
                    {
                        imageSource.Source = "Like_Red.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 1);

                        #endregion region

                        #region for like post  
                        var current1 = Connectivity.NetworkAccess;
                        if (current1 == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();

                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 1;
                            serenaDataSync.PostSerenaFavourite(post);
                        }
                        #endregion
                    }

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayHomePage.Xaml";
                addlogFile.ExceptionEventName = "ImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void lblResultsClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Explore());
        }

        #endregion

    }
}
