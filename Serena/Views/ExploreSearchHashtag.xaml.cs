﻿using Serena.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Plugin.Toast;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExploreSearchHashtag : ContentPage
    {
        Tophashtags tophashtags = new Tophashtags();
        public ExploreSearchHashtag(string selectedHashTag)
        {
            try
            {
                InitializeComponent();

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    #region searched hashtags

                    lblResults.Text = "Hashtags";
                    lblShowResults.Text = "Showing results for " + selectedHashTag;


                    var hashtag = App.DBIni.TophashtagDB.GetTophashtagData().Where(x => x.Hashtags == selectedHashTag);
                    if (hashtag != null)
                    {
                        var addtophashtag = new List<Tophashtags>();
                        foreach (var hashtags in hashtag)
                        {
                            var addhashtag = new Tophashtags();
                            addhashtag.Hashtags = hashtags.Hashtags;
                            addhashtag.Count = hashtags.Count + " tags  > ";
                            addhashtag.Img1 = hashtags.Img1;
                            addhashtag.Img2 = hashtags.Img2;
                            addhashtag.Img3 = hashtags.Img3;
                            addtophashtag.Add(addhashtag);
                        }
                        hashtaglist.ItemsSource = addtophashtag;
                    }
                    else
                        hashtaglist.ItemsSource = "";

                    #region visibility 

                    hashtaglist.IsVisible = true;
                    lblTopHashTagsTitle.IsVisible = true;

                    #endregion

                    #endregion

                    #region people
                    lblPeople.IsVisible = true;
                    var topPeople = App.DBIni.TophashtagDB.GetTopPeopleData();
                    if (topPeople.Count() > 0)
                        listview.ItemsSource = topPeople;
                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExploreSearchHashtag.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void Rating_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var test = "Rating :" + " " + e.NewValue + " /5";
        }

        async void explorePostClicked(object sender, EventArgs e)
        {
            //var labelExplorePost = (Label)sender;
            //var item = (TapGestureRecognizer)labelExplorePost.GestureRecognizers[0];
            //string selectedHashTag = item.CommandParameter.ToString();

            //if (!string.IsNullOrEmpty(Settings.GeneralSettings))
            //    await Navigation.PushAsync(new ExplorePost(selectedHashTag));
            //else
            //{
            //    bool value = await DisplayAlert("", "You can only see the hashtag posts as a member", "OK", "Cancel");
            //    if (value == true)
            //        await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

            //}
        }
        async void lblResultsClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Explore());
        }
    }
}