﻿using Newtonsoft.Json;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using Syncfusion.XForms.Buttons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationPage : ContentPage
    {
        
        public LocationPage()
        {
            InitializeComponent();
            SaveBtn.IsVisible = false;
        }

        //Switch Button Chnaged Event
        private void SfSwitch_StateChanged(object sender, SwitchStateChangedEventArgs e)
        {
            //If location switch is on then NextButton, SkipButton is not visible and SaveButton is visible
            if (LocationSwitch.IsOn == true)
            {
                NEXTBtn.IsVisible = false;
                SkipBtn.IsVisible = false;
                SaveBtn.IsVisible = true;
            }
            //If location switch is on then NextButton, SkipButton is visible and SaveButton is not visible
            else
            {
                NEXTBtn.IsVisible = true;
                SkipBtn.IsVisible = true;
                SaveBtn.IsVisible = false;
            }


        }
        void NextBtnClicked(object sender, System.EventArgs e)
        {

        }

        //If Skip Button clicked then NextButton, SkipButton is not visible and SaveButton is visible
        void SkipBtnClicked(object sender, System.EventArgs e)
        {
            NEXTBtn.IsVisible = false;
            SkipBtn.IsVisible = false;
            SaveBtn.IsVisible = true;

        }

        //Save Button clicked event
        private async void SaveBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                SfBusyIndicator busyIndicator = new SfBusyIndicator()
                {
                    AnimationType = AnimationTypes.Cupertino,
                    ViewBoxHeight = 50,
                    ViewBoxWidth = 50,
                    EnableAnimation = true,
                    Title = "Loading...",
                    TextSize = 12,
                    FontFamily = "Cabin",
                    TextColor = Color.FromHex("#9900CC")

                };

                this.Content = busyIndicator;

                //Get UserProfeData from Profile Page
                UserProfileData sessionProfilePage = JsonConvert.DeserializeObject<UserProfileData>(UserProfileSettings.ProfileSettings);

                //If UserData is not null
                if (sessionProfilePage != null)
                {
                    App.DBIni.UserProfileDataDB.AddUser(sessionProfilePage);
                    //Get specific user data from EmailId
                    var data = App.DBIni.UserProfileDataDB.GetUsers().Where(x => x.UserEmailId == sessionProfilePage.UserEmailId).FirstOrDefault();
                    if (data != null)
                    {
                        ////Set Id to profile model
                        //sessionProfilePage.Id = data.Id;
                        ////Add UserProfileData to sqlite
                        //var retrunvalue = profileDB.AddUser(sessionProfilePage);


                        ////Saving data in Serena_UserProfile table of DynamoDB using API
                        //Uri u = new Uri("https://5hm7gzgyjc.execute-api.us-east-2.amazonaws.com/test/serenauserprofile");

                        //var payload = new Dictionary<string, string>
                        //    {
                        //    {"Id", data.Id.ToString()},
                        //    {"UserAccountType", sessionProfilePage.UserAccountType},
                        //    {"UserAgeRange", sessionProfilePage.UserAgeRange },
                        //    {"UserBio", sessionProfilePage.UserBio},
                        //    {"UserEmailId", sessionProfilePage.UserEmailId},
                        //    {"UserFollowerJson", sessionProfilePage.UserFollowerJson},
                        //    {"UserFollowingJson",  sessionProfilePage.UserFollowingJson},
                        //    {"UserLocation", sessionProfilePage.UserLocation},
                        //    {"UserPostJson", sessionProfilePage.UserPostJson},
                        //    {"UserProfileImage", sessionProfilePage.UserProfileImage},
                        //    {"UserWebsite", sessionProfilePage.UserWebsite},
                        //    };
                        //string strPayload = JsonConvert.SerializeObject(payload);
                        //HttpContent c = new StringContent(strPayload, Encoding.UTF8, "application/json");
                        //var t = Task.Run(() => SendURI(u, c));
                        //if (t.Result != "")
                        //{
                        //    Console.WriteLine(t.Result);
                        //    Console.ReadLine();
                        //    this.IsBusy = false;
                        //    string test = Settings.GeneralSettings;

                        //    //Clear session
                        //    UserProfileSettings.ProfileSettings = "";

                        //    //Navigate to Profile()
                        //    await Navigation.PushAsync(new Profile());
                        //}
                        #region add data in userProfile table in dynamo DB and SQLite
                        //Add UserProfileData to sqlite
                        var retrunvalue = App.DBIni.UserProfileDataDB.AddUser(sessionProfilePage);

                        Uri userProfieURI = new Uri("https://i3g5872fhi.execute-api.us-east-2.amazonaws.com/serena/updateuserprofilerds");

                        var userProfilePayload = new Dictionary<string, string>
                            {
                            {"UserEmailId", sessionProfilePage.UserEmailId},
                            {"UserProfileImage", sessionProfilePage.UserProfileImage},
                             {"UserAccountType", sessionProfilePage.UserAccountType},
                            {"UserBio", sessionProfilePage.UserBio},
                            {"UserWebsite", sessionProfilePage.UserWebsite},
                             {"UserLocation", sessionProfilePage.UserLocation},
                            {"UserAgeRange", sessionProfilePage.UserAgeRange},
                            {"UserName", sessionProfilePage.UserName},
                            //{"TrequestStatus", sessionProfilePage.TrequestStatus},
                            };

                        string stringPayload = JsonConvert.SerializeObject(userProfilePayload);
                        HttpContent content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                        var task = Task.Run(() => SendURI(userProfieURI, content));
                        Console.WriteLine(task.Result);
                        Console.ReadLine();
                        if (task.Result != "")
                        {
                            this.IsBusy = false;
                            string test = Settings.GeneralSettings;
                            //Clear session
                            UserProfileSettings.ProfileSettings = "";
                            //Navigate to Profile()
                            await Navigation.PushAsync(new Profile());
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LocationPage.Xaml";
                addlogFile.ExceptionEventName = "SaveBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Profile());
            }
        }

        //Code for API response
        static async Task<string> SendURI(Uri u, HttpContent c)
        {
            var response = string.Empty;
            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = u,
                    Content = c
                };
                try
                {
                    HttpResponseMessage result = await client.SendAsync(request);
                    if (result.IsSuccessStatusCode)
                    {
                        //Success Status Code
                        response = result.StatusCode.ToString();
                    }
                    else
                    {
                        throw new InvalidOperationException("403 (Forbidden)");
                    }

                }
                catch (HttpRequestException exception)
                {
                    if (exception.Message.Contains("401 (Unauthorized)"))
                    {

                    }
                    else if (exception.Message.Contains("403 (Forbidden)"))
                    {

                    }
                }

            }
            return response;
        }

    }
}