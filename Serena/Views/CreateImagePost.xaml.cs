﻿using Amazon.S3;
using Amazon.S3.Model;
using MultiImagePicker.Services;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Serena.DBService;
using Serena.Helpers;
using Serena.ImagePickers.Android;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Serena.Model.Address;
using static Serena.Data.Constants;
using System.Net;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateImagePost : ContentPage
    {
        Serena_Post post = new Serena_Post();
        string sessionEmailId;
        string UserProfileImageURL = "";
        MediaFile selectedImageFile;

        [Obsolete]
        public CreateImagePost(MediaFile selectedimagefile, string PostText, Color PostTextColor, double setOpacity)
        {
            InitializeComponent();
            try
            {
                LoadInitialData(selectedimagefile, PostText, PostTextColor, setOpacity);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void LoadInitialData(MediaFile selectedimagefile, string PostText, Color PostTextColor, double setOpacity)
        {
            try
            {
                //Get the login user EmailId from Session
                sessionEmailId = Settings.GeneralSettings;
                //Get image from PostImagePage
                selectedImageFile = selectedimagefile;
                PostingImage.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                PostingImage.Opacity = setOpacity;
                TextReflectLbl.Text = PostText;
                TextReflectLbl.FontSize = 20;
                TextReflectLbl.TextColor = PostTextColor;

                Device.BeginInvokeOnMainThread(async () => await AskForPermissions());
                PostLbl.IsEnabled = false;


                //Add topic to combolist
                var topicsList = new List<Topics>();
                var alltopicsData = App.DBIni.TopicsDB.GetTopicsData();
                if (alltopicsData != null)
                {
                    foreach (var topicInfo in alltopicsData)
                    {
                        Topics addTopic = new Topics();
                        addTopic.TopicName = topicInfo.TopicName;
                        topicsList.Add(addTopic);
                    }
                    TopicsComboBox.DataSource = topicsList;
                }

                //list is visible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //PostClick event
        async void PostLblClick(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To add Loading... on click event 
                    SfBusyIndicator busyIndicator = new SfBusyIndicator()
                    {
                        AnimationType = AnimationTypes.Cupertino,
                        ViewBoxHeight = 50,
                        ViewBoxWidth = 50,
                        EnableAnimation = true,
                        Title = "Loading...",
                        TextSize = 12,
                        FontFamily = "Cabin",
                        TextColor = Color.FromHex("#9900CC")
                    };
                    this.Content = busyIndicator;

                    #region If passing value is null or empty
                    if (string.IsNullOrEmpty(LocationText.Text))
                    {
                        LocationText.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TopicsComboBox.Text))
                    {
                        TopicsComboBox.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TagLbl.Text))
                    {
                        TagLbl.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TextReflectLbl.Text))
                    {
                        TextReflectLbl.Text = "-";
                        TextReflectLbl.TextColor = Color.Black;
                    }

                    #endregion

                    #region push image in s3

                    if (selectedImageFile != null)
                    {
                        string filename = Path.GetFileName(selectedImageFile.Path);
                        //Replace space beween the filename with +
                        string UserFileName = filename.Replace(" ", "+");

                        try
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            if (current == NetworkAccess.Internet)
                            {
                                var putRequest2 = new PutObjectRequest
                                {
                                    BucketName = PostbucketName,
                                    Key = UserFileName,
                                    ContentType = "image/jpeg",
                                    InputStream = selectedImageFile.GetStream(),
                                    CannedACL = S3CannedACL.PublicReadWrite
                                };
                                //To get the profile image URL
                                UserProfileImageURL = "https://serenapostimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                                putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                                PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                            }
                            else
                                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            if (amazonS3Exception.ErrorCode != null &&
                                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                                ||
                                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                            {
                                throw new Exception("Check the provided AWS Credentials.");
                            }
                            else
                            {
                                throw new Exception("Error occurred: " + amazonS3Exception.Message);
                            }
                        }
                    }


                    #endregion

                    #region create GUID for post
                    Guid guid = Guid.NewGuid();
                    string Guidstr = guid.ToString();
                    Guidstr = (guid + sessionEmailId);
                    if (string.IsNullOrEmpty(Guidstr))
                    {
                        Guidstr = "-";
                    }
                    #endregion

                    #region add data in SQLite Post

                    post.UserEmailId = sessionEmailId;
                    post.Description = descriptionLbl.Text;
                    post.ShareType = "-";
                    post.Location = LocationText.Text;
                    post.Topics = TopicsComboBox.Text;
                    post.ImageUrl = UserProfileImageURL;
                    post.Tags = TagLbl.Text;
                    post.HashTags = "-";
                    post.PostText = TextReflectLbl.Text;
                    post.PostColor = TextReflectLbl.TextColor.ToHex();
                    post.LastCreated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.LastUpdated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.TimeOfPost = DateTime.Now.ToString("h:mm tt");
                    post.PostBackgroundColor = "-";
                    post.Opacity = PostingImage.Opacity;
                    post.PostType = "Image";
                    post.Guid = Guidstr;
                    App.DBIni.SerenaPostDB.AddPost(post);

                    #endregion

                    #region Mysql DB API 
                    if (current == NetworkAccess.Internet)
                    {
                        var serenaDataSync = new SerenaDataSync();
                        await serenaDataSync.SavePost(post);
                        await Navigation.PushAsync(new Post());
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "PostLblClick";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        //Description change event
        private void DescriptionTextChange(object sender, EventArgs e)
        {
            //Tf description is not null or empty then activate post label
            if (!string.IsNullOrEmpty(descriptionLbl.Text))
            {
                PostLbl.TextColor = Color.FromHex("#9900CC");
                PostLbl.IsEnabled = true;
            }
            else
            {
                PostLbl.TextColor = Color.FromHex("#A6A6A6");
                PostLbl.IsEnabled = false;
            }
        }

        //Make sure Permissions are given to the users storage.
        [Obsolete]
        private async Task<bool> AskForPermissions()
        {
            try
            {
                await CrossMedia.Current.Initialize();

                var storagePermissions = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                var photoPermissions = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Photos);
                if (storagePermissions != Plugin.Permissions.Abstractions.PermissionStatus.Granted || photoPermissions != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage, Permission.Photos });
                    storagePermissions = results[Permission.Storage];
                    photoPermissions = results[Permission.Photos];
                }

                if (storagePermissions != Plugin.Permissions.Abstractions.PermissionStatus.Granted || photoPermissions != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    await DisplayAlert("Permissions Denied!", "Please go to your app settings and enable permissions.", "Ok");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("error. permissions not set. here is the stacktrace: \n" + ex.StackTrace);
                return false;
            }
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {
            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(LocationText.Text))))
                    { //Be sure to UrlEncode the search term they enter

                        using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                        {
                            if (message.IsSuccessStatusCode)
                            {
                                string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                                PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                                if (predictionList.Status == "OK")
                                {
                                    Addresses.Clear();

                                    if (predictionList.Predictions.Count > 0)
                                    {
                                        foreach (Prediction prediction in predictionList.Predictions)
                                        {
                                            Addresses.Add(new AddressInfo
                                            {
                                                Address = prediction.Description
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    AddressList.ItemsSource = null;
                                }
                            }
                        }
                    }

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(LocationText.Text))
                {
                    //list visible false
                    AddressList.IsVisible = true;
                    await GetPlacesPredictionsAsync();
                    AddressList.ItemsSource = Addresses;

                }
                else if (string.IsNullOrEmpty(LocationText.Text))
                {
                    //list visible false
                    AddressList.IsVisible = false;
                    AddressList.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    AddressList.ItemsSource = "";
                    var PlaceAddress = (Xamarin.Forms.Label)sender;
                    LocationText.Text = PlaceAddress.Text;
                    //list visible false
                    AddressList.IsVisible = false;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreateImagePost.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        private void ClearBtnClicked(object sender, EventArgs e)
        {
            LocationText.Text = "";
            AddressList.ItemsSource = null;
            //list is visible false
            AddressList.IsVisible = false;
        }
    }
}