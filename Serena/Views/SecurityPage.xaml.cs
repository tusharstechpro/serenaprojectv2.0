﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecurityPage : ContentPage
    {
        public SecurityPage()
        {
            InitializeComponent();
        }
    }
}