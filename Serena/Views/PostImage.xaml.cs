﻿using Plugin.Media.Abstractions;
using Serena.DBService;
using Serena.Model;
using Syncfusion.XForms.Buttons;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostImage : ContentPage
    {
        public string SelectedImage;
        MediaFile selectedimage;

        public PostImage(MediaFile selectedimagefile)
        {
            InitializeComponent();
            try
            {
                var SelectedImageFile = selectedimagefile;
                selectedimage = SelectedImageFile;
                if (SelectedImageFile != null)
                {
                    string filename = Path.GetFileName(SelectedImageFile.Path);
                    //Replace space beween the filename with +
                    string UserFileName = filename.Replace(" ", "+");

                    //if Profile Image is null
                    if (PostingImage == null)
                    {
                        DisplayAlert("Error", "Could not get the image, please try again.", "OK");
                        return;
                    }
                    //Set Profile Image
                    PostingImage.Source = ImageSource.FromStream(() => SelectedImageFile.GetStream());
                    PostingImage.Opacity = 1;
                    slider1.Value = 100;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostImage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Add text to image 
        private void AddTextChanged(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(TextChnageLbl.Text))
            {
                TextReflectLbl.Text = "";
                NextBtn.Text = "Skip";
            }
            else
            {
                TextReflectLbl.Text = TextChnageLbl.Text;
                TextReflectLbl.FontSize = 20;
                NextBtn.Text = "Next";
            }
        }

        //Navigate to CreateImagePost  on NextButton click
        private async void NextBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateImagePost(selectedimage, TextReflectLbl.Text, TextReflectLbl.TextColor, PostingImage.Opacity));
        }

        //Text color change event
        void TextColorClicked(object sender, System.EventArgs e)
        {
            //Get syncfusion chip control background color
            var chip = sender as SfChip;
            string bgcolor = chip.BackgroundColor.ToHex();
            //If selected color is Red
            if (bgcolor == "#FFEB5757")
            {
                TextReflectLbl.TextColor = Color.FromHex("#EB5757");
            }
            //If selected color is Orange
            else if (bgcolor == "#FFF2994A")
            {
                TextReflectLbl.TextColor = Color.FromHex("#F2994A");
            }
            //If selected color is Yellow
            else if (bgcolor == "#FFF2C94C")
            {
                TextReflectLbl.TextColor = Color.FromHex("#F2C94C");
            }
            //If selected color is Green 
            else if (bgcolor == "#FF219653")
            {
                TextReflectLbl.TextColor = Color.FromHex("#219653");
            }
            //If selected color is Blue
            else if (bgcolor == "#FF2F80ED")
            {
                TextReflectLbl.TextColor = Color.FromHex("#2F80ED");
            }
            //If selected color is Voilet
            else if (bgcolor == "#FF9B51E0")
            {
                TextReflectLbl.TextColor = Color.FromHex("#9B51E0");
            }
            //If selected color is Black
            else if (bgcolor == "#FF333333")
            {
                TextReflectLbl.TextColor = Color.FromHex("#333333");
            }
            //If selected color is White
            else if (bgcolor == "#FFFFFFFF")
            {
                TextReflectLbl.TextColor = Color.FromHex("#FFFFFF");
            }

        }
        private void OnSliderValueChanged(object sender, System.EventArgs e)
        {
            try
            {
                double additon = slider1.Value >= 90 && slider1.Value <= 100 ? slider1.Value : slider1.Value + 10.0;
                PostingImage.Opacity = ((additon >= 100 ? 100 : additon) / 100);
            }
            catch (Exception ex)
            {
                //PostingImage.Opacity = 1;
                //return;
            }
        }
    }
}