﻿
using Newtonsoft.Json;
using Plugin.Toast;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xam.Forms.VideoPlayer;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using Serena.Data;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Post : ContentPage
    {
        string sessionEmailId;
        public Post()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {

            try
            {
                LoadInitialData();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Post.Xaml";
                addlogFile.ExceptionEventName = "OnAppearing";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To get current emailId from session
                    sessionEmailId = Settings.GeneralSettings;
                    //To get all post of user by sessionEmailId from SQLite
                    var data = App.DBIni.SerenaPostDB.GetSpecificPost(sessionEmailId);
                    //To get userProfileData by sessionEmailId from SQLite
                    var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                    if (data != null)
                    {
                        RecentPostFrame.IsVisible = true;
                        RecentPostLbl.IsVisible = true;
                        ViewAllLbl.IsVisible = true;
                        if (data.PostType == "Text")//textpost
                        {
                            ImagePostLayout.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            VideoPostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo
                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(data.LastCreated);

                            #endregion

                            RecentPostImg.IsVisible = false;
                            PostTextLbl.Text = data.PostText;
                            var textcolor = data.PostColor.Replace("#FF", "#");
                            PostTextLbl.TextColor = Color.FromHex(textcolor);
                            UserLocationLbl.Text = data.Location;
                            var bgcolor = data.PostBackgroundColor.Replace("#FF", "#");
                            TextPostLayout.BackgroundColor = Color.FromHex(bgcolor);
                            DescriptionLbl.Text = data.Description;
                            //var tags = data.Tags + " " + data.HashTags;
                            TagsLbl.Text = data.Tags;

                            //Favourite Functionality Code
                            guid.Text = data.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) ? Convert.ToString(data.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) && Convert.ToString(data.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                        }
                        else if (data.PostType == "Image" && data.PostText == "-")//Imagepost
                        {
                            TextPostLayout.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            VideoPostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo
                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(data.LastCreated);

                            #endregion

                            RecentPostImg.Source = data.ImageUrl;
                            UserLocationLbl.Text = data.Location;
                            DescriptionLbl.Text = data.Description;
                            //var tags = data.Tags + " " + data.HashTags;
                            TagsLbl.Text = data.Tags;
                            RecentPostImg.Opacity = data.Opacity;
                            //Favourite Functionality Code
                            guid.Text = data.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) ? Convert.ToString(data.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) && Convert.ToString(data.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                        }
                        else if (data.PostType == "Image" && data.PostText != "-")//Image text overlay
                        {
                            TextPostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            VideoPostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo
                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(data.LastCreated);
                            #endregion

                            RecentPostImg.Source = data.ImageUrl;
                            UserLocationLbl.Text = data.Location;
                            DescriptionLbl.Text = data.Description;
                            var tags = data.Tags + " " + data.HashTags;
                            TagsLbl.Text = data.Tags;
                            TextReflectLbl.Text = data.PostText;
                            var textcolor = data.PostColor.Replace("#FF", "#");
                            TextReflectLbl.TextColor = Color.FromHex(textcolor);
                            TextReflectLbl.FontFamily = "Cabin";
                            RecentPostImg.Opacity = data.Opacity;
                            //Favourite Functionality Code
                            guid.Text = data.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) ? Convert.ToString(data.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) && Convert.ToString(data.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";

                        }
                        else if (data.PostType == "Place")//Place post
                        {
                            TextPostLayout.IsVisible = false;
                            ImagePostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            VideoPostLayout.IsVisible = false;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo
                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(data.LastCreated);

                            #endregion

                            UserLocationLbl.Text = data.Location;
                            DescriptionLbl.Text = data.Description;
                            //Favourite Functionality Code
                            guid.Text = data.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) ? Convert.ToString(data.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) && Convert.ToString(data.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                            TagsLbl.Text = data.Tags;
                            //SetMapCordinates();
                            Position loc1 = new Position(data.Latitude, data.Longitude);
                            Pin marker1 = new Pin()
                            {
                                Address = data.PlaceAddress,
                                Label = "Address",
                                Position = loc1,
                                Type = PinType.Place
                            };

                            map.Pins.Add(marker1);
                            map.MoveToRegion(MapSpan.FromCenterAndRadius(marker1.Position, Distance.FromMeters(1000)));
                        }
                        else if (data.PostType == "Video")//Video post
                        {
                            TextPostLayout.IsVisible = false;
                            ImagePostLayout.IsVisible = false;
                            PostTextLbl.IsVisible = false;
                            PlacePostLayout.IsVisible = false;
                            VideoPostLayout.IsVisible = true;
                            UserImg.Source = userProfileData.UserProfileImage;
                            UserNameLbl.Text = userProfileData.UserName;

                            #region TimeAgo
                            // Extension method for time ago
                            TimeLbl.Text = TimeAgo(data.LastCreated);

                            #endregion
                            UriVideoSource uriVideoSource = new UriVideoSource()
                            {
                                Uri = data.ImageUrl
                            };
                            videoPlayer.Source = uriVideoSource;
                            UserLocationLbl.Text = data.Location;
                            DescriptionLbl.Text = data.Description;
                            TagsLbl.Text = data.Tags;
                            //Favourite Functionality Code
                            guid.Text = data.Guid;
                            favourite.Text = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) ? Convert.ToString(data.Favourite) : "0";
                            LikeImg.Source = !string.IsNullOrEmpty(Convert.ToString(data.Favourite)) && Convert.ToString(data.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                        }
                    }
                    else
                    {
                        RecentPostFrame.IsVisible = false;
                        RecentPostLbl.IsVisible = false;
                        ViewAllLbl.IsVisible = false;
                    }
                }
                else
                {
                    RecentPostFrame.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Post.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }
        //Wellness button click
        async void WellnessBtnClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                //If user is not present in session then redirect to LoginPage
                sessionEmailId = Settings.GeneralSettings;
                if (string.IsNullOrEmpty(sessionEmailId))
                {
                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                }
                else
                {
                    await Navigation.PushAsync(new PostMenuPage());
                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        async void likeImageTapped(object sender, EventArgs args)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    int tapCount = 0;

                    int val = Convert.ToInt32(favourite.Text);
                    if (val == 0)
                        tapCount++;
                    else
                        if (val == 1)
                        tapCount = 0;

                    var imageSender = (Image)sender;
                    if (tapCount % 2 == 0)
                    {
                        imageSender.Source = "LikeIcon.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(guid.Text, 0);

                        #endregion region

                        #region for dislike post
                        if (current == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();
                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = guid.Text;
                            post.Favourite = 0;
                            await serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                        #endregion
                    }
                    else
                    {
                        imageSender.Source = "PurpleHeart.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(guid.Text, 1);

                        #endregion region

                        #region for like post  
                        if (current == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();
                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = guid.Text;
                            post.Favourite = 1;
                            await serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                        #endregion
                    }

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                await Navigation.PushAsync(new Post());
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Post.Xaml";
                addlogFile.ExceptionEventName = "ImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void CommentImageTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (guid.Text != null)
                    {
                        string selectedGuid = guid.Text;

                        await Navigation.PushAsync(new NavigationPage(new Reviews(selectedGuid, "Post", "")));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Post.Xaml";
                addlogFile.ExceptionEventName = "CommentImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        private async void ViewAllLbl_Tapped(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await Navigation.PushAsync(new NavigationPage(new ViewAllPosts()));
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
    }
}