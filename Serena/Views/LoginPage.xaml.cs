﻿using System;
using System.Linq;
using System.Diagnostics;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Auth;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Serena.Data;
using Serena.Helpers;
using Xamarin.Essentials;
using Serena.DBService;
using Serena.Model;
using Serena.ViewModels;

namespace Serena.Views
{
    public partial class LoginPage : ContentPage
    {
        IOAuth2Service oAuth2Service;

        //[Obsolete]
        public LoginPage()
        {
            InitializeComponent();
            try
            {
                this.BindingContext = new SocialLoginPageViewModel(oAuth2Service);
                createaccLbl.Text = "Or tap to create a " + "SERENA" + " account";
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LoginPage.xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void OnLabel1Clicked(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new SignUpPage());
            await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }
    }
}