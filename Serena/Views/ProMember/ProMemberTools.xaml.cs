﻿using Amazon.S3;
using Amazon.S3.Model;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Toast;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Serena.Data.Constants;


namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProMemberTools : ContentPage
    {
        MediaFile Selectedvideofile;

        string sessionEmailId;
        string UserProfileImageURL = "";
        int indexId = 0;
        string flagUplaod = "";

        public ProMemberTools(int index, string uploadFlag, MediaFile media)
        {
            InitializeComponent();
            Selectedvideofile = media;
            indexId = index;
            flagUplaod = uploadFlag;
        }

        private void SocialProfileTabData(int index, string uploadFlag, MediaFile media)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    Selectedvideofile = media;

                    sessionEmailId = Settings.GeneralSettings;

                    if (string.IsNullOrEmpty(sessionEmailId))
                    {
                        Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                    }

                    #region to show video upload popup 

                    if (index == 1)
                        PMToolsTabs.SelectedIndex = 1;
                    else
                        PMToolsTabs.SelectedIndex = 0;

                    if (index == 1 && uploadFlag == "Success")
                    {
                        popupLoadingView.IsVisible = true;
                        stkPop.BackgroundColor = Color.FromHex("#DCDCDC");
                        PMToolsTabs.BackgroundColor = Color.FromHex("#DCDCDC");
                    }
                    else
                    {
                        popupLoadingView.IsVisible = false;
                        stkPop.BackgroundColor = Color.FromHex("#FFFFFF");
                        PMToolsTabs.BackgroundColor = Color.FromHex("#FFFFFF");
                    }

                    #endregion

                    lblCategories.Text = "Categories your media & posts cover";
                    sessionEmailId = Settings.GeneralSettings;

                    if (PMToolsTabs.SelectedIndex == 0)
                    {
                        if (!string.IsNullOrEmpty(sessionEmailId))
                        {
                            var current1 = Connectivity.NetworkAccess;
                            if (current1 == NetworkAccess.Internet)
                            {
                            
                                var getAll = App.DBIni.UserProfileDataDB.GetPromemberTool_Social(sessionEmailId);
                                if (getAll != null)
                                {
                                    txtAbout.Text = getAll.About;
                                    txtTagLine.Text = getAll.Tagline;
                                    PhotoBtn.BackgroundImage = UserProfileImageURL = getAll.Coverphoto;
                                    if (getAll.Categories != "")
                                    {
                                        string s = getAll.Categories;
                                        string[] values = s.Split(',');
                                        foreach (var item in values)
                                        {
                                            if (item.ToLower() == "accupncture")
                                                chkAccupncture.IsChecked = true;
                                            if (item.ToLower() == "meditation")
                                                chkMeditation.IsChecked = true;
                                            if (item.ToLower() == "sleep")
                                                chkSleep.IsChecked = true;
                                            if (item.ToLower() == "yoga")
                                                chkYoga.IsChecked = true;
                                            if (item.ToLower() == "pilates")
                                                chkPilates.IsChecked = true;
                                            if (item.ToLower() == "chinese herbal medicine")
                                                chkHerbal.IsChecked = true;
                                            if (item.ToLower() == "martial arts")
                                                chkMartial.IsChecked = true;
                                            if (item.ToLower() == "diet")
                                                chkDiet.IsChecked = true;
                                            if (item.ToLower() == "dance")
                                                chkDance.IsChecked = true;
                                            if (item.ToLower() == "physiotherapy")
                                                chkPhysiotherapy.IsChecked = true;
                                            if (item.ToLower() == "massage")
                                                chkMassage.IsChecked = true;
                                            if (item.ToLower() == "exercise")
                                                chkExercise.IsChecked = true;
                                        }
                                    }
                                }
                            }
                            else
                                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                        }

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);


            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                SocialProfileTabData(indexId, flagUplaod, Selectedvideofile);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void btnSocialProfile_Clicked(object sender, System.EventArgs e)
        {

        }

        async void btnSave_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    List<string> strList = new List<string>();
                    ProMemberToolsModel proMemberToolsModel = new ProMemberToolsModel();
                    proMemberToolsModel.UserEmailId = sessionEmailId;
                    proMemberToolsModel.Tagline = txtTagLine.Text;
                    proMemberToolsModel.About = txtAbout.Text;
                    proMemberToolsModel.Coverphoto = UserProfileImageURL;
                    if (chkAccupncture.IsChecked == true)
                        strList.Add(lblAccupncture.Text);
                    if (chkDance.IsChecked == true)
                        strList.Add(lblDance.Text);
                    if (chkDiet.IsChecked == true)
                        strList.Add(lblDiet.Text);
                    if (chkExercise.IsChecked == true)
                        strList.Add(lblExercise.Text);
                    if (chkHerbal.IsChecked == true)
                        strList.Add(lblHerbal.Text);
                    if (chkMartial.IsChecked == true)
                        strList.Add(lblMartial.Text);
                    if (chkMassage.IsChecked == true)
                        strList.Add(lblMassage.Text);
                    if (chkMeditation.IsChecked == true)
                        strList.Add(lblMeditation.Text);
                    if (chkPhysiotherapy.IsChecked == true)
                        strList.Add(lblPhysio.Text);
                    if (chkPilates.IsChecked == true)
                        strList.Add(lblPilates.Text);
                    if (chkSleep.IsChecked == true)
                        strList.Add(lblSleep.Text);
                    if (chkYoga.IsChecked == true)
                        strList.Add(lblYoga.Text);


                    StringBuilder sb = new System.Text.StringBuilder();
                    foreach (string str in strList)
                    {
                        if (sb.Length > 0)
                            sb.Append(",");

                        sb.Append(str);
                    }
                    proMemberToolsModel.Categories = sb.ToString();

                    var returnValue = App.DBIni.UserProfileDataDB.AddPromemberTool_Social(proMemberToolsModel);

                    var current1 = Connectivity.NetworkAccess;
                    if (current1 == NetworkAccess.Internet)
                    {
                        //adding in mysql
                        var serenaDataSync = new SerenaDataSync();
                        Task.Run(() => serenaDataSync.SaveSocialPostMemberTools(proMemberToolsModel)).Wait();
                    }
                    //await Navigation.PushAsync(new NavigationPage(new ProMemberTools()));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "btnSave_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void PhotoBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current1 = Connectivity.NetworkAccess;
                if (current1 == NetworkAccess.Internet)
                {
                    await CrossMedia.Current.Initialize();

                    //If pickedup profile image format is not supported to device
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        await DisplayAlert("Not Supported", "Device currently not support this functionality", "OK");
                        return;
                    }
                    //Adjust the size of Profile Image
                    var mediaOption = new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.MaxWidthHeight,
                    };

                    //Get the Profile Image path
                    var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOption);
                    if (selectedImageFile != null)
                    {
                        string filename = Path.GetFileName(selectedImageFile.Path);
                        //Replace space beween the filename with +
                        string UserFileName = filename.Replace(" ", "+");

                        //if Profile Image is null
                        if (PhotoBtn == null)
                        {
                            await DisplayAlert("Error", "could not get the image, please try again.", "OK");
                            return;
                        }
                        //Set Profile Image
                        PhotoBtn.BackgroundImage = ImageSource.FromStream(() => selectedImageFile.GetStream());
                        PhotoLbl.Text = "";
                        try
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            var putRequest2 = new PutObjectRequest
                            {
                                BucketName = ProfilePagebucketName,
                                Key = UserFileName,
                                //FilePath = filePath,
                                ContentType = "image/jpeg",
                                InputStream = selectedImageFile.GetStream(),
                                CannedACL = S3CannedACL.PublicReadWrite
                            };
                            //To get the profile image URL
                            UserProfileImageURL = "https://profilessimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                            putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                            var current = Connectivity.NetworkAccess;
                            if (current == NetworkAccess.Internet)
                            {
                                PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            if (amazonS3Exception.ErrorCode != null &&
                                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                                ||
                                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                            {
                                await DisplayAlert("InvalidAccessKeyId", "Check the provided AWS Credentials", "OK");
                                return;
                            }
                            else
                            {
                                await DisplayAlert("Error occurred:", amazonS3Exception.Message, "OK");
                                return;
                            }
                        }
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "PhotoBtn_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void PMToolsTabs_SelectionChanged(object sender, Syncfusion.XForms.TabView.SelectionChangedEventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var selectedIndex = e.Index;

                    if (selectedIndex == 1)
                    {
                        var current1 = Connectivity.NetworkAccess;
                        if (current1 == NetworkAccess.Internet)
                        {
                            //SerenaDataSync Data = new SerenaDataSync();
                            //Task.Run(() => Data.GetProClass(sessionEmailId)).Wait();
                            var getUploadedClass = App.DBIni.UserProfileDataDB.GetProClassDetailsByEmailId(sessionEmailId);
                            if (getUploadedClass.Count() > 0)
                                listview.ItemsSource = getUploadedClass;
                        }
                    }
                    else
                        if (PMToolsTabs.SelectedIndex == 0)
                    {
                        if (!string.IsNullOrEmpty(sessionEmailId))
                        {
                            var current1 = Connectivity.NetworkAccess;
                            if (current1 == NetworkAccess.Internet)
                            {
                                var getAll = App.DBIni.UserProfileDataDB.GetPromemberTool_Social(sessionEmailId);
                                if (getAll != null)
                                {
                                    txtAbout.Text = getAll.About;
                                    txtTagLine.Text = getAll.Tagline;
                                    PhotoBtn.BackgroundImage = UserProfileImageURL = getAll.Coverphoto;
                                    if (getAll.Categories != "")
                                    {
                                        string s = getAll.Categories;
                                        string[] values = s.Split(',');
                                        foreach (var item in values)
                                        {
                                            if (item.ToLower() == "accupncture")
                                                chkAccupncture.IsChecked = true;
                                            if (item.ToLower() == "meditation")
                                                chkMeditation.IsChecked = true;
                                            if (item.ToLower() == "sleep")
                                                chkSleep.IsChecked = true;
                                            if (item.ToLower() == "yoga")
                                                chkYoga.IsChecked = true;
                                            if (item.ToLower() == "pilates")
                                                chkPilates.IsChecked = true;
                                            if (item.ToLower() == "chinese herbal medicine")
                                                chkHerbal.IsChecked = true;
                                            if (item.ToLower() == "martial arts")
                                                chkMartial.IsChecked = true;
                                            if (item.ToLower() == "diet")
                                                chkDiet.IsChecked = true;
                                            if (item.ToLower() == "dance")
                                                chkDance.IsChecked = true;
                                            if (item.ToLower() == "physiotherapy")
                                                chkPhysiotherapy.IsChecked = true;
                                            if (item.ToLower() == "massage")
                                                chkMassage.IsChecked = true;
                                            if (item.ToLower() == "exercise")
                                                chkExercise.IsChecked = true;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "PMToolsTabs_SelectionChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void btnUpload_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProMemberMenu());
        }

        private void Closebtn_Clicked(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = false;
            stkPop.BackgroundColor = Color.FromHex("#FFFFFF");
            PMToolsTabs.BackgroundColor = Color.FromHex("#FFFFFF");

        }

        async void Share_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await Navigation.PushAsync(new ProMemberSharePost(Selectedvideofile));
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }

        async void ThumbNail_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var thumbNailImage = (Xamarin.Forms.Image)sender;
                    var item = (TapGestureRecognizer)thumbNailImage.GestureRecognizers[0];
                    string videoUrl = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenVideo(videoUrl, "PromemberTools"));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "ThumbNail_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {

                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var anyWhereTap = (Xamarin.Forms.StackLayout)sender;
                    var item = (TapGestureRecognizer)anyWhereTap.GestureRecognizers[0];
                    string videoUrl = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenVideo(videoUrl, "PromemberTools"));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "TapGestureRecognizer_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}