﻿using Syncfusion.XForms.Buttons;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.DBService;
using Serena.Model;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Threading;
using static Serena.Model.Address;
using static Serena.Data.Constants;
using System.Net;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;


namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SocialProfile : ContentPage
    {
        string sessionEmailId;
        string UserAccounttype = "";
        SignUpDB signupDB = new SignUpDB();
        int isPro = 0, bproMember = 0;
        public SocialProfile(string socialEmailId, string becomeProMember)
        {
            InitializeComponent();
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    bproMember = Convert.ToInt32(becomeProMember);
                    lblInfo.Text = "Your information here is used to allow other users to \n" +
                        "message with you about content and/or services.\n";

                    BindingContext = this; // for TapCommand
                                           //lblBecome.IsVisible = true; // Apply for pro text

                    sessionEmailId = socialEmailId;

                    #region get social profile details

                    var UserData = signupDB.GetSpecificUserByEmail(sessionEmailId);
                    if (UserData != null)
                    {
                        txtFULLNAME.Text = UserData.FULLNAME;
                        txtUSERNAME.Text = UserData.USERNAME;
                        if (string.IsNullOrEmpty(txtUSERNAME.Text))
                            txtUSERNAME.Text = UserData.FULLNAME;
                    }
                    var userprofileData = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                    if (userprofileData != null)
                    {
                        txtWEBSITE.Text = userprofileData.UserWebsite;
                        txtHOMEBASE.Text = userprofileData.UserLocation;
                        txtCONTACTEMAIL.Text = userprofileData.ContactEmail;
                        txtPHONE.Text = userprofileData.ContactPhone;
                        isPro = userprofileData.IsPro;

                        if (userprofileData.AllowContact == "NO" || string.IsNullOrEmpty(userprofileData.AllowContact))
                        {
                            contactSwitch.IsOn = false;
                            sfinputContactEmail.IsVisible = false;
                            sfPhone.IsVisible = false;
                        }
                        else
                        {
                            contactSwitch.IsOn = true;
                            sfinputContactEmail.IsVisible = true;
                            sfPhone.IsVisible = true;
                        }
                        if (userprofileData.AllowContact == "EI")
                        {
                            rdEITHER.IsChecked = true;
                            contactSwitch.IsOn = true;
                            sfinputContactEmail.IsVisible = true;
                            sfPhone.IsVisible = true;
                        }
                        if (userprofileData.AllowContact == "PH")
                        {
                            rdPHONE.IsChecked = true;
                            contactSwitch.IsOn = true;
                            sfinputContactEmail.IsVisible = true;
                            sfPhone.IsVisible = true;
                        }
                        if (userprofileData.AllowContact == "EM")
                        {
                            rdEMAIL.IsChecked = true;
                            contactSwitch.IsOn = true;
                            sfinputContactEmail.IsVisible = true;
                            sfPhone.IsVisible = true;
                        }

                    }
                    else
                        Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

                    AddressList.IsVisible = false;
                    #endregion

                    #region Adding test data in AgeRange
                    var current1 = Connectivity.NetworkAccess;
                    if (current1 == NetworkAccess.Internet)
                    {
                        var serenaDataSync = new SerenaDataSync();
                        serenaDataSync.GetSerenaAgeRangeAsync();
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                #endregion
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //void OpenPage(string url)
        //{
        //    if (isPro == 0)
        //        Navigation.PushModalAsync(new BecomeProMember());
        //    else
        //        DisplayAlert("Already a Pro-Member!!!", "OK", "Cancel");
        //}
        private void SfSwitch_StateChanged(object sender, SwitchStateChangedEventArgs e)
        {
            if (contactSwitch.IsOn == true)
            {
                radioGroup.IsVisible = true;
                //if (rdEITHER.IsChecked == true)
                //{
                sfinputContactEmail.IsVisible = true;
                sfPhone.IsVisible = true;
                //}
                //else
                //{
                //    sfinputContactEmail.IsVisible = false;
                //    sfPhone.IsVisible = false;
                //}
            }
            else
            {
                radioGroup.IsVisible = false;
                sfinputContactEmail.IsVisible = false;
                sfPhone.IsVisible = false;
            }

        }
        void ProfileTypeVisibilityChanged(object sender, System.EventArgs e)
        {
            if (ProfileSwitch.SelectedSegment == 1)
            {
                UserAccounttype = "PRIVATE";
            }
            else if (ProfileSwitch.SelectedSegment == 0)
            {
                UserAccounttype = "PUBLIC";
            }
        }
        async void btnNext_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current1 = Connectivity.NetworkAccess;
                if (current1 == NetworkAccess.Internet)
                {
                    UserProfileData userprofileData = new UserProfileData();
                    var olduserProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                    userprofileData.UserEmailId = sessionEmailId;
                    userprofileData.UserWebsite = txtWEBSITE.Text;
                    userprofileData.UserLocation = txtHOMEBASE.Text;
                    userprofileData.ContactEmail = txtCONTACTEMAIL.Text;
                    userprofileData.ContactPhone = txtPHONE.Text;
                    userprofileData.UserName = txtUSERNAME.Text;
                    userprofileData.UserPostJson = olduserProfileData.UserPostJson;
                    userprofileData.UserProfileImage = olduserProfileData.UserProfileImage;
                    userprofileData.UserAccountType = UserAccounttype;
                    userprofileData.UserBio = olduserProfileData.UserBio;
                    userprofileData.UserAgeRange = olduserProfileData.UserAgeRange;

                    if (rdEITHER.IsChecked == true && contactSwitch.IsOn == true)
                        userprofileData.AllowContact = "EI";
                    if (rdPHONE.IsChecked == true && contactSwitch.IsOn == true)
                        userprofileData.AllowContact = "PH";
                    if (rdEMAIL.IsChecked == true && contactSwitch.IsOn == true)
                        userprofileData.AllowContact = "EM";
                    if (contactSwitch.IsOn == false)
                        userprofileData.AllowContact = "NO";

                    userprofileData.IsPro = bproMember == 1 ? bproMember : 0;

                    var retrunvalue = App.DBIni.UserProfileDataDB.AddUser(userprofileData);

                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                        //adding in mysql
                        var serenaDataSync = new SerenaDataSync();
                        await serenaDataSync.PostSocialProfileData(userprofileData, txtFULLNAME.Text);
                    }
                    await Navigation.PushAsync(new NavigationPage(new ProfilePage(txtHOMEBASE.Text, txtWEBSITE.Text, UserAccounttype)));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "btnSave_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {

            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time

            try
            {
                CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(txtHOMEBASE.Text))))
                { //Be sure to UrlEncode the search term they enter

                    using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                            PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                            if (predictionList.Status == "OK")
                            {
                                Addresses.Clear();

                                if (predictionList.Predictions.Count > 0)
                                {
                                    foreach (Prediction prediction in predictionList.Predictions)
                                    {
                                        Addresses.Add(new AddressInfo
                                        {
                                            Address = prediction.Description
                                        });
                                    }
                                }
                            }
                            else
                            {
                                AddressList.ItemsSource = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void ClearBtnClicked(object sender, EventArgs e)
        {
            txtHOMEBASE.Text = "";
            AddressList.ItemsSource = null;
            //list isible false
            AddressList.IsVisible = false;
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                AddressList.ItemsSource = "";
                var PlaceAddress = (Xamarin.Forms.Label)sender;
                txtHOMEBASE.Text = PlaceAddress.Text;
                //list isible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }
        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (!string.IsNullOrEmpty(txtHOMEBASE.Text))
                    {
                        //list is visible false
                        AddressList.IsVisible = true;
                        await GetPlacesPredictionsAsync();
                        AddressList.ItemsSource = Addresses;

                    }
                    else if (string.IsNullOrEmpty(txtHOMEBASE.Text))
                    {
                        //list is visible false
                        AddressList.IsVisible = false;
                        AddressList.ItemsSource = null;
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialProfile.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}