﻿using Plugin.Toast;
using Serena.Data;
using Serena.Helpers;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BecomeProMember : ContentPage
    {
        public BecomeProMember()
        {
            InitializeComponent();
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {

                lstReq.ItemsSource = new List<ProMembership>
            {
            new  ProMembership
                {
                Description = "Posting to the Explore section of the app at least 7 free video or audio sessions (minimum length 5 minutes; maximum 90 minutes)"

                 },
               new ProMembership
               {
                   Description = "Regular engagement through posts to Today feed at least 3x/week"

                },
                new ProMembership
                {
                    Description = "Posts should not carry over branding from other platforms, but do not need to be unique to Serena",
                },

           };

                lstBenefits.ItemsSource = new List<ProMembership>
            {
            new  ProMembership
                {
                BDescription = "New customer acquisition (local & global). Serena spends the money to attract customers to the platform and keep them engaged."

                 },
               new ProMembership
               {
                   BDescription = "Schedule optimization. Serena provides a simple, in-app method of managing Serena bookings with general app members."

                },
                new ProMembership
                {
                    BDescription = "Ability to upload Media (videos/online classes, talks, etc.). Unlimited number.",
                },
                 new ProMembership
               {
                   BDescription = "Personalize access to content with payscale: (1) Free to all, (2) Free for 7 days then subscribers only, (3) Free for 30 days then subscribers only, & (4) Subscribers only."

                },
                new ProMembership
                {
                    BDescription = "Keep your regulars coming back: The Serena App provides engaging and meaningful way for your clients to share their wellness journey with others.",
                },
                new ProMembership
               {
                   BDescription = "Paid upfront/no bad debts: Serena captures the fee from the general member upfront."

                },
                new ProMembership
                {
                    BDescription = "Influencer/Celebrity status as the app grows!",
                }
           };
                lblPay.Text = "What You “Pay”";
                lstPay.ItemsSource = new List<ProMembership>
            {
            new  ProMembership
                {
                PDescription = "Serena takes a portion of the total booking fee " +
                "based on a scheduled scaled to usage as follows:\n" +
                "● 35% of the First Booking after app store  " +
                "  commissions is retained by Serena\n" +
                "● 25% Second\n" +
                "● 20% Third\n" +
                "● 15% Fourth\n" +
                "● 10% Fifth\n" +
                "● 3% Sixth and all future\n"
                 }
           };
                var labelFormatted = new Label();
                var fs = new FormattedString();
                fs.Spans.Add(new Span { Text = "  ○ Total play count (75%) \n" });
                fs.Spans.Add(new Span { Text = "  ○ Teacher engagement (25%) as\n " });
                fs.Spans.Add(new Span { Text = " measured by total follows and likes in\n" });
                fs.Spans.Add(new Span { Text = " the Social Feed. \n" });
                labelFormatted.FormattedText = fs;

                lstPay2.ItemsSource = new List<ProMembership>
            {
            new  ProMembership
                {
                PDescription2 = "Pros receive 50% of all subscription income that\n" +
                " Serena receives after app store commissions.\n" +
                "● Serena distributes from the total subscription\n" +
                " pool based on two metrics only  \n" +
                fs
                 }
           };
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        private void chkIagree_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (chkIagree.IsChecked == true)
                    btnPro.IsEnabled = true;
                else
                    btnPro.IsEnabled = false;
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        async void btnPro_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new SocialProfile(Settings.GeneralSettings, "1"));
        }

        async void btnNo_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Profile());
        }
    }
}