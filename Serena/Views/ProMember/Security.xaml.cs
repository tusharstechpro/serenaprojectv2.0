﻿using Plugin.Toast;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Text.RegularExpressions;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Security : ContentPage
    {

        CryptographyASE Crypto = new CryptographyASE();

        public Security()
        {
            InitializeComponent();
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                txtAccEMail.Text = Settings.GeneralSettings;
                SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
                txtConfirmPassword.IsEnabled = false;
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }

        private void txtOldPassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var input = txtOldPassword.Text;
                var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(txtAccEMail.Text);
                if (UserData != null)
                {
                    string passToCompare = Crypto.Decrypt(UserData.PASSWORD);
                    if (passToCompare == txtOldPassword.Text)
                    {
                        inputOldPass.HasError = false;
                        inputOldPass.ErrorText = "";
                    }
                    else
                    {
                        inputOldPass.HasError = true;
                        inputOldPass.ErrorText = "Password Incorrect";
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Security.Xaml";
                addlogFile.ExceptionEventName = "txtOldPassword_TextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void txtNewPassEvent(object sender, EventArgs e)
        {
            var input = txtNewPassword.Text;
            var test = new Regex(@"^(?=.*[a-z])(?=.*[@$!%*?&#_()-+/!])[A-Za-z\d@$!%*?&#_()-+/!]{8,10}$");
            var isValidated = test.IsMatch(input);

            if (isValidated == false)
            {
                inputnewPass.HasError = true;
                inputnewPass.ErrorText = "Password must contain 8-10 characters with symbol (!, &, *, etc)!";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
                txtConfirmPassword.IsEnabled = false;

            }
            else
            {
                inputnewPass.HasError = false;
                inputnewPass.ErrorText = "";
                txtConfirmPassword.IsEnabled = true;
            }
        }

        private void txtConfirmPassEvent(object sender, EventArgs e)
        {
            var input = txtNewPassword.Text;
            var test = new Regex(@"^(?=.*[a-z])(?=.*[@$!%*?&#_()-+/!])[A-Za-z\d@$!%*?&#_()-+/!]{8,10}$");
            var isValidated = test.IsMatch(input);

            if (isValidated == false)
            {
                inputConfirmPass.HasError = true;
                inputConfirmPass.ErrorText = "Password must contain 8-10 characters with symbol (!, &, *, etc)!";
                SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                SignUpBtn.TextColor = Color.White;
                SignUpBtn.IsEnabled = false;
            }
            else
            {
                if (txtNewPassword.Text != txtConfirmPassword.Text)
                {
                    inputConfirmPass.HasError = true;
                    inputConfirmPass.ErrorText = "Password and Confirm Password are not same! ";
                    SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = false;
                }
                else
                {
                    inputConfirmPass.HasError = false;
                    inputConfirmPass.ErrorText = "";
                    SignUpBtn.BackgroundColor = Color.FromHex("#9900CC");
                    SignUpBtn.TextColor = Color.White;
                    SignUpBtn.IsEnabled = true;
                }
            }
        }

        public async void SignUpBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (txtNewPassword.Text == txtOldPassword.Text)
                    {
                        inputnewPass.HasError = true;
                        inputnewPass.ErrorText = "NewPassword and Old Password are same! ";
                        SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                        SignUpBtn.TextColor = Color.White;
                        SignUpBtn.IsEnabled = false;
                    }
                    else
                if (txtNewPassword.Text != txtConfirmPassword.Text)
                    {
                        inputConfirmPass.HasError = true;
                        inputConfirmPass.ErrorText = "Password and Confirm Password are not same! ";
                        SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                        SignUpBtn.TextColor = Color.White;
                        SignUpBtn.IsEnabled = false;
                    }
                    if (input2.HasError == true || inputnewPass.HasError == true || inputConfirmPass.HasError == true)
                    {
                        SignUpBtn.BackgroundColor = Color.FromHex("#9D9D9D");
                        SignUpBtn.TextColor = Color.White;
                        SignUpBtn.IsEnabled = false;
                    }
                    else
                    {

                        string encryptedPassword = Crypto.Encrypt(txtNewPassword.Text);
                        App.DBIni.SignUpDB.updateUser(txtAccEMail.Text, encryptedPassword);

                        #region AddUser_out
                        var SignUpData = App.DBIni.SignUpDB.GetOutUserByEmail(txtAccEMail.Text);
                        SignUpData.PASSWORD = encryptedPassword;
                        SignUpData.LastSyncDate = DateTime.Now;
                        SignUpData.Status = "1";//Save
                        var retturnvalue_Out = App.DBIni.SignUpDB.AddUser_out(SignUpData);
                        #endregion

                        bool answer = await DisplayAlert("", "Password Changed!!!", "OK", "Cancel");
                        if (answer == false)
                            await Navigation.PushAsync(new NavigationPage(new Security()));
                        else
                            await Navigation.PushAsync(new NavigationPage(new Security()));

                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Security.Xaml";
                addlogFile.ExceptionEventName = "SignUpBtn_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
    }
}