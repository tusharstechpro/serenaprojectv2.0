﻿using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Toast;
using Serena.DBService;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Data;


namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProMemberMenu : ContentPage
    {
        FileData pickerFile = null;
        public ProMemberMenu()
        {
            InitializeComponent();
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //Tap Gesture Recognizer  
                    var libraryTap = new TapGestureRecognizer();
                    libraryTap.Tapped += async (sender, e) =>
                    {
                        DefaultBackground();
                        lblLibrary.TextColor = Color.FromHex("#000000");
                        lblCamera.TextColor = Color.FromHex("#B7B7B7");
                        lblFiles.TextColor = Color.FromHex("#B7B7B7");

                        try
                        {
                            if (!CrossMedia.Current.IsPickVideoSupported)
                            {
                                await DisplayAlert("Videos Not Supported", ":( Permission not granted to videos.", "OK");
                                return;
                            }
                        //Pick video file from device
                        var file = await CrossMedia.Current.PickVideoAsync();
                            if (file == null)
                                return;

                            Stream inputStream = file.GetStream();
                        //Get video path
                        var path = file.Path;
                        //Get Video Length
                        var duration = DependencyService.Get<VideoUploadInterface>().getVideoLength(path);
                        //Get last 2 digit from video length
                        //var Seconds = duration.Substring(duration.Length - 2);
                        //Convert seconds in Integer value
                        //var exactDuration = Convert.ToInt32(Seconds);
                        //If video duration is less than or equal to 30
                        //if (exactDuration <= 30)
                        //{
                        //show popup as video selected with video file location
                        await DisplayAlert("Video Selected", "Location: " + file.Path, "OK");
                        //If path is not null
                        if (file.Path != null)
                            {
                            //navigate to post video page to show video
                            await Navigation.PushAsync(new UploadMedia(file.Path, duration, file, null));
                            }
                        //}
                        ////else show alert popup that video limit is 30 seconds 
                        //else
                        //{
                        //    await DisplayAlert("Video limit is 30 Seconds", "", "OK");
                        //}
                    }
                        catch (Exception ex)
                        {

                            throw ex;
                        }
                        stckLibrary.BackgroundColor = Color.White;
                    };
                    stckLibrary.GestureRecognizers.Add(libraryTap);


                    var cameraTap = new TapGestureRecognizer();
                    cameraTap.Tapped += async (sender, e) =>
                    {
                        DefaultBackground();
                        lblCamera.TextColor = Color.FromHex("#000000");
                        lblLibrary.TextColor = Color.FromHex("#B7B7B7");
                        lblFiles.TextColor = Color.FromHex("#B7B7B7");
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
                        {
                            await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                            return;
                        }
                        if (CrossMedia.Current.IsCameraAvailable && CrossMedia.Current.IsTakeVideoSupported)
                        {
                        // Supply media options for saving our video after it's taken.
                        var mediaOptions = new Plugin.Media.Abstractions.StoreVideoOptions
                            {
                                Directory = "Videos",
                                Name = $"{DateTime.Today}.mp4",
                                MaxWidthHeight = 1024,
                                Quality = VideoQuality.Medium
                            };

                        // Record a video
                        var file = await CrossMedia.Current.TakeVideoAsync(mediaOptions);
                            if (file == null)
                                return;

                            var path = file.Path;
                        //Get Video Length
                        var duration = DependencyService.Get<VideoUploadInterface>().getVideoLength(path);
                        //Get last 2 digit from video length
                        //var Seconds = duration.Substring(duration.Length - 2);
                        //Convert seconds in Integer value
                        //var exactDuration = Convert.ToInt32(Seconds);
                        //If video duration is less than or equal to 30
                        //if (exactDuration <= 30)
                        //{
                        //show popup as video selected with video file location
                        await DisplayAlert("Video Selected", "Location: " + file.Path, "OK");
                        //If path is not null
                        if (file.Path != null)
                            {
                            //navigate to post video page to show video
                            await Navigation.PushAsync(new UploadMedia(file.Path, duration, file, null));
                            }
                        //}
                        //else show alert popup that video limit is 30 seconds 
                        else
                            {
                                await DisplayAlert("Video limit is 30 Seconds", "", "OK");
                            }
                        }


                        stckCamera.BackgroundColor = Color.White;
                    };
                    stckCamera.GestureRecognizers.Add(cameraTap);


                    var filesTap = new TapGestureRecognizer();
                    filesTap.Tapped += async (sender, e) =>
                    {
                        DefaultBackground();
                        lblFiles.TextColor = Color.FromHex("#000000");
                        stckFiles.BackgroundColor = Color.White;
                        lblLibrary.TextColor = Color.FromHex("#B7B7B7");
                        lblCamera.TextColor = Color.FromHex("#B7B7B7");

                        try
                        {
                            string[] fileTypes = null;

                            if (Device.RuntimePlatform == Device.Android)
                            {
                            //filetype filter for android
                            fileTypes = new string[] { "video/mp4", "video/x-ms-wmv", "video/x-msvideo", "video/*" };
                            }
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                            //filetype filter for ios
                            fileTypes = new string[] {
                                 "video/mp4", "video/x-ms-wmv", "video/x-msvideo"  }; // same as iOS constant UTType.Image
                        }

                            await PickAndShow(fileTypes);
                            if (pickerFile == null)
                                return;
                            else
                            {
                                var path = pickerFile.FilePath;

                            //Get Video Length
                            var duration = DependencyService.Get<VideoUploadInterface>().getVideoLength(path);
                            //Get last 2 digit from video length
                            //var Seconds = duration.Substring(duration.Length - 2);
                            //Convert seconds in Integer value
                            //var exactDuration = Convert.ToInt32(Seconds);
                            //If video duration is less than or equal to 30
                            //if (exactDuration <= 30)
                            //{
                            //show popup as video selected with video file location
                            await DisplayAlert("Video Selected", "Location: " + pickerFile.FilePath, "OK");
                            //If path is not null
                            if (pickerFile.FilePath != null)
                                {
                                //navigate to post video page to show video
                                await Navigation.PushAsync(new UploadMedia(pickerFile.FilePath, duration, null, pickerFile));
                                }
                            //}
                            //else show alert popup that video limit is 30 seconds 
                            else
                                {
                                    await DisplayAlert("Video limit is 30 Seconds", "", "OK");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var addlogFile = new ExceptionLog();
                            addlogFile.ExceptionDetails = ex.Message;
                            addlogFile.ExceptionCategoryScreenName = "ProMemberMenuPage.Xaml";
                            addlogFile.ExceptionEventName = "Init";

                            App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                        }
                    };
                    stckFiles.GestureRecognizers.Add(filesTap);
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ProMemberMenuPage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        private async Task PickAndShow(string[] fileTypes)
        {
            try
            {

                var pickedFile = await CrossFilePicker.Current.PickFile(fileTypes);
                if (pickedFile != null)
                {
                    pickerFile = pickedFile;
                }
                else
                {
                    pickerFile = null;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ProMemberMenuPage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        public void DefaultBackground()
        {
            stckLibrary.BackgroundColor = Color.White;
            stckCamera.BackgroundColor = Color.White;
            stckFiles.BackgroundColor = Color.White;
        }
    }
}