﻿using Amazon.S3;
using Amazon.S3.Model;
using Plugin.FilePicker.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Toast;
using Serena.Data;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.IO;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Serena.Data.Constants;


namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UploadMedia : ContentPage
    {

        string filePath = "", sessionEmailId = "";
        Stream str = null;
        string CoverPageImageURL = "", mediaUrl = "";
        MediaFile Selectedvideofile;

        public UploadMedia(string path, string duration, MediaFile inputStream, FileData pickerFile)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    InitializeComponent();
                    filePath = path;
                    lblTime.Text = "Time: " + duration.Remove(0, 3);
                    if (inputStream != null)
                        Selectedvideofile = inputStream;
                    else
                        str = pickerFile.GetStream();

                    sessionEmailId = Settings.GeneralSettings;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                //videoPlayer.Source = path;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "UploadMedia.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void PhotoBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await CrossMedia.Current.Initialize();

                    //If pickedup profile image format is not supported to device
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        await DisplayAlert("Not Supported", "Device currently not support this functionality", "OK");
                        return;
                    }
                    //Adjust the size of Profile Image
                    var mediaOption = new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.MaxWidthHeight,
                    };

                    //Get the Profile Image path
                    var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOption);
                    if (selectedImageFile != null)
                    {
                        string filename = Path.GetFileName(selectedImageFile.Path);
                        //Replace space beween the filename with +
                        string UserFileName = filename.Replace(" ", "+");

                        //if Profile Image is null
                        if (PhotoBtn == null)
                        {
                            await DisplayAlert("Error", "could not get the image, please try again.", "OK");
                            return;
                        }
                        //Set Profile Image
                        PhotoBtn.BackgroundImage = ImageSource.FromStream(() => selectedImageFile.GetStream());
                        PhotoLbl.Text = "";
                        try
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            var putRequest2 = new PutObjectRequest
                            {
                                BucketName = ProfilePagebucketName,
                                Key = UserFileName,
                                //FilePath = filePath,
                                ContentType = "image/jpeg",
                                InputStream = selectedImageFile.GetStream(),
                                CannedACL = S3CannedACL.PublicReadWrite
                            };
                            //To get the profile image URL
                            CoverPageImageURL = "https://profilessimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                            putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                            var current1 = Connectivity.NetworkAccess;
                            if (current1 == NetworkAccess.Internet)
                            {
                                PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            if (amazonS3Exception.ErrorCode != null &&
                                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                                ||
                                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                            {
                                await DisplayAlert("InvalidAccessKeyId", "Check the provided AWS Credentials", "OK");
                                return;
                            }
                            else
                            {
                                await DisplayAlert("Error occurred:", amazonS3Exception.Message, "OK");
                                return;
                            }
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PromemberTools.Xaml";
                addlogFile.ExceptionEventName = "PhotoBtn_Clicked";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current1 = Connectivity.NetworkAccess;
                if (current1 == NetworkAccess.Internet)
                {
                    //To activate busy indicator
                    SfBusyIndicator busyIndicator = new SfBusyIndicator()
                    {
                        AnimationType = AnimationTypes.Cupertino,
                        ViewBoxHeight = 50,
                        ViewBoxWidth = 50,
                        EnableAnimation = true,
                        Title = "Loading...",
                        TextSize = 12,
                        FontFamily = "Cabin",
                        TextColor = Color.FromHex("#9900CC")
                    };
                    this.Content = busyIndicator;


                    #region upload video file in amazon s3
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                        string filename = Path.GetFileName(filePath);
                        //Replace space beween the filename with +
                        string UserFileName = filename.Replace(" ", "+");

                        try
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            var putRequest2 = new PutObjectRequest
                            {
                                BucketName = PostbucketName,
                                Key = UserFileName,
                                ContentType = "application/mp4",
                                InputStream = Selectedvideofile != null ? Selectedvideofile.GetStream() : str,
                                CannedACL = S3CannedACL.PublicReadWrite
                            };
                            //To get the video URL
                            mediaUrl = "https://serenapostimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                            putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                            var current2 = Connectivity.NetworkAccess;
                            if (current2 == NetworkAccess.Internet)
                            {
                                PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                            }
                        }
                        catch (AmazonS3Exception amazonS3Exception)
                        {
                            if (amazonS3Exception.ErrorCode != null &&
                                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                                ||
                                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                            {
                                throw new Exception("Check the provided AWS Credentials.");
                            }
                            else
                            {
                                throw new Exception("Error occurred: " + amazonS3Exception.Message);
                            }
                        }
                    }
                    #endregion

                    #region create GUID for post

                    Guid guid = Guid.NewGuid();
                    string Guidstr = guid.ToString();
                    Guidstr = (guid + sessionEmailId);
                    if (string.IsNullOrEmpty(Guidstr))
                    {
                        Guidstr = "-";
                    }

                    #endregion

                    string category = "";
                    if (rdFitness.IsChecked == true)
                        category = "Fitness";
                    if (rdYoga.IsChecked == true)
                        category = "Yoga";
                    if (rdPilates.IsChecked == true)
                        category = "Pilates";
                    if (rdMeditation.IsChecked == true)
                        category = "Meditation";

                    //level of difficulties
                    string level = "";
                    if (rdEasy.IsChecked == true)
                        level = "Easy";
                    if (rdIntermediate.IsChecked == true)
                        level = "Intermediate";
                    if (rdHard.IsChecked == true)
                        level = "Hard";

                    var UploadMediaModel = new UploadMediaModel();
                    var UploadMediaModel_Out = new UploadMediaModel_Out();
                    var findUploadMediaModel = new FindUploadClassModel();
                    UploadMediaModel_Out.Guid = findUploadMediaModel.Guid = UploadMediaModel.Guid = Guidstr;
                    UploadMediaModel_Out.UserEmailId = findUploadMediaModel.UserEmailId = UploadMediaModel.UserEmailId = sessionEmailId;
                    UploadMediaModel_Out.MediaUrl = findUploadMediaModel.MediaUrl = UploadMediaModel.MediaUrl = mediaUrl;
                    UploadMediaModel_Out.CoverPhotoUrl = findUploadMediaModel.CoverPhotoUrl = UploadMediaModel.CoverPhotoUrl = CoverPageImageURL;
                    UploadMediaModel_Out.CoverPhotoUrl = findUploadMediaModel.Title = UploadMediaModel.Title = txtTitle.Text;
                    UploadMediaModel_Out.About = findUploadMediaModel.About = UploadMediaModel.About = txtAbout.Text;
                    UploadMediaModel_Out.PlayList = findUploadMediaModel.PlayList = UploadMediaModel.PlayList = "";
                    UploadMediaModel_Out.Category = findUploadMediaModel.Category = UploadMediaModel.Category = category;
                    UploadMediaModel_Out.Tags = findUploadMediaModel.Tags = UploadMediaModel.Tags = txtTags.Text;
                    UploadMediaModel_Out.Mentions = findUploadMediaModel.Mentions = UploadMediaModel.Mentions = level;//level of difficulties
                    UploadMediaModel_Out.Duration = findUploadMediaModel.Duration = UploadMediaModel.Duration = lblTime.Text;
                    findUploadMediaModel.AvgRating = 0;
                    findUploadMediaModel.TotalReviews = "0";
                    UploadMediaModel_Out.LastSyncDate = DateTime.Now;

                    //add in sqllite
                    var addProClass = App.DBIni.UserProfileDataDB.AddProClass(UploadMediaModel);


                    //add in sqllite out
                    var addProClass_Out = App.DBIni.UserProfileDataDB.AddProClass_Out(UploadMediaModel_Out);

                    //add in sqllite FindUploadClassModel for class tab in explore 
                    var FindUploadClassModel = App.DBIni.UserProfileDataDB.AddFindClass(findUploadMediaModel);


                    //var current3 = Connectivity.NetworkAccess;
                    //if (current3 == NetworkAccess.Internet)
                    //{
                    //    //adding in mysql
                    //    var serenaDataSync = new SerenaDataSync();
                    //    await serenaDataSync.SaveProClass(UploadMediaModel);
                    //}

                    await Navigation.PushAsync(new ProMemberTools(1, "Success", Selectedvideofile));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "UploadMedia.Xaml";
                addlogFile.ExceptionEventName = "TapGestureRecognizer_Tapped";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}