﻿
using Plugin.Toast;
using Xam.Forms.VideoPlayer;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Data;
using Serena.Views.ActivityTab;

namespace Serena.Views.ProMember
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenVideo : ContentPage
    {
        public string prevPageName;
        public OpenVideo(string videoUrl,string PageName)
        {
            InitializeComponent();
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                prevPageName = PageName;
                var uriVideoSource = new UriVideoSource()
                {
                    Uri = videoUrl
                };
                videoPlayer.Source = uriVideoSource;
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }

        async void videoPlayer_PlayCompletion(object sender, System.EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if(prevPageName == "PromemberTools")
                    await Navigation.PushAsync(new ProMemberTools(1, "", null));
                else if(prevPageName== "DoneWithClass")
                    await Navigation.PushAsync(new DoneWithClass());
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }
    }
}