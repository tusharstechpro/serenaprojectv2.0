﻿using Syncfusion.XForms.Buttons;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace Serena.Views
{
    [DesignTimeVisible(false)]
    public partial class PostText : ContentPage
    {
        public PostText()
        {
            InitializeComponent();
            NextBtn.IsVisible = false;
        }

        //Text post change event
        private void AddTextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextChnageLbl.Text))
            {
                TextReflectLbl.Text = "Your text here";
                NextBtn.IsVisible = false;
            }
            else
            {
                TextReflectLbl.Text = TextChnageLbl.Text;
                TextReflectLbl.FontSize = 20;
                NextBtn.IsVisible = true;
            }
        }

        //Next button click event
        private async void NextBtnClick(object sender, EventArgs e)
        {
            //If text is entered then navigate to CreatePost page
            if (TextReflectLbl.Text != "Your text here")
            {
                NextBtn.IsVisible = true;
                string text = TextReflectLbl.Text;
                Color bgcolor = CaptureView.BackgroundColor;
                Color textcolor = TextReflectLbl.TextColor;
                await Navigation.PushAsync(new CreatePost(text, bgcolor, textcolor));
            }
            //If text is not entered then NextButton is visible false
            else
            {
                NextBtn.IsVisible = false;
            }
        }

        //Edit background click event
        void BackgroundColorClicked(object sender, System.EventArgs e)
        {
            //Get the Syncfusion chip
            var chip = sender as SfChip;
            //Get the Syncfusion chip background color
            string bgcolor = chip.BackgroundColor.ToHex();

            if (bgcolor == "#FFEB5757")
            {
                CaptureView.BackgroundColor = Color.FromHex("#EB5757");
            }
            else if (bgcolor == "#FFF2994A")
            {
                CaptureView.BackgroundColor = Color.FromHex("#F2994A");
            }
            else if (bgcolor == "#FFF2C94C")
            {
                CaptureView.BackgroundColor = Color.FromHex("#F2C94C");
            }
            else if (bgcolor == "#FF219653")
            {
                CaptureView.BackgroundColor = Color.FromHex("#219653");
            }
            else if (bgcolor == "#FF2F80ED")
            {
                CaptureView.BackgroundColor = Color.FromHex("#2F80ED");
            }
            else if (bgcolor == "#FF9B51E0")
            {
                CaptureView.BackgroundColor = Color.FromHex("#9B51E0");
            }
            else if (bgcolor == "#FF333333")
            {
                CaptureView.BackgroundColor = Color.FromHex("#333333");
            }
            else if (bgcolor == "#FFFFFFFF")
            {
                CaptureView.BackgroundColor = Color.FromHex("#FFFFFF");
            }
        }
        void TextColorClicked(object sender, System.EventArgs e)
        {
            var chip = sender as SfChip;
            string bgcolor = chip.BackgroundColor.ToHex();
            chip.BorderColor = Color.White;
            chip.BorderWidth = 1;
            if (bgcolor == "#FFEB5757")
            {
                chip.BorderColor = Color.FromHex("#9900CC");
                chip.BorderWidth = 1;
                TextReflectLbl.TextColor = Color.FromHex("#EB5757");
            }
            else if (bgcolor == "#FFF2994A")
            {
                TextReflectLbl.TextColor = Color.FromHex("#F2994A");
            }
            else if (bgcolor == "#FFF2C94C")
            {
                TextReflectLbl.TextColor = Color.FromHex("#F2C94C");
            }
            else if (bgcolor == "#FF219653")
            {
                TextReflectLbl.TextColor = Color.FromHex("#219653");
            }
            else if (bgcolor == "#FF2F80ED")
            {
                TextReflectLbl.TextColor = Color.FromHex("#2F80ED");
            }
            else if (bgcolor == "#FF9B51E0")
            {
                TextReflectLbl.TextColor = Color.FromHex("#9B51E0");
            }
            else if (bgcolor == "#FF333333")
            {
                TextReflectLbl.TextColor = Color.FromHex("#333333");
            }
            else if (bgcolor == "#FFFFFFFF")
            {
                TextReflectLbl.TextColor = Color.FromHex("#FFFFFF");
            }
            
        }
    }
}