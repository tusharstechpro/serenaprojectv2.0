﻿using Newtonsoft.Json;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Serena.Model.Address;
using static Serena.Data.Constants;
using System.Net;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePost : ContentPage
    {
        Serena_Post post = new Serena_Post();
        string sessionEmailId;
        public CreatePost(string Posttext, Color bgcolor, Color textcolor)
        {
            InitializeComponent();
            try
            {
                //Get the login user EmailId from Session
                sessionEmailId = Settings.GeneralSettings;

                //Get text values from PostText page
                PostCaptureView.BackgroundColor = bgcolor;
                TextReflectLbl.Text = Posttext;
                TextReflectLbl.TextColor = textcolor;
                PostLbl.IsEnabled = false;

                //Add topic to combolist
                var topicsList = new List<Topics>();
                var alltopicsData = App.DBIni.TopicsDB.GetTopicsData();
                if (alltopicsData != null)
                {
                    foreach (var topicInfo in alltopicsData)
                    {
                        Topics addTopic = new Topics();
                        addTopic.TopicName = topicInfo.TopicName;
                        topicsList.Add(addTopic);
                    }
                    TopicsComboBox.DataSource = topicsList;
                }

                //list isible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreatePost.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Post Label click event
       async void PostLblClick(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To activate busy indicator
                    SfBusyIndicator busyIndicator = new SfBusyIndicator()
                    {
                        AnimationType = AnimationTypes.Cupertino,
                        ViewBoxHeight = 50,
                        ViewBoxWidth = 50,
                        EnableAnimation = true,
                        Title = "Loading...",
                        TextSize = 12,
                        FontFamily = "Cabin",
                        TextColor = Color.FromHex("#9900CC")
                    };
                    this.Content = busyIndicator;

                    #region If passing value is null or empty
                    if (string.IsNullOrEmpty(LocationText.Text))
                    {
                        LocationText.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TopicsComboBox.Text))
                    {
                        TopicsComboBox.Text = "-";
                    }
                    if (string.IsNullOrEmpty(TagLbl.Text))
                    {
                        TagLbl.Text = "-";
                    }
                    #endregion

                    #region create GUID for post
                    Guid guid = Guid.NewGuid();
                    string Guidstr = guid.ToString();
                    Guidstr = (guid + sessionEmailId);
                    if (string.IsNullOrEmpty(Guidstr))
                    {
                        Guidstr = "-";
                    }
                    #endregion

                    #region add data in SQLite Post
                    post.UserEmailId = sessionEmailId;
                    post.Description = descriptionLbl.Text;
                    post.ShareType = ShareTypeComboBox.Text;
                    post.Location = LocationText.Text;
                    post.Topics = TopicsComboBox.Text;
                    post.ImageUrl = "-";
                    post.Tags = TagLbl.Text;
                    post.HashTags = "-";
                    post.PostText = TextReflectLbl.Text;
                    post.PostColor = TextReflectLbl.TextColor.ToHex();
                    post.LastCreated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.LastUpdated = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
                    post.TimeOfPost = DateTime.Now.ToString("h:mm tt");
                    post.PostBackgroundColor = PostCaptureView.BackgroundColor.ToHex();
                    post.PlaceAddress = "-";
                    post.Longitude = 0.1;
                    post.Latitude = 0.1;
                    post.PostType = "Text";
                    post.Guid = Guidstr;
                    App.DBIni.SerenaPostDB.AddPost(post);
                    #endregion

                    #region Mysql DB API 
                    if (current == NetworkAccess.Internet)
                    {
                        var serenaDataSync = new SerenaDataSync();
                        await serenaDataSync.SavePost(post);
                        await Navigation.PushAsync(new Post());
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);


            }

            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreatePost.Xaml";
                addlogFile.ExceptionEventName = "PostLblClick";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        //Description change event
        private void DescriptionTextChange(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(descriptionLbl.Text))
            {
                PostLbl.TextColor = Color.FromHex("#9900CC");
                PostLbl.IsEnabled = true;
            }
            else
            {
                PostLbl.TextColor = Color.FromHex("#A6A6A6");
                PostLbl.IsEnabled = false;
            }
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {
            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(LocationText.Text))))
                    { //Be sure to UrlEncode the search term they enter

                        using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                        {
                            if (message.IsSuccessStatusCode)
                            {
                                string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                                PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                                if (predictionList.Status == "OK")
                                {
                                    Addresses.Clear();

                                    if (predictionList.Predictions.Count > 0)
                                    {
                                        foreach (Prediction prediction in predictionList.Predictions)
                                        {
                                            Addresses.Add(new AddressInfo
                                            {
                                                Address = prediction.Description
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    AddressList.ItemsSource = null;
                                }
                            }
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreatePost.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                //await Navigation.PushAsync(new Post());
            }
        }

        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(LocationText.Text))
                {
                    //list isible false
                    AddressList.IsVisible = true;
                    await GetPlacesPredictionsAsync();
                    AddressList.ItemsSource = Addresses;

                }
                else if (string.IsNullOrEmpty(LocationText.Text))
                {
                    //list isible false
                    AddressList.IsVisible = false;
                    AddressList.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreatePost.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                AddressList.ItemsSource = "";
                var PlaceAddress = (Xamarin.Forms.Label)sender;
                LocationText.Text = PlaceAddress.Text;
                //list isible false
                AddressList.IsVisible = false;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "CreatePost.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        private void ClearBtnClicked(object sender, EventArgs e)
        {
            LocationText.Text = "";
            AddressList.ItemsSource = null;
            //list is visible false
            AddressList.IsVisible = false;
        }
    }
}