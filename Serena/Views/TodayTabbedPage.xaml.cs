﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodayTabbedPage : TabbedPage
    {
        string sessionEmailId;
        public interface IActivatePage
        {
            void Active(bool active);
        }
        public TodayTabbedPage()
        {
            InitializeComponent();
            //this.On<Xamarin.Forms.PlatformConfiguration.Android>().SetIsSwipePagingEnabled(false);
            try
            {
               
                sessionEmailId = Settings.GeneralSettings;
                if (!string.IsNullOrEmpty(sessionEmailId))
                {
                    //Old Code
                    //GetUsersdataAsync();
                    NavigationPage navigationPage = new NavigationPage(new TodayHomePage());
                    navigationPage.IconImageSource = "TODAY.png";
                    navigationPage.Title = "TODAY";
                    Children.Add(navigationPage);
                    NavigationPage navigationPage1 = new NavigationPage(new Post());
                    navigationPage1.IconImageSource = "POSTS.png";
                    navigationPage1.Title = "POST";
                    Children.Add(navigationPage1);
                    NavigationPage navigationPage2 = new NavigationPage(new Explore());
                    navigationPage2.IconImageSource = "EXPLORE.png";
                    navigationPage2.Title = "FIND";
                    Children.Add(navigationPage2);
                    NavigationPage navigationPage4 = new NavigationPage(new Activity());
                    navigationPage4.IconImageSource = "ACTIVITY.png";
                    navigationPage4.Title = "ACTIVITY";
                    Children.Add(navigationPage4);
                    NavigationPage navigationPage3 = new NavigationPage(new Profile());
                    navigationPage3.IconImageSource = "PROFILE.png";
                    navigationPage3.Title = "ME";
                    Children.Add(navigationPage3);

                    //var serenaDataSync = new SerenaDataSync();
                    //var serenausersProfile = serenaDataSync.GetUsersdataAsync();

                }
                else
                {
                    NavigationPage navigationPage = new NavigationPage(new Today());
                    navigationPage.IconImageSource = "TODAY.png";
                    navigationPage.Title = "TODAY";
                    Children.Add(navigationPage);
                    NavigationPage navigationPage1 = new NavigationPage(new Post());
                    navigationPage1.IconImageSource = "POSTS.png";
                    navigationPage1.Title = "POST";
                    Children.Add(navigationPage1);
                    NavigationPage navigationPage2 = new NavigationPage(new Explore());
                    navigationPage2.IconImageSource = "EXPLORE.png";
                    navigationPage2.Title = "FIND";
                    Children.Add(navigationPage2);
                    NavigationPage navigationPage3 = new NavigationPage(new Profile());
                    navigationPage3.IconImageSource = "PROFILE.png";
                    navigationPage3.Title = "ME";
                    Children.Add(navigationPage3);
                }
                var pages = Children.GetEnumerator();
                pages.MoveNext();
                pages.MoveNext();
                pages.MoveNext();
                CurrentPage = pages.Current;
                CurrentPageChanged += MainPage_CurrentPageChanged;
                this.CurrentPageChanged += CurrentPageHasChanged;
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayTabbedPage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
         }

        private void MainPage_CurrentPageChanged(object sender, System.EventArgs e)
        {
            _lastActivePage?.Active(false);

            OnAppearing();
        }
        protected override void OnAppearing()
        {
            _lastActivePage = CurrentPage as IActivatePage;
            _lastActivePage?.Active(true);
        }

        private IActivatePage _lastActivePage;

        protected async void CurrentPageHasChanged(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var tabbedPage = (TabbedPage)sender;
                    Title = tabbedPage.CurrentPage.Title;
                    if (Title == "TODAY")
                    {
                        if (!string.IsNullOrEmpty(sessionEmailId))
                        {
                            //var homepage = (TodayHomePage)sender;
                            //homepage.BindingContext
                            NavigationPage todayHomePage = new NavigationPage(new TodayHomePage());
                        }
                        else
                        {
                            NavigationPage today = new NavigationPage(new Today());
                        }
                    }
                    if (Title == "POST")
                    {
                        NavigationPage post = new NavigationPage(new Post());
                    }
                    if (Title == "FIND")
                    {
                        NavigationPage explore = new NavigationPage(new Explore());
                    }
                    if (Title == "ME")
                    {
                        NavigationPage profile = new NavigationPage(new Profile());
                    }
                    if (Title == "ACTIVITY")
                    {
                        NavigationPage activity = new NavigationPage(new Activity());
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "TodayTabbedPage.Xaml";
                addlogFile.ExceptionEventName = "CurrentPageHasChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
 
    }
}