﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Model;
using System;
using Serena.Views.ProMember;
using Plugin.Toast;
using Xamarin.Essentials;
using System.Threading.Tasks;
using Serena.Helpers;
using Serena.DBService;
using System.Collections.Generic;
using System.Linq;
using Serena.Data;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HostView : ContentPage
    {

        string sessionEmailId, SHTime, watchpartyGUID;
        DateTime SHDate;
        public HostView(string WatchPartyGuid)
        {
            InitializeComponent();
            LoadInitialData(WatchPartyGuid);
        }

        private void LoadInitialData(string WatchPartyGuid)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //Hode edit WP section for users other than host
                    EditWPStacklayout.IsVisible = false;
                    watchpartyGUID = WatchPartyGuid;
                    //To GetSerenaUpcomingNotifications from API
                    //var serenaDataSync = new SerenaDataSync();
                    //Task.Run(() => serenaDataSync.GetSerenaUpcomingNotifications()).Wait();
                    //To GetUpcomingNotificationsBy watchpartyGUID
                    var findModel = App.DBIni.NotificationDB.GetUpcomingNotificationsByGuid(watchpartyGUID);
                    if (findModel != null)//If findmodel is not null
                    {
                        //To get  ScheduledDate and time value
                        SHDate = findModel.ScheduledDate; SHTime = findModel.ScheduledTime;
                        videoPlayer.IsVisible = true;
                        //To get sessionEmailId 
                        sessionEmailId = Settings.GeneralSettings;
                        //To GetSpecificUserdata By Email
                        var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(findModel.UserEmailId);
                        if (UserData != null)
                        {
                            //To display username
                            lblUName.Text = UserData.USERNAME;
                        }
                        #region Display video data
                        lblAbout.Text = findModel.About;
                        lblTags.Text = findModel.Tags;
                        lblTitle.Text = findModel.Title;
                        lblPlaylist.Text = "";
                        lblCategory.Text = findModel.Category;
                        lblLevel.Text = findModel.Mentions;//level of difficulties
                        if (lblLevel.Text.ToLower() == "easy")
                        {
                            lblLevel.BackgroundColor = Color.FromHex("#3A9938");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        if (lblLevel.Text.ToLower() == "intermediate")
                        {
                            lblLevel.BackgroundColor = Color.FromHex("#262626");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        if (lblLevel.Text.ToLower() == "hard")
                        {
                            lblLevel.BackgroundColor = Color.FromHex("#E14545");
                            lblLevel.Text = findModel.Mentions.ToUpper();
                        }
                        lblCategories.Text = findModel.Category;
                        lblDuration.Text = findModel.Duration;
                        #endregion

                        // watch party and watch party members data binding
                        #region comma seprated members
                        string nos = findModel.Members;
                        if (nos != "")
                        {
                            AttendeesList.IsVisible = true;
                            List<string> numbers = nos.Split(',').ToList<string>();
                            var watchPartylist = new List<WatchPartyList>();
                            foreach (var item in numbers)
                            {
                                var emailid = item;
                                var UserProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(emailid);
                                if (UserProfileData != null)
                                {
                                    WatchPartyList WPL = new WatchPartyList();
                                    WPL.UserName = UserProfileData.UserName;
                                    WPL.ProfileImageUrl = UserProfileData.UserProfileImage;
                                    watchPartylist.Add(WPL);
                                }

                            }
                            AttendeesList.ItemsSource = watchPartylist;
                            //GM = watchPartylist;
                        }
                        else
                        {
                            AttendeesList.IsVisible = false;
                        }
                        #endregion

                        //To watch party datetime data binding
                        #region Grid Date Time 
                        var shDate1 = SHDate.Date.ToString("MMMM dd");
                        string Datestr = shDate1.Substring(shDate1.Length - 2, 2);
                        string Monthstr = shDate1.Substring(0, 3);

                        datelbl.Text = Datestr + " " + Monthstr;
                        timelbl.Text = "@ " + SHTime;
                        #endregion

                        #region Button Text
                        TimeSpan Difference = Convert.ToDateTime(SHDate).Date - DateTime.Now.Date;
                        var Days = Difference.Days;
                        if (Days > 0)
                        {
                            join_nowbtn.Text = "STARTING IN " + Days + " DAYS";
                        }
                        else if (Days == 0)
                        {
                            join_nowbtn.Text = "STARTING SOON";
                        }
                        #endregion

                        #region For Watchparty hosts
                        if (findModel.HostEmailID == sessionEmailId)
                            EditWPStacklayout.IsVisible = true;
                        else
                            EditWPStacklayout.IsVisible = false;
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void WithdrawbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "WithdrawbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void join_nowbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    string buttontext = join_nowbtn.Text;
                    if (buttontext == "STARTING SOON")
                    {
                        await Navigation.PushAsync(new WaitingRoom(watchpartyGUID));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "join_nowbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void EditWPLbl_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new EditWatchParty(watchpartyGUID,SHDate));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "EditWPLbl_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void ClearBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //EditWPDrawer.ToggleDrawer();
                    //EditWPDrawer.IsOpen = false;
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "ClearBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        void SaveBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //WatchParty WPModel = new WatchParty();
                    //var returnValue = App.DBIni.WatchPartyDB.AddWatchParty(WPModel);
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "HostView.Xaml";
                addlogFile.ExceptionEventName = "SaveBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        //protected  override bool OnBackButtonPressed()
        //{
        //    base.OnBackButtonPressed();
        //    return true;
        //    await Navigation.PushAsync(new Activity());
        //}
    }
}