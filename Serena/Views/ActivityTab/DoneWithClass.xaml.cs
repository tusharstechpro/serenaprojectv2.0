﻿using Xam.Forms.VideoPlayer;
using Plugin.Toast;
using Serena.Data;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.SfBusyIndicator.XForms;
using System.Globalization;
using Serena.Views.ProMember;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DoneWithClass : ContentPage
    {
        string sessionEmailId;
        int TotalMin = 0;
        int TotalSec = 0;
        SfBusyIndicator busyIndicator = new SfBusyIndicator();
        public DoneWithClass()
        {
            InitializeComponent();
            LoadInitialData();
        }
        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To get sessionEmailId
                    sessionEmailId = Settings.GeneralSettings;
                    //To get LoggedIn users profile data
                    var profiledata = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                    if (profiledata != null)
                    {
                        niceworkLbl.Text = "Nice work " + profiledata.UserName + "!";
                    }
                    //To get count of total classes taken by LoggedIn user
                    var WPData = App.DBIni.NotificationDB.GetClasses(sessionEmailId);
                    classtakencntLbl.Text = WPData.Count().ToString();

                    //To get total count of classes attended and taken by LoggedIn user
                    var WatchPartyData = App.DBIni.NotificationDB.GetAcceptedWatchPartyDetails("1", sessionEmailId);
                    classescntLbl.Text = WatchPartyData.Count().ToString();

                    if (WatchPartyData != null)
                    {
                        var UploadClassesList = new List<Upcoming_Notifications>();
                        foreach (var WP in WatchPartyData)
                        {
                            #region Duration
                            string repStr = WP.Duration.Replace("Time: ", "");
                            string secs = repStr.Remove(0, 3);
                            string min = repStr.Substring(0, 2);

                            if (min != "00")
                                WP.Duration = min + " " + "MINS";

                            if (min == "01" || min == "1")
                                WP.Duration = min + " " + "MIN";

                            if (secs != "00")
                                WP.Duration = secs + " " + "SECS";
                            else
                                WP.Duration = min + " " + "MINS";

                            if (secs == "01" || secs == "1")
                                WP.Duration = secs + " " + "SEC";

                            //To get total min and sec of classes video
                            TotalMin = TotalMin + int.Parse(min);
                            TotalSec = TotalSec + int.Parse(secs);
                            #endregion
                            UploadClassesList.Add(WP);
                        }
                        //To calculate total min
                        int Minutes = TotalSec / 60;
                        mincntLbl.Text = (TotalMin + Minutes).ToString();
                        //If Totalmin is more than or equal to 1 day
                        if (TotalMin + Minutes >= 1440)
                        {
                            //Convert total min into days
                            TimeSpan elapsedTime = new TimeSpan(0, TotalMin + Minutes, 0);
                            daysLbl.Text = "You've been busy practicing wellness with us for " + elapsedTime.Days + " days" + " in a row! Keep it up by scheduling more classes, or saving them for later";
                        }
                        //If Totalmin is not more than or equal to 1 day
                        else
                        {
                            daysLbl.Text = "You've been busy practicing wellness with us for " + mincntLbl.Text + " Minutes" + " in a row! Keep it up by scheduling more classes, or saving them for later";
                        }
                        ClassesList.ItemsSource = UploadClassesList;
                    }

                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "DoneWithClass.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void ClearBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    SfBusyIndicator();
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "DoneWithClass.Xaml";
                addlogFile.ExceptionEventName = "ClearBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void SfBusyIndicator()
        {
            busyIndicator.IsBusy = true;
            busyIndicator.AnimationType = AnimationTypes.Cupertino;
            busyIndicator.ViewBoxHeight = 50;
            busyIndicator.ViewBoxWidth = 50;
            busyIndicator.EnableAnimation = true;
            busyIndicator.Title = "Loading...";
            busyIndicator.TextSize = 12;
            busyIndicator.FontFamily = "Cabin";
            busyIndicator.TextColor = Color.FromHex("#9900CC");
            this.Content = busyIndicator;
        }

        async void POSTButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    SfBusyIndicator();
                    await Navigation.PushAsync(new Post());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "DoneWithClass.Xaml";
                addlogFile.ExceptionEventName = "POSTButton_Clicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void CoverPhotoUrl_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //To get video url from tapped cover image
                    var imgTap = (Xamarin.Forms.Image)sender;
                    var item = (TapGestureRecognizer)imgTap.GestureRecognizers[0];
                    string MediaUrl = item.CommandParameter.ToString();
                    if (MediaUrl != null)
                    {
                        var prevPageName = "DoneWithClass";
                        await Navigation.PushAsync(new OpenVideo(MediaUrl, prevPageName));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "DoneWithClass.Xaml";
                addlogFile.ExceptionEventName = "CoverPhotoUrl_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}