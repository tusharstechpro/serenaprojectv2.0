﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThanksPopupScreen : ContentPage
    {
        public ThanksPopupScreen()
        {
            InitializeComponent();
            popupLoadingView.IsVisible = true;
        }
        async void Closebtn_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                popupLoadingView.IsVisible = false;
                await Navigation.PushAsync(new DoneWithClass());
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        async void Share_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                popupLoadingView.IsVisible = false;
                await Navigation.PushAsync(new DoneWithClass());
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
    }
}