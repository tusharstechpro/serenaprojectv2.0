﻿using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Syncfusion.XForms.Buttons;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;

namespace Serena.Views.ActivityTab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Notification : ContentPage
    {
         
        string sessionEmailId;
        public Notification()
        {
            InitializeComponent();
            LoadInitialData();

        }
        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    sessionEmailId = Settings.GeneralSettings;
                   // var serenaDataSync = new SerenaDataSync();
                   // Task.Run(() => serenaDataSync.GetSerenaNotifications()).Wait();
                    #region Notification list binding
                    var notifyData = App.DBIni.NotificationDB.GetNotificationsBySessionId(sessionEmailId);
                    if (notifyData != null)
                    {
                        TodayNotifyList.ItemsSource = null;
                        var RequestNotify = new List<Serena_Notifications>();
                        foreach (var notifyDetails in notifyData)
                        {
                            if (notifyDetails.category == "0")//Post Like
                            {

                                var Req = new Serena_Notifications();
                                string number = NumberToWords.ConvertAmount(double.Parse(notifyDetails.CountORRequestBy));
                                Req.NotificationLbl = number + " " + " people liked your photo";
                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(notifyDetails.LastUpdated) ?
                                  notifyDetails.LastUpdated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                #endregion
                                Req.LastUpdated = TimeAgo(convertToDatetime);
                                RequestNotify.Add(Req);
                            }
                            else if (notifyDetails.category == "1")//Incoming Request
                            {
                                var Req = new Serena_Notifications();
                                var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(notifyDetails.CountORRequestBy);
                                Req.CountORRequestBy = UserData.FULLNAME;
                                Req.NotificationLbl = " requested to follow you";
                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(notifyDetails.LastUpdated) ?
                                  notifyDetails.LastUpdated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                #endregion
                                Req.LastUpdated = TimeAgo(convertToDatetime);
                                RequestNotify.Add(Req);
                            }
                            else if (notifyDetails.category == "2")//Accepted request
                            {
                                var Req = new Serena_Notifications();
                                var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(notifyDetails.CountORRequestBy);
                                Req.CountORRequestBy = UserData.FULLNAME;
                                Req.NotificationLbl = " accepted your invitation!";
                                #region TimeAgo

                                DateTime convertToDatetime = Convert.ToDateTime(!string.IsNullOrEmpty(notifyDetails.LastUpdated) ?
                                  notifyDetails.LastUpdated : DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                                  CultureInfo.InvariantCulture);

                                #endregion
                                Req.LastUpdated = TimeAgo(convertToDatetime);
                                RequestNotify.Add(Req);
                            }
                        }
                        TodayNotifyList.ItemsSource = RequestNotify;
                    }
                    #endregion
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Notification.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //Accept button click event
        void AcceptbtnClicked(object sender, EventArgs e)
        {

        }

        //Reject button click event
        void RejectbtnClicked(object sender, EventArgs e)
        {

        }

        async void ClearbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    await Navigation.PushAsync(new Activity());
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Notification.Xaml";
                addlogFile.ExceptionEventName = "ClearbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }

    }
}