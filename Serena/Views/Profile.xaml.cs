﻿using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.Toast;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Serena.Views.ProMember;
using Syncfusion.DataSource.Extensions;
using Syncfusion.XForms.Buttons;
using Syncfusion.XForms.Graphics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace Serena.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile : ContentPage
    {
        string IsPro = "0";
        string sessionEmailId;
        string sessionSegmentValue;
        UserProfileData_Tran profileDataTran = new UserProfileData_Tran();
        UserProfileDataTran_Out profileDataTranOut = new UserProfileDataTran_Out();
        public ObservableCollection<Serena.ViewModel.Location> _mapLocations;
        public int _pinCreatedCount = 0;
        public string ResponseData;

        public Profile()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {

            try
            {
                LoadInitialData();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "OnAppearing";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    FollowersListMessageLbl.IsVisible = false;
                    //Set default visibility mode to public
                    if (PublicBtn.Text == "" || PublicBtn.Text == null)
                    {
                        PublicBtn.Text = "PUBLIC";
                    }
                    //Get the login user EmailId from Session
                    sessionEmailId = Settings.GeneralSettings;
                    sessionSegmentValue = SegmentSession.segmentsessionValue;
                    if (!string.IsNullOrEmpty(sessionEmailId))
                    {
                        var userprofileData = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                        if (userprofileData != null)
                        {
                            UserNameLbl.Text = userprofileData.UserName;//new change 24march by shraddhap
                            HandleNameLbl.Text = "@" + userprofileData.UserName; //new change 24march by shraddhap
                            ImageCircle.Source = userprofileData.UserProfileImage;
                            LocationLbl.Text = userprofileData.UserLocation;
                            BioLbl.Text = userprofileData.UserBio;
                            LinkLbl.Text = userprofileData.UserWebsite;
                            PublicBtn.Text = userprofileData.UserAccountType;
                            if (userprofileData.UserAccountType == null || userprofileData.UserAccountType == "")
                            {
                                PublicBtn.Text = "PUBLIC";
                            }
                            IsPro = Convert.ToString(userprofileData.IsPro);

                            #region Follow followibng list binding
                            var followUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(3);//Followers List
                            var followCount = followUsers.Count().ToString();
                            if (followCount != "0")
                            {
                                var addUserProfileFollowList = new List<UserProfileData>();
                                foreach (var user in followUsers)
                                {
                                    var followUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                    UserProfileData addFollowUserProfile = new UserProfileData();
                                    addFollowUserProfile.UserEmailId = followUsersdata.UserEmailId;
                                    addFollowUserProfile.UserName = followUsersdata.UserName;
                                    addFollowUserProfile.UserProfileImage = followUsersdata.UserProfileImage;
                                    addUserProfileFollowList.Add(addFollowUserProfile);
                                }
                                FollowersList.ItemsSource = null;
                                FollowersList.ItemsSource = addUserProfileFollowList;
                                FollowerTab.Title = followUsers.Count().ToString() + " " + "followers";
                            }

                            var requestUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(4);//Request List
                            var requestCount = requestUsers.Count().ToString();
                            if (requestCount != "0")
                            {
                                var addUserProfileRequestList = new List<UserProfileData>();
                                foreach (var user in requestUsers)
                                {
                                    var requestUsersData = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                    UserProfileData addrequestUserProfile = new UserProfileData();
                                    addrequestUserProfile.UserEmailId = requestUsersData.UserEmailId;
                                    addrequestUserProfile.UserName = requestUsersData.UserName;
                                    addrequestUserProfile.UserProfileImage = requestUsersData.UserProfileImage;
                                    addUserProfileRequestList.Add(addrequestUserProfile);
                                }
                                RequestList.ItemsSource = addUserProfileRequestList;
                            }
                            else if (requestCount == "0")
                            {
                                requestLbl.IsVisible = false;
                                RequestList.IsVisible = false;
                                RequestList.ItemsSource = null;
                            }

                            var followingUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(1);//followingUsers List
                            var followingCount = followingUsers.Count().ToString();
                            if (followingCount != "0")
                            {
                                var addUserProfileFollowingList = new List<UserProfileData>();
                                foreach (var user in followingUsers)
                                {
                                    var followingUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                    UserProfileData addFollowingUserProfile = new UserProfileData();
                                    addFollowingUserProfile.UserEmailId = followingUsersdata.UserEmailId;
                                    addFollowingUserProfile.UserName = followingUsersdata.UserName;
                                    addFollowingUserProfile.UserProfileImage = followingUsersdata.UserProfileImage;
                                    addUserProfileFollowingList.Add(addFollowingUserProfile);
                                }
                                FollowingList.ItemsSource = addUserProfileFollowingList;
                                FollowingTab.Title = followingUsers.Count().ToString() + " " + "following";
                            }

                            var suggestionsUsers = App.DBIni.UserProfileDataTranDB.GetSpecificUserByStatus(3);//suggestions List
                            var suggestionCount = suggestionsUsers.Count().ToString();
                            if (suggestionCount != "0")
                            {
                                var addUserProfileSuggestionList = new List<UserProfileData>();
                                foreach (var user in suggestionsUsers)
                                {
                                    var suggestionsUsersdata = App.DBIni.UserProfileDataDB.GetSpecificUser(user.UserEmailId);

                                    UserProfileData addsuggestionsUsersProfile = new UserProfileData();
                                    addsuggestionsUsersProfile.UserEmailId = suggestionsUsersdata.UserEmailId;
                                    addsuggestionsUsersProfile.UserName = suggestionsUsersdata.UserName;
                                    addsuggestionsUsersProfile.UserProfileImage = suggestionsUsersdata.UserProfileImage;
                                    addUserProfileSuggestionList.Add(addsuggestionsUsersProfile);
                                }
                                SuggestionsList.ItemsSource = addUserProfileSuggestionList;
                            }
                            #endregion
                        }
                        lblSignOut.IsVisible = true;

                        #region Profile posts tab
                        var postdata = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(sessionEmailId);
                        if (postdata != null)
                        {
                            var postImage = new List<Serena_Post>();
                            foreach (var postDetails in postdata)
                            {
                                if (postDetails.PostType == "Text")//Textpost
                                {
                                    var userTextPost = new Serena_Post();
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    var bgcolor = postDetails.PostBackgroundColor.Replace("#FF", "#");
                                    userTextPost.PostText = postDetails.PostText;
                                    userTextPost.PostColor = textcolor;
                                    userTextPost.PostBackgroundColor = bgcolor;
                                    postImage.Add(userTextPost);

                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText == "-")//ImagePost
                                {
                                    var userImagePost = new Serena_Post();
                                    userImagePost.ImageUrl = postDetails.ImageUrl;
                                    userImagePost.Opacity = postDetails.Opacity;
                                    postImage.Add(userImagePost);
                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText != "-")//ImagePost text overlay
                                {
                                    PostList.ItemsSource = null;
                                    var userImageTextPost = new Serena_Post();
                                    userImageTextPost.ImageUrl = postDetails.ImageUrl;
                                    userImageTextPost.PostText = postDetails.PostText;
                                    userImageTextPost.Opacity = postDetails.Opacity;
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    userImageTextPost.PostColor = textcolor;

                                    postImage.Add(userImageTextPost);
                                }
                            }
                            PostList.ItemsSource = postImage;
                            PostTab.Title = postImage.Count().ToString() + " " + "posts";
                        }
                        #endregion

                        #region promember condition
                        //lblPro.IsEnabled = true;

                        if (IsPro == "0")
                            lblPro.Text = "APPLY FOR PRO";
                        else
                            lblPro.Text = "PRO-MEMBER TOOLS";

                        #endregion
                    }
                    else
                    {
                        lblPro.Text = "APPLY FOR PRO";
                        //lblPro.IsEnabled = false;
                        //For guest user -- if session is blank
                        lblSignOut.IsVisible = false;
                    }
                }
                else
                {
                    lblPro.Text = "APPLY FOR PRO";
                    //CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        #region Button click events
        //Follow button click event
        void FollowBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var sfButton = sender as SfButton;
                    string RequestUserEmail = sfButton.CommandParameter.ToString();
                    Guid guid = Guid.NewGuid();
                    string Guidstr = guid.ToString();
                    Guidstr = (guid + RequestUserEmail);
                    //To chnage follow button UI
                    requestedBtnUI(sfButton);

                    #region add data in UserProfileTran and out table in SQLite   
                    profileDataTran.UserEmailId = profileDataTranOut.UserEmailId = sessionEmailId;
                    profileDataTran.RequestTo = profileDataTranOut.RequestTo = RequestUserEmail;
                    profileDataTran.Status = profileDataTranOut.Status = 0;//For sending req
                    profileDataTran.Guid = profileDataTranOut.Guid = Guidstr;
                    profileDataTran.LastUpdated = profileDataTranOut.LastSyncDate = DateTime.Now;// newly added
                    var returns = App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                    var returnss = App.DBIni.UserProfileDataTranDB.AddOutUser(profileDataTranOut);

                    #endregion

                    FollowersList.RefreshListViewItem();
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "FollowBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Following button click event
        async void FollowingBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    var sfButton = sender as SfButton;
                    bool answer = await DisplayAlert("", "You really want to unfollow this user?", "OK", "Cancel");
                    if (answer == true)
                    {
                        string UnfollowEmailId = sfButton.CommandParameter.ToString();
                        var userTranData = App.DBIni.UserProfileDataTranDB.GetGuidByUserEmailId(UnfollowEmailId);
                        string Guid = userTranData.Guid;
                        if (!string.IsNullOrEmpty(Guid))
                        {  //To change Unfollow button UI
                            UnfollowBtnUI(sfButton);
                            #region update status in UserProfileTran and out table in SQLite
                            profileDataTran.UserEmailId = profileDataTranOut.UserEmailId = sessionEmailId;
                            //profileDataTran.RequestTo = profileDataTranOut.RequestTo = UnfollowEmailId;
                            profileDataTran.Status = profileDataTranOut.Status = 3;//For unfollow
                            profileDataTran.Guid = profileDataTranOut.Guid = Guid;
                            profileDataTran.LastUpdated = profileDataTranOut.LastSyncDate = DateTime.Now;// newly added
                            var returns = App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                            var returnss = App.DBIni.UserProfileDataTranDB.AddOutUser(profileDataTranOut);
                            #endregion
                            LoadInitialData();
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "FollowingBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Reject button click event
        void RejectbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var sfButton = sender as SfButton;
                    string rejectedEmail = sfButton.CommandParameter.ToString();
                    var userTranData = App.DBIni.UserProfileDataTranDB.GetGuidByUserEmailId(rejectedEmail);
                    string Guid = userTranData.Guid;
                    if (!string.IsNullOrEmpty(Guid))
                    {
                        //To change reject button UI
                        rejectedBtnUI(sfButton);
                        #region update status in UserProfileTran and out table in SQLite
                        profileDataTran.UserEmailId = profileDataTranOut.UserEmailId = rejectedEmail;
                        profileDataTran.Status = profileDataTranOut.Status = 2;//For Rejected
                        profileDataTran.Guid = profileDataTranOut.Guid = Guid;
                        profileDataTran.LastUpdated = profileDataTranOut.LastSyncDate = DateTime.Now;// newly added
                        var returns = App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                        var returnss = App.DBIni.UserProfileDataTranDB.AddOutUser(profileDataTranOut);
                        #endregion
                    }
                    else
                        CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "RejectbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Accept button click event
        void AcceptbtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    var sfButton = sender as SfButton;
                    string acceptedEmail = sfButton.CommandParameter.ToString();
                    var userTranData = App.DBIni.UserProfileDataTranDB.GetGuidByUserEmailId(acceptedEmail);
                    var Guid = userTranData.Guid;
                    if (!string.IsNullOrEmpty(Guid))
                    {
                        //Tochange accept button UI
                        acceptedBtnUI(sfButton);
                        #region update status in UserProfileTran and out table in SQLite
                        profileDataTran.UserEmailId = profileDataTranOut.UserEmailId = acceptedEmail;
                        profileDataTran.Status = profileDataTranOut.Status = 1;//For Accepted
                        profileDataTran.Guid = profileDataTranOut.Guid = Guid;
                        profileDataTran.LastUpdated = profileDataTranOut.LastSyncDate = DateTime.Now;// newly added
                        var returns = App.DBIni.UserProfileDataTranDB.AddUser(profileDataTran);
                        var returnss = App.DBIni.UserProfileDataTranDB.AddOutUser(profileDataTranOut);
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "AcceptbtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Show the visibility popup on buttonclick
        async void PublicBtnClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (string.IsNullOrEmpty(sessionEmailId))
                {
                    bool answer = await DisplayAlert("", "You need to sign in first", "OK", "Cancel");
                    if (answer == true)//clicks OK
                    {
                        await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                    }
                    else //clicks Cancel
                    {

                    }
                }
                else
                {
                    popupLoadingView.IsVisible = true;
                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        private void ClearImageTapped(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = false;
        }

        //Change the button text if clicked on Stay Public Button
        void StayPublicBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    PublicBtn.Text = "PUBLIC";
                    var userprofileData = App.DBIni.ProfileDB.GetSpecificUser(sessionEmailId);
                    if (userprofileData != null)
                    {
                        userprofileData.UserAccountType = "PUBLIC";
                        // update UserFollowerJson into user profile table
                        App.DBIni.ProfileDB.AddUser(userprofileData);
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "StayPublicBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Change the button text if clicked on GoPrivate Button
        void GoPrivateBtnClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    PublicBtn.Text = "PRIVATE";
                    var userprofileData = App.DBIni.ProfileDB.GetSpecificUser(sessionEmailId);
                    if (userprofileData != null)
                    {
                        userprofileData.UserAccountType = "PRIVATE";
                        // update UserFollowerJson into user profile table
                        App.DBIni.ProfileDB.AddUser(userprofileData);
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "GoPrivateBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //hamburger Button click event
        void hamburgerButton_Clicked(object sender, EventArgs e)
        {
            navigationDrawer.ToggleDrawer();
        }
        #endregion

        #region Button UI Code
        private void UnfollowBtnUI(SfButton sfButton)
        {
            sfButton.IsEnabled = false;
            RequestList.SelectedItems.Clear();
            sfButton.TextColor = Color.FromHex("#000000");
            sfButton.BackgroundColor = Color.FromHex("#F4F4F4");
            sfButton.Text = "UNFOLLOW";
            sfButton.FontFamily = "Cabin";
            sfButton.FontSize = 12;
            sfButton.WidthRequest = 150;
            sfButton.HeightRequest = 40;
            SfLinearGradientBrush linearGradientBrush = new SfLinearGradientBrush();
            linearGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                        {
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                        };
            sfButton.BackgroundGradient = linearGradientBrush;
        }
        private static void requestedBtnUI(SfButton sfButton)
        {
            sfButton.IsEnabled = false;
            sfButton.TextColor = Color.FromHex("#000000");
            sfButton.BackgroundColor = Color.FromHex("#F4F4F4");
            sfButton.Text = "REQUESTED";
            sfButton.FontFamily = "Cabin";
            sfButton.FontSize = 12;
            sfButton.WidthRequest = 120;
            SfLinearGradientBrush linearGradientBrush = new SfLinearGradientBrush();
            linearGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                    new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                    new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
            sfButton.BackgroundGradient = linearGradientBrush;
        }
        private static void acceptedBtnUI(SfButton sfButton)
        {
            sfButton.IsEnabled = false;
            sfButton.TextColor = Color.FromHex("#000000");
            sfButton.BackgroundColor = Color.FromHex("#F4F4F4");
            sfButton.Text = "ACCEPTED";
            sfButton.FontFamily = "Cabin";
            sfButton.FontSize = 12;
            sfButton.WidthRequest = 150;
            sfButton.HeightRequest = 40;
            SfLinearGradientBrush linearGradientBrush = new SfLinearGradientBrush();
            linearGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                        {
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                        };
            sfButton.BackgroundGradient = linearGradientBrush;
        }
        private static void rejectedBtnUI(SfButton sfButton)
        {
            sfButton.IsEnabled = false;
            sfButton.TextColor = Color.FromHex("#000000");
            sfButton.BackgroundColor = Color.FromHex("#F4F4F4");
            sfButton.Text = "REJECTED";
            sfButton.FontFamily = "Cabin";
            sfButton.FontSize = 12;
            sfButton.WidthRequest = 150;
            sfButton.HeightRequest = 40;
            SfLinearGradientBrush linearGradientBrush = new SfLinearGradientBrush();
            linearGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                        {
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                            new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                        };
            sfButton.BackgroundGradient = linearGradientBrush;
        }
        #endregion

        #region Search in lists for followers/following/suggestionlist
        //Search present above followers list
        string FollowsearchBar = null;
        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            FollowsearchBar = FollowersSearch.Text;
            if (FollowersList.DataSource != null)
            {
                this.FollowersList.DataSource.Filter = SearchUserName;
                this.FollowersList.DataSource.RefreshFilter();
            }

        }
        private bool SearchUserName(object obj)
        {
            if (FollowsearchBar == null || FollowsearchBar == null)
                return true;

            var username = obj as UserProfileData;
            if (username.UserName.ToLower().Contains(FollowsearchBar.ToLower())
                 || username.UserName.ToLower().Contains(FollowsearchBar.ToLower()))
            {
                FollowersListMessageLbl.IsVisible = false;
                return true;
            }
            else
            {
                FollowersListMessageLbl.IsVisible = true;
                return false;
            }

        }

        //Search present above following list
        string followingSearchBar = null;
        private void FollowingSearchBar_TextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            followingSearchBar = FollowingSearch.Text;
            if (FollowingList.DataSource != null)
            {
                this.FollowingList.DataSource.Filter = FollowingSearchUserName;
                this.FollowingList.DataSource.RefreshFilter();
            }
        }
        private bool FollowingSearchUserName(object obj)
        {
            if (followingSearchBar == null || followingSearchBar == null)
                return true;

            var username = obj as UserProfileData;


            if (username.UserName.ToLower().Contains(followingSearchBar.ToLower())
                 || username.UserName.ToLower().Contains(followingSearchBar.ToLower()))
                return true;
            else
                return false;
        }

        //Search present above Suggestion list
        string suggSearchBar = null;
        private void SuggSearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            suggSearchBar = SuggestionsSearch.Text;
            if (SuggestionsList.DataSource != null)
            {
                this.SuggestionsList.DataSource.Filter = SuggSearchUserName;
                this.SuggestionsList.DataSource.RefreshFilter();
            }
        }
        private bool SuggSearchUserName(object obj)
        {
            if (suggSearchBar == null || suggSearchBar == null)
                return true;

            var username = obj as UserProfileData;
            if (username.UserName.ToLower().Contains(suggSearchBar.ToLower())
                 || username.UserName.ToLower().Contains(suggSearchBar.ToLower()))
                return true;
            else
                return false;
        }
        #endregion

        #region LabelTapped and SegmentedControl events
        async void FollowersUserNameLblClicked(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    var FollowLbl = (Xamarin.Forms.Label)sender;
                    string followUserName = FollowLbl.Text.ToString();

                    var item = (TapGestureRecognizer)FollowLbl.GestureRecognizers[0];
                    string selectedUserEmailId = item.CommandParameter.ToString();

                    var userdata = App.DBIni.UserProfileDataDB.GetSpecificUser(selectedUserEmailId);
                    if (userdata != null)
                    {
                        await Navigation.PushAsync(new FollowUsersPage(userdata.UserEmailId));
                    }
                    else
                    {
                        await DisplayAlert("", "No user found!", "");
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "FollowersUserNameLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        async void SocialProfileLblClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await Navigation.PushAsync(new SocialProfile(sessionEmailId, IsPro));
                //To close drawer when back button pressed
                navigationDrawer.ToggleDrawer();
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        async void ProMemberLblClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (lblPro.Text == "PRO-MEMBER TOOLS")
                {

                    //await Navigation.PushAsync(new SocialProfile(sessionEmailId));
                    await Navigation.PushAsync(new ProMemberTools(0, "", null));
                    //To close drawer when back button pressed
                    navigationDrawer.ToggleDrawer();
                }
                else if (lblPro.Text == "APPLY FOR PRO")
                {
                    if (IsPro == "0")
                    {
                        if (string.IsNullOrEmpty(sessionEmailId))
                        {
                            bool value = await DisplayAlert("", "Login to become a pro member", "OK", "Cancel");
                            if (value == true)
                                await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                        }
                        else
                            await Navigation.PushAsync(new BecomeProMember());
                    }
                    else
                        await DisplayAlert("Already a Pro-Member!!!", "OK", "Cancel");

                    navigationDrawer.ToggleDrawer();
                }
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        async void SecurityLblClicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await Navigation.PushAsync(new Security());
                //To close drawer when back button pressed
                navigationDrawer.ToggleDrawer();
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        async void Payment_Tapped(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (string.IsNullOrEmpty(sessionEmailId))
                {
                    bool value = await DisplayAlert("", "Login to access payment-subscriptions", "OK", "Cancel");
                    if (value == true)
                        await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                }
                else
                    await Navigation.PushAsync(new Payment());

                //To close drawer when back button pressed
                navigationDrawer.ToggleDrawer();
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
        #endregion
        async void lblSignOut_Clicked(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                //Clear the session before logout
                Settings.GeneralSettings = "";
                UserProfileSettings.ProfileSettings = "";
                if (Device.RuntimePlatform == Device.iOS)
                {
                    CrossFacebookClient.Current.Logout();
                    CrossGoogleClient.Current.Logout();
                }

                await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }

        void profileTab_SelectionChanged(object sender, Syncfusion.XForms.TabView.SelectionChangedEventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var selectedIndex = e.Index;
                    if (selectedIndex == 0)
                    {
                        var postdata = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(sessionEmailId);
                        if (postdata != null)
                        {
                            var postImage = new List<Serena_Post>();
                            foreach (var postDetails in postdata)
                            {
                                if (postDetails.PostType == "Text")//Textpost
                                {
                                    var userTextPost = new Serena_Post();
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    var bgcolor = postDetails.PostBackgroundColor.Replace("#FF", "#");
                                    userTextPost.PostText = postDetails.PostText;
                                    userTextPost.PostColor = textcolor;
                                    userTextPost.PostBackgroundColor = bgcolor;
                                    postImage.Add(userTextPost);

                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText == "-")//ImagePost
                                {
                                    var userImagePost = new Serena_Post();
                                    userImagePost.ImageUrl = postDetails.ImageUrl;
                                    userImagePost.Opacity = postDetails.Opacity;
                                    postImage.Add(userImagePost);
                                }
                                else if (postDetails.PostType == "Image" && postDetails.PostText != "-")//ImagePost text overlay
                                {
                                    PostList.ItemsSource = null;
                                    var userImageTextPost = new Serena_Post();
                                    userImageTextPost.ImageUrl = postDetails.ImageUrl;
                                    userImageTextPost.PostText = postDetails.PostText;
                                    userImageTextPost.Opacity = postDetails.Opacity;
                                    var textcolor = postDetails.PostColor.Replace("#FF", "#");
                                    userImageTextPost.PostColor = textcolor;

                                    postImage.Add(userImageTextPost);
                                }
                            }
                            PostList.ItemsSource = postImage;
                            PostTab.Title = postImage.Count().ToString() + " " + "posts";
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Profile.Xaml";
                addlogFile.ExceptionEventName = "profileTab";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void HelpCenter_Tapped(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
        }
    }
}