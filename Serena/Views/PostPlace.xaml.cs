﻿using Newtonsoft.Json;
using Serena.DBService;
using Serena.Model;
using static Serena.Data.Constants;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using static Serena.Model.Address;
using Plugin.Toast;
using Serena.Data;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostPlace : ContentPage
    {
        public string PostAddressValue;
        public double latitude;
        public double longitude;
        public PostPlace()
        {
            InitializeComponent();
            NextBtn.IsEnabled = false;
        }

        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        public async Task GetPlacesPredictionsAsync()
        {

            // TODO: Add throttle logic, Google begins denying requests if too many are made in a short amount of time

            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    CancellationToken cancellationToken = new CancellationTokenSource(TimeSpan.FromMinutes(2)).Token;

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format(GooglePlacesApiAutoCompletePath, GooglePlacesApiKey, WebUtility.UrlEncode(SearchText.Text))))
                    { //Be sure to UrlEncode the search term they enter

                        using (HttpResponseMessage message = await HttpClientInstance.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false))
                        {
                            if (message.IsSuccessStatusCode)
                            {
                                string json = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                                PlacesLocationPredictions predictionList = await Task.Run(() => JsonConvert.DeserializeObject<PlacesLocationPredictions>(json)).ConfigureAwait(false);

                                if (predictionList.Status == "OK")
                                {
                                    Addresses.Clear();

                                    if (predictionList.Predictions.Count > 0)
                                    {
                                        foreach (Prediction prediction in predictionList.Predictions)
                                        {
                                            Addresses.Add(new AddressInfo
                                            {
                                                Address = prediction.Description
                                            });
                                        }
                                    }
                                }
                                else
                                {
                                    AddressList.ItemsSource = null;
                                }
                            }
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostPlace.Xaml";
                addlogFile.ExceptionEventName = "GetPlacesPredictionsAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private async void NextBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreatePlacePost(PostAddressValue, latitude, longitude));
        }

        private async void CancelBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Post());
        }

        private async void OnTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(SearchText.Text))
                {
                    await GetPlacesPredictionsAsync();
                    AddressList.ItemsSource = Addresses;
                    NextBtn.TextColor = Color.FromHex("#9900CC");
                    NextBtn.IsEnabled = true;
                   
                }
                else if (string.IsNullOrEmpty(SearchText.Text))
                {
                    AddressList.ItemsSource = null;
                    NextBtn.TextColor = Color.FromHex("#A6A6A6");
                    NextBtn.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostPlace.Xaml";
                addlogFile.ExceptionEventName = "OnTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        async void AddressLblClicked(object sender, EventArgs e)
        {
            try
            {
                AddressList.ItemsSource = "";
                var PlaceAddress = (Xamarin.Forms.Label)sender;
                SearchText.Text = PlaceAddress.Text;
                PostAddressValue = PlaceAddress.Text;
                var locations = await Geocoding.GetLocationsAsync(PlaceAddress.Text);

                var location = locations?.FirstOrDefault();
                if (location != null)
                {
                    Position loc1 = new Position(location.Latitude,location.Longitude);
                    latitude = location.Latitude;
                    longitude = location.Longitude;
                    Pin marker1 = new Pin()
                    {
                        Address = PlaceAddress.Text,
                        Label = "Address",
                        Position = loc1,
                        Type = PinType.Place
                    };

                    map.Pins.Add(marker1);
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(marker1.Position, Distance.FromMeters(1000)));
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostPlace.Xaml";
                addlogFile.ExceptionEventName = "AddressLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Post());
            }
        }

        private void ClearBtnClicked(object sender, EventArgs e)
        {
            SearchText.Text = "";
            map.Pins.Clear();
            AddressList.ItemsSource = null;
        }
    }
}
