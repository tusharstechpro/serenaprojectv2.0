﻿using Plugin.Toast;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExploreByCategory : ContentPage
    {
        string tappedCategory = "";
        public ExploreByCategory(string selectedCategory)
        {
            try
            {
                InitializeComponent();
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    tappedCategory = selectedCategory;
                    lblCategory.Text = "Showing Results for " + selectedCategory.ToUpperInvariant();
                    var findclass = App.DBIni.UserProfileDataDB.GetTopFindClassByCategory(selectedCategory);
                    if (findclass.Count() > 0)
                        listClasses.ItemsSource = findclass;
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExploreByCategory.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        //Top Class search
        private void OnClassSearchTextChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtClassSearch.Text))
                {

                    var Searchtitle = App.DBIni.UserProfileDataDB.GetTopFindClassByCategory(tappedCategory).Where(p => p.Title.ToLower().Contains(txtClassSearch.Text.ToLower()) && p.Category.ToLower() == tappedCategory);
                    if (Searchtitle != null)
                    {
                        var addtopclassTitle = new List<SearchClassModel>();
                        foreach (var classTitle in Searchtitle)
                        {
                            var addClass = new SearchClassModel();
                            addClass.Title = classTitle.Title;
                            addClass.TotalReviews = classTitle.TotalReviews + " tags  > ";

                            addtopclassTitle.Add(addClass);
                        }

                        #region visibility

                        searchClasslist.ItemsSource = addtopclassTitle;

                        searchClasslist.IsVisible = true;
                        lblclasstopResults.IsVisible = true;

                        listClasses.IsVisible = false;
                        lblTopClassTitle.IsVisible = false;

                        #endregion
                    }

                }
                else if (string.IsNullOrEmpty(txtClassSearch.Text))
                {
                    searchClasslist.ItemsSource = null;
                    searchClasslist.IsVisible = false;
                    listClasses.IsVisible = true;
                    lblTopClassTitle.IsVisible = true;
                    lblclasstopResults.IsVisible = false;

                }
            }
            catch (Exception ex)
            {

                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExploreByCategory.Xaml";
                addlogFile.ExceptionEventName = "OnSearchTextChanged";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //class video
        async void ThumbNail_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var thumbNailImage = (Xamarin.Forms.Image)sender;
                    var item = (TapGestureRecognizer)thumbNailImage.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenClassVideo(selectedGuid));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExploreByCategory.Xaml";
                addlogFile.ExceptionEventName = "ThumbNail_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var anyWhereTap = (Xamarin.Forms.StackLayout)sender;
                    var item = (TapGestureRecognizer)anyWhereTap.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new OpenClassVideo(selectedGuid));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ExploreByCategory.Xaml";
                addlogFile.ExceptionEventName = "TapGestureRecognizer_Tapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


    }
}