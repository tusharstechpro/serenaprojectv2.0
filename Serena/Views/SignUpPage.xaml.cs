﻿using Newtonsoft.Json;
using Serena.Data;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using Serena.ViewModel;
using Serena.ViewModels;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        IOAuth2Service oAuth2Service;

        public SignUpPage()
        {
            InitializeComponent();
            try
            {
                this.BindingContext = new SocialSignuppageViewModel(oAuth2Service);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SignUpPage.xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
    }
}