﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Serena.Model.Address;
using static Serena.Data.Constants;
using System.Net;
using Syncfusion.SfPullToRefresh.XForms;
using Syncfusion.SfBusyIndicator.XForms;
using System.Text;

using Plugin.Toast;
using Xamarin.Essentials;
using Serena.Data;


namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        UserProfileData uProfileData = new UserProfileData();
        UserProfileData_Out upDataOut = new UserProfileData_Out();
        string EmailId;
        string EmailSession = "";
        string UserAccounttypetxt = "";
        string UserProfileImageURL = "";
        public ObservableCollection<AgeRange> AgeRangeCollection;
        string Homebasetxt;
        string Websitetxt;

        public ProfilePage(string HomeBase, string Website, string AccountType)
        {
            InitializeComponent();
            try
            {
                Homebasetxt = HomeBase;
                Websitetxt = Website;
                UserAccounttypetxt = AccountType;
                //pullToRefresh.Refreshed += PullToRefresh_Refreshed;
                LoadInitialData();

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ProfilePage.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }

        private async void SaveBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {


                    SfBusyIndicator busyIndicator = new SfBusyIndicator()
                    {
                        AnimationType = AnimationTypes.Cupertino,
                        ViewBoxHeight = 50,
                        ViewBoxWidth = 50,
                        EnableAnimation = true,
                        Title = "Loading...",
                        TextSize = 12,
                        FontFamily = "Cabin",
                        TextColor = Color.FromHex("#9900CC")

                    };

                    this.Content = busyIndicator;

                    //Get UserProfeData from Profile Page
                    if (EmailSession != null)
                    {
                        //Get specific user data from EmailId
                        var UserProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(EmailId);
                        if (UserProfileData != null)
                        {
                            #region add data in userProfile table in dynamo DB and SQLite
                            //Add UserProfileData to sqlite
                            uProfileData.UserEmailId = upDataOut.UserEmailId = EmailSession;
                            uProfileData.UserName = upDataOut.UserName = UserProfileData.UserName;///22/07/2020
                            uProfileData.UserProfileImage = upDataOut.UserProfileImage = UserProfileImageURL;
                            uProfileData.UserAccountType = upDataOut.UserAccountType = UserAccounttypetxt;
                            uProfileData.UserBio = upDataOut.UserBio = Bio.Text;
                            uProfileData.UserWebsite = upDataOut.UserWebsite = Websitetxt;
                            uProfileData.UserLocation = upDataOut.UserLocation = Homebasetxt;
                            uProfileData.UserAgeRange = upDataOut.UserAgeRange = AgeComboBox.Text;
                            uProfileData.UserPostJson = upDataOut.UserPostJson = UserProfileData.UserPostJson;
                            uProfileData.LastUpdated = upDataOut.LastSyncDate = DateTime.Now;
                            upDataOut.Status = "1";
                            var retrunvalue = App.DBIni.UserProfileDataDB.AddUser(uProfileData);
                            var retrunvalues = App.DBIni.UserProfileDataDB.AddUserOut(upDataOut);
                            this.IsBusy = false;
                            //Clear session
                            UserProfileSettings.ProfileSettings = "";
                            await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));//navigate to login page after apply for pro member
                            #endregion
                        }
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "LocationPage.Xaml";
                addlogFile.ExceptionEventName = "SaveBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                await Navigation.PushAsync(new Profile());
            }
        }

        //Add photo on page and save to s3 bucket
        async void AddPhotoClick(object sender, System.EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await CrossMedia.Current.Initialize();

                //If pickedup profile image format is not supported to device
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("Not Supported", "Device currently not support this functionality", "OK");
                    return;
                }
                //Adjust the size of Profile Image
                var mediaOption = new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Small,
                    CompressionQuality = 50
                };

                //Get the Profile Image path
                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOption);
                if (selectedImageFile != null)
                {
                    string filename = Path.GetFileName(selectedImageFile.Path);
                    //Replace space beween the filename with +
                    string UserFileName = filename.Replace(" ", "+");

                    //if Profile Image is null
                    if (PhotoBtn == null)
                    {
                        await DisplayAlert("Error", "could not get the image, please try again.", "OK");
                        return;
                    }
                    //Set Profile Image
                    PhotoBtn.BackgroundImage = ImageSource.FromStream(() => selectedImageFile.GetStream());
                    PhotoLbl.Text = "";
                    try
                    {
                        if (current == NetworkAccess.Internet)
                        {
                            //S3 bucket call to save the profile image
                            var awsKey = _accessKey;
                            var awsSecretKey = _secretKey;

                            client = new AmazonS3Client(awsKey, awsSecretKey, bucketRegion);
                            var putRequest2 = new PutObjectRequest
                            {
                                BucketName = ProfilePagebucketName,
                                Key = UserFileName,
                                //FilePath = filePath,
                                ContentType = "image/jpeg",
                                InputStream = selectedImageFile.GetStream(),
                                CannedACL = S3CannedACL.PublicReadWrite
                            };
                            //To get the profile image URL
                            UserProfileImageURL = "https://profilessimages.s3.us-east-2.amazonaws.com/" + UserFileName;
                            putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
                            PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
                    }
                    catch (AmazonS3Exception amazonS3Exception)
                    {
                        if (amazonS3Exception.ErrorCode != null &&
                            (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                            ||
                            amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                        {
                            await DisplayAlert("InvalidAccessKeyId", "Check the provided AWS Credentials", "OK");
                            return;
                        }
                        else
                        {
                            await DisplayAlert("Error occurred:", amazonS3Exception.Message, "OK");
                            return;
                        }
                    }
                }

            }
            else
                CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

        }
        private static HttpClient _httpClientInstance;
        public static HttpClient HttpClientInstance => _httpClientInstance ?? (_httpClientInstance = new HttpClient());

        private ObservableCollection<AddressInfo> _addresses;
        public ObservableCollection<AddressInfo> Addresses
        {
            get => _addresses ?? (_addresses = new ObservableCollection<AddressInfo>());
            set
            {
                if (_addresses != value)
                {
                    _addresses = value;
                    OnPropertyChanged();
                }
            }
        }

        private void LoadInitialData()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {

                    //NEXTBtn.IsEnabled = false;
                    string session = Settings.GeneralSettings;
                    EmailSession = session;
                    if (!string.IsNullOrEmpty(session))
                    {
                        EmailId = session;
                        //var data = signupDB.GetUsers();
                        var UserData = App.DBIni.SignUpDB.GetSpecificUserByEmail(EmailId);
                        if (UserData != null)
                        {
                            string firstname = UserData.FULLNAME.Split(' ').First();
                            WelcomeLbl.Text = "Welcome" + " " + firstname + "!";
                        }
                        //var GetAllUserProfileData = App.DBIni.UserProfileDataDB.GetUsers();
                        var UserProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(EmailId);
                        if (UserProfileData != null)
                        {
                            PhotoBtn.BackgroundImage = UserProfileData.UserProfileImage;
                            Bio.Text = UserProfileData.UserBio;
                            AgeComboBox.Text = UserProfileData.UserAgeRange;
                            UserProfileImageURL = UserProfileData.UserProfileImage;
                            if (UserProfileData.UserProfileImage == "-")
                            {
                                PhotoLbl.Text = "Add a Photo";
                            }
                            else
                            {
                                PhotoLbl.Text = "";
                            }
                        }
                        //Add ageRangedata to combolist
                        var AgeRangeList = new List<AgeRange>();
                        var allAgeRangeData = App.DBIni.AgeDB.GetAgeData();
                        if (allAgeRangeData != null)
                        {
                            foreach (var ageRangeInfo in allAgeRangeData)
                            {
                                AgeRange addAgeRange = new AgeRange();
                                addAgeRange.AgeRangeName = ageRangeInfo.AgeRangeName;
                                AgeRangeList.Add(addAgeRange);
                            }
                            AgeComboBox.DataSource = AgeRangeList;
                        }
                    }
                    //navigate to the LoginPage
                    else
                    {
                        DisplayAlert("Alert", "Please Login!", "OK");
                        Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ProfilePage.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                Navigation.PushAsync(new Profile());
            }
        }
    }
}