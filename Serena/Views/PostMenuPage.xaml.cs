﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Serena.DBService;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostMenuPage : ContentPage
    {
        public PostMenuPage()
        {
            InitializeComponent();
            LoadInitialData();
        }
        private void LoadInitialData()
        {
            try
            {
                //Tap Gesture Recognizer  
                var libraryTap = new TapGestureRecognizer();
                libraryTap.Tapped += async (sender, e) =>
                {
                    DefaultBackground();
                    libraryLbl.TextColor = Color.FromHex("#000000");
                    cameraLbl.TextColor = Color.FromHex("#B7B7B7");
                    textLbl.TextColor = Color.FromHex("#B7B7B7");
                    placeLbl.TextColor = Color.FromHex("#B7B7B7");

                    try
                    {
                        await CrossMedia.Current.Initialize();

                        //If pickedup profile image format is not supported to device
                        if (!CrossMedia.Current.IsPickPhotoSupported)
                        {
                            await DisplayAlert("Not Supported", "Device currently not support this functionality", "OK");
                            return;
                        }
                        //Adjust the size of Profile Image
                        var mediaOption = new PickMediaOptions()
                        {
                            PhotoSize = PhotoSize.Small,
                            CompressionQuality = 50
                        };

                        //Get the Profile Image path
                        var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOption);
                        if (selectedImageFile != null)
                        {
                            await Navigation.PushAsync(new PostImage(selectedImageFile));
                        }
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    stckLibrary.BackgroundColor = Color.White;
                };
                stckLibrary.GestureRecognizers.Add(libraryTap);
                var cameraTap = new TapGestureRecognizer();
                cameraTap.Tapped += async (sender, e) =>
                {
                    DefaultBackground();
                    cameraLbl.TextColor = Color.FromHex("#000000");
                    libraryLbl.TextColor = Color.FromHex("#B7B7B7");
                    textLbl.TextColor = Color.FromHex("#B7B7B7");
                    placeLbl.TextColor = Color.FromHex("#B7B7B7");
                    VideoLbl.TextColor = Color.FromHex("#B7B7B7");

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                        return;
                    }

                    var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                        Directory = "Sample",
                        Name = "test.jpg",
                        CompressionQuality = 50
                    });

                    if (file == null)
                        return;
                    var selectedImageFile = file;
                    if (selectedImageFile != null)
                    {
                        await Navigation.PushAsync(new PostImage(selectedImageFile));
                    }
                    stckCamera.BackgroundColor = Color.White;
                };
                stckCamera.GestureRecognizers.Add(cameraTap);
                var placeTap = new TapGestureRecognizer();
                placeTap.Tapped += async (sender, e) =>
                {
                    DefaultBackground();
                    placeLbl.TextColor = Color.FromHex("#000000");
                    stckPlace.BackgroundColor = Color.White;
                    libraryLbl.TextColor = Color.FromHex("#B7B7B7");
                    cameraLbl.TextColor = Color.FromHex("#B7B7B7");
                    textLbl.TextColor = Color.FromHex("#B7B7B7");
                    VideoLbl.TextColor = Color.FromHex("#B7B7B7");
                    await Navigation.PushAsync(new PostPlace());
                };
                stckPlace.GestureRecognizers.Add(placeTap);
                var textTap = new TapGestureRecognizer();
                textTap.Tapped += async (sender, e) =>
                {
                    DefaultBackground();
                    textLbl.TextColor = Color.FromHex("#000000");
                    libraryLbl.TextColor = Color.FromHex("#B7B7B7");
                    cameraLbl.TextColor = Color.FromHex("#B7B7B7");
                    placeLbl.TextColor = Color.FromHex("#B7B7B7");
                    VideoLbl.TextColor = Color.FromHex("#B7B7B7");
                    try
                    {
                        await Navigation.PushAsync(new PostText());
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    stckText.BackgroundColor = Color.White;
                };
                stckText.GestureRecognizers.Add(textTap);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostMenuPage.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                Navigation.PushAsync(new Post());
            }
        }
        public void DefaultBackground()
        {
            stckLibrary.BackgroundColor = Color.White;
            stckCamera.BackgroundColor = Color.White;
            stckPlace.BackgroundColor = Color.White;
            stckText.BackgroundColor = Color.White;
        }

        //Video Label click event
        async void VideoLblClicked(object sender, EventArgs e)
        {
            try
            {
                DefaultBackground();
                VideoLbl.TextColor = Color.FromHex("#000000");
                textLbl.TextColor = Color.FromHex("#B7B7B7");
                libraryLbl.TextColor = Color.FromHex("#B7B7B7");
                cameraLbl.TextColor = Color.FromHex("#B7B7B7");
                placeLbl.TextColor = Color.FromHex("#B7B7B7");


                if (!CrossMedia.Current.IsPickVideoSupported)
                {
                    await DisplayAlert("Videos Not Supported", ":( Permission not granted to videos.", "OK");
                    return;
                }
                //Pick video file from device
                var selectedvideoFile = await CrossMedia.Current.PickVideoAsync();
                if (selectedvideoFile != null)
                {
                    //Get video path
                    var path = selectedvideoFile.Path;
                    //Get Video Length
                    var duration = DependencyService.Get<VideoUploadInterface>().getVideoLength(path);
                    ImageSource thumbnail = DependencyService.Get<VideoUploadInterface>().GenerateThumbImage(path ,6);
                    //Get last 2 digit from video length
                    var Seconds = duration.Substring(duration.Length - 2);
                    //Convert seconds in Integer value
                    var exactDuration = Convert.ToInt32(Seconds);
                    //If video duration is less than or equal to 30
                    if (exactDuration <= 30)
                    {
                        //show popup as video selected with video file location
                        await DisplayAlert("Video Selected", "Location: " + selectedvideoFile.Path, "OK");
                        //If path is not null
                        if (selectedvideoFile.Path != null)
                        {
                            //navigate to post video page to show video
                            await Navigation.PushAsync(new CreateVideoPost(selectedvideoFile, thumbnail));

                        }
                        stckText.BackgroundColor = Color.White;
                    }
                    //else show alert popup that video limit is 30 seconds 
                    else
                    {
                        await DisplayAlert("Video limit is 30 Seconds", "", "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "PostMenuPage.Xaml";
                addlogFile.ExceptionEventName = "VideoLblClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
    }
}