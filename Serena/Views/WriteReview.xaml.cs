﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WriteReview : ContentPage
    {
        string reviewGuid;
        public WriteReview(string guid)
        {
            InitializeComponent();
            reviewGuid = guid;
            popupLoadingView.IsVisible = true;

        }
        async void SaveBtnClicked(object sender, EventArgs e)
        {
            try
            {

                #region adding reviews for particular post

                var serenaPostReview = new SerenaReviews();
                var serenaPostReview_out = new SerenaReviews_Out();

                serenaPostReview_out.Guid = serenaPostReview.Guid = reviewGuid;
                serenaPostReview_out.Review = serenaPostReview.Review = txtReview.Text;
                serenaPostReview_out.Rating = serenaPostReview.Rating = Math.Round(sfRating2.Value, 1);
                serenaPostReview_out.UserEmailId = serenaPostReview.UserEmailId = Settings.GeneralSettings;
                serenaPostReview_out.DateAdded = serenaPostReview.DateAdded = DateTime.Now.ToString("MM/dd/yyyy");
                serenaPostReview_out.LastSyncDate = DateTime.Now;

                var userProfileData = App.DBIni.SignUpDB.GetSpecificUserByEmail(Settings.GeneralSettings);
                if (userProfileData != null)
                    serenaPostReview_out.UserName = serenaPostReview.UserName = userProfileData != null ? userProfileData.FULLNAME : string.Empty;

                App.DBIni.ReviewsDB.AddReview(serenaPostReview);
                App.DBIni.ReviewsDB.AddReview_Out(serenaPostReview_out);

                #endregion

                //#region saving the data to rds
                //var current = Connectivity.NetworkAccess;
                //if (current == NetworkAccess.Internet)
                //{
                //    var serenaDataSync = new SerenaDataSync();
                //    Task.Run(() => serenaDataSync.PostSerenaReviewsByGuid(serenaPostReview));
                //}
                //#endregion

                popupLoadingView.IsVisible = false;
                txtReview.Text = "";
                sfRating2.Value = 0;
                await Navigation.PushAsync(new OpenClassVideo(reviewGuid));

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "WriteReview.Xaml";
                addlogFile.ExceptionEventName = "SaveBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        async void CloseBtnClicked(object sender, EventArgs e)
        {
            txtReview.Text = "";
            sfRating2.Value = 0;
            popupLoadingView.IsVisible = false;
            await Navigation.PushAsync(new OpenClassVideo(reviewGuid));

        }
    }
}