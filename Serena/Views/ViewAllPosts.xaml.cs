﻿using Plugin.Toast;
using Serena.DBService;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Data;
using Syncfusion.SfBusyIndicator.XForms;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewAllPosts : ContentPage
    {
        string sessionEmailId;
        public ViewAllPosts()
        {
            InitializeComponent();
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    LoadInitialData();
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ViewAllPosts.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        private void LoadInitialData()
        {
            try
            {
                //To get current emailId from session
                sessionEmailId = Settings.GeneralSettings;
                var postImage = new List<Serena_Post>();
                //To get LoggedIn user's UserProfileData
                    var userProfileData = App.DBIni.UserProfileDataDB.GetSpecificUser(sessionEmailId);
                    if(userProfileData != null)
                    {
                    //To get LoggedIn user's all post data
                        var postData = App.DBIni.SerenaPostDB.GetAllSerenaPostsBySessionId(sessionEmailId);
                        if (postData != null)
                        {
                            foreach (var posts in postData)
                            {
                                if (posts.PostType == "Text")
                                {
                                    posts.UserName = userProfileData.UserName;
                                    posts.UserProfileImage = userProfileData.UserProfileImage;
                                    posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                                   
                                #region TimeAgo
                                    // Extension method for time ago
                                    posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                    #endregion
                                    postImage.Add(posts);
                                }
                                else if (posts.PostType == "Image" && posts.PostText == "-")//Image
                                {
                                    posts.UserName = userProfileData.UserName;
                                posts.UserProfileImage = userProfileData.UserProfileImage;
                                posts.PostBackgroundColor = Color.Black.ToHex();
                                posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                                
                                #region TimeAgo
                                // Extension method for time ago
                                posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                    #endregion
                                    postImage.Add(posts);
                                }
                                else if (posts.PostType == "Image" && posts.PostText != "-")//Image overlay
                                {
                                posts.UserName = userProfileData.UserName;
                                posts.UserProfileImage = userProfileData.UserProfileImage;
                                posts.PostBackgroundColor = Color.Black.ToHex();
                                posts.FavouriteImage = !string.IsNullOrEmpty(Convert.ToString(posts.Favourite)) && Convert.ToString(posts.Favourite) == "0" ? "LikeIcon.png" : "PurpleHeart.png";
                                
                                #region TimeAgo
                                // Extension method for time ago
                                posts.TimeOfPost = TimeAgo(posts.LastCreated);
                                    #endregion
                                    postImage.Add(posts);
                                }
                            }
                        }
                }
                HomePostList.ItemsSource = postImage;
                //Hide PostList if no post available
                if (postImage.Count == 0) { HomePostList.IsVisible = false; }
                else { HomePostList.IsVisible = true; }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ViewAllPosts.Xaml";
                addlogFile.ExceptionEventName = "LoadInitialData";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private static string TimeAgo(DateTime tempDate)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(tempDate);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} mins ago", timeSpan.Minutes) :
                    "1 min ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hrs ago", timeSpan.Hours) :
                    "1 hr ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "1 day ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "1 month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "1 year ago";
            }
            return result;
        }
        void likeImageTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    int tapCount = 0;
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    if (selectedImage.File.ToLower() == "likeicon.png")
                        tapCount++;
                    else if (selectedImage.File.ToLower() == "PurpleHeart.png")
                        tapCount = 0;

                    if (tapCount % 2 == 0)
                    {
                        imageSource.Source = "LikeIcon.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 0);

                        #endregion region

                        #region for dislike post

                        var current1 = Connectivity.NetworkAccess;
                        if (current1 == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();

                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 0;
                            serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                        #endregion
                    }
                    else
                    {
                        imageSource.Source = "PurpleHeart.png";

                        #region update post favourite column in sqllite

                        var data = App.DBIni.SerenaPostDB.UpdateFavouritePost(selectedGuid, 1);

                        #endregion region

                        #region for like post  

                        var current1 = Connectivity.NetworkAccess;
                        if (current1 == NetworkAccess.Internet)
                        {
                            Serena_Post post = new Serena_Post();

                            var serenaDataSync = new SerenaDataSync();
                            post.UserEmailId = sessionEmailId;
                            post.Guid = selectedGuid;
                            post.Favourite = 1;
                            serenaDataSync.PostSerenaFavourite(post);
                        }
                        else
                            CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

                        #endregion

                        #region like count
                        //HomePostList.ItemTemplate = new DataTemplate(() => {
                        //    var grid = new Grid();
                        //    var name = new Label { FontSize = 14 };
                        //    name.SetBinding(Label.TextProperty, new Binding("FavCount"));
                        //    grid.Children.Add(name);
                        //    return grid;
                        //});
                        #endregion
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ViewAllPosts.Xaml";
                addlogFile.ExceptionEventName = "ImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        async void CommentImageTapped(object sender, EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    var imageSource = (Image)sender;
                    var selectedImage = imageSource.Source as FileImageSource;

                    var item = (TapGestureRecognizer)imageSource.GestureRecognizers[0];
                    string selectedGuid = item.CommandParameter.ToString();

                    await Navigation.PushAsync(new NavigationPage(new Reviews(selectedGuid, "ViewAllPosts", "")));
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "ViewAllPosts.Xaml";
                addlogFile.ExceptionEventName = "CommentImageTapped";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
    }
}