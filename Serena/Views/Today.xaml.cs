﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Serena.Helpers;
using Syncfusion.XForms.Graphics;
using System;
using Serena.Model;
using Serena.DBService;
using System.Collections.Generic;
using Syncfusion.XForms.Buttons;
using Xamarin.Essentials;
using Plugin.Toast;
using Serena.Data;

[assembly: ExportFont("Cabin-Regular.ttf", Alias = "Cabin")]
namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Today : ContentPage
    {
        public string SessionEmail;
        public Today()
        {
            //If active user is present in session
            InitializeComponent();
            try
            {
                LoadInitialData();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "Init";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }

        }
        public async void TopicsFollowBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (string.IsNullOrEmpty(SessionEmail))
                    {
                        //change the follow button text as following
                        var sfButton = sender as SfButton;
                        string sfButtontext = sfButton.Text;
                        if (sfButtontext == "FOLLOW")
                        {
                            bool answer = await DisplayAlert("", "You can only follow as a member", "OK", "Cancel");
                            if (answer == true)
                            {
                                //Settings.GeneralSettings = "";
                                string session = Settings.GeneralSettings;
                                if (!string.IsNullOrEmpty(session))
                                {
                                    sfButton.TextColor = Color.Black;
                                    sfButton.Text = "FOLLOWING";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
                                    sfButton.BackgroundGradient = radialGradientBrush;
                                }
                                else
                                {
                                    //chnage the button text as Follow and background color
                                    sfButton.TextColor = Color.White;
                                    sfButton.Text = "FOLLOW";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                        { new SfGradientStop(){ Color = Color.FromHex("#650486"), Offset = 0 },
                                            new SfGradientStop(){ Color = Color.FromHex("#9900CC"), Offset = 1 }};

                                    sfButton.BackgroundGradient = radialGradientBrush;
                                    //navigate to LoginPage()
                                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                                }
                            }
                        }

                    }
                    else
                    {
                        await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "TopicsFollowBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async void painBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (string.IsNullOrEmpty(SessionEmail))
                    {
                        //change the follow button text as following
                        var sfButton = sender as SfButton;
                        string sfButtontext = sfButton.Text;
                        if (sfButtontext == "FOLLOW")
                        {
                            bool answer = await DisplayAlert("", "You can only follow as a member", "OK", "Cancel");
                            if (answer == true)
                            {
                                //Settings.GeneralSettings = "";
                                string session = Settings.GeneralSettings;
                                if (!string.IsNullOrEmpty(session))
                                {
                                    sfButton.TextColor = Color.Black;
                                    sfButton.Text = "FOLLOWING";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
                                    sfButton.BackgroundGradient = radialGradientBrush;
                                }
                                else
                                {
                                    //chnage the button text as Follow and background color
                                    sfButton.TextColor = Color.White;
                                    sfButton.Text = "FOLLOW";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                        { new SfGradientStop(){ Color = Color.FromHex("#650486"), Offset = 0 },
                                            new SfGradientStop(){ Color = Color.FromHex("#9900CC"), Offset = 1 }};

                                    sfButton.BackgroundGradient = radialGradientBrush;
                                    //navigate to LoginPage()
                                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                                }
                            }
                        }

                    }
                    else
                    {
                        await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "painBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        public async void journeyBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (string.IsNullOrEmpty(SessionEmail))
                    {
                        //change the follow button text as following
                        var sfButton = sender as SfButton;
                        string sfButtontext = sfButton.Text;
                        if (sfButtontext == "FOLLOW")
                        {
                            bool answer = await DisplayAlert("", "You can only follow as a member", "OK", "Cancel");
                            if (answer == true)
                            {
                                //Settings.GeneralSettings = "";
                                string session = Settings.GeneralSettings;
                                if (!string.IsNullOrEmpty(session))
                                {
                                    sfButton.TextColor = Color.Black;
                                    sfButton.Text = "FOLLOWING";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
                                    sfButton.BackgroundGradient = radialGradientBrush;
                                }
                                else
                                {
                                    //chnage the button text as Follow and background color
                                    sfButton.TextColor = Color.White;
                                    sfButton.Text = "FOLLOW";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                        { new SfGradientStop(){ Color = Color.FromHex("#650486"), Offset = 0 },
                                            new SfGradientStop(){ Color = Color.FromHex("#9900CC"), Offset = 1 }};

                                    sfButton.BackgroundGradient = radialGradientBrush;
                                    //navigate to LoginPage()
                                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                                }
                            }
                        }

                    }
                    else
                    {
                        await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "journeyBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        public async void cookingBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (string.IsNullOrEmpty(SessionEmail))
                    {
                        //change the follow button text as following
                        var sfButton = sender as SfButton;
                        string sfButtontext = sfButton.Text;
                        if (sfButtontext == "FOLLOW")
                        {
                            bool answer = await DisplayAlert("", "You can only follow as a member", "OK", "Cancel");
                            if (answer == true)
                            {
                                //Settings.GeneralSettings = "";
                                string session = Settings.GeneralSettings;
                                if (!string.IsNullOrEmpty(session))
                                {
                                    sfButton.TextColor = Color.Black;
                                    sfButton.Text = "FOLLOWING";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
                                    sfButton.BackgroundGradient = radialGradientBrush;
                                }
                                else
                                {
                                    //chnage the button text as Follow and background color
                                    sfButton.TextColor = Color.White;
                                    sfButton.Text = "FOLLOW";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                        { new SfGradientStop(){ Color = Color.FromHex("#650486"), Offset = 0 },
                                            new SfGradientStop(){ Color = Color.FromHex("#9900CC"), Offset = 1 }};

                                    sfButton.BackgroundGradient = radialGradientBrush;
                                    //navigate to LoginPage()
                                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                                }
                            }
                        }

                    }
                    else
                    {
                        await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
                    }
                }
                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "cookingBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        public async void yogaposesBtnClicked(object sender, System.EventArgs e)
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (string.IsNullOrEmpty(SessionEmail))
                    {
                        //change the follow button text as following
                        var sfButton = sender as SfButton;
                        string sfButtontext = sfButton.Text;
                        if (sfButtontext == "FOLLOW")
                        {
                            bool answer = await DisplayAlert("", "You can only follow as a member", "OK", "Cancel");
                            if (answer == true)
                            {
                                //Settings.GeneralSettings = "";
                                string session = Settings.GeneralSettings;
                                if (!string.IsNullOrEmpty(session))
                                {
                                    sfButton.TextColor = Color.Black;
                                    sfButton.Text = "FOLLOWING";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                {
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 0 },
                                     new SfGradientStop(){ Color = Color.FromHex("#F4F4F4"), Offset = 1 }
                                };
                                    sfButton.BackgroundGradient = radialGradientBrush;
                                }
                                else
                                {
                                    //chnage the button text as Follow and background color
                                    sfButton.TextColor = Color.White;
                                    sfButton.Text = "FOLLOW";
                                    sfButton.FontFamily = "Cabin";
                                    sfButton.FontSize = 12;


                                    SfRadialGradientBrush radialGradientBrush = new SfRadialGradientBrush();
                                    radialGradientBrush.GradientStops = new Syncfusion.XForms.Graphics.GradientStopCollection()
                                        { new SfGradientStop(){ Color = Color.FromHex("#650486"), Offset = 0 },
                                            new SfGradientStop(){ Color = Color.FromHex("#9900CC"), Offset = 1 }};

                                    sfButton.BackgroundGradient = radialGradientBrush;
                                    //navigate to LoginPage()
                                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                                }
                            }
                        }

                    }
                    else
                    {
                        await Navigation.PushAsync(new NavigationPage(new TodayHomePage()));
                    }
                }

                else
                    CrossToastPopUp.Current.ShowCustomToast(Constants.ToastMessage, Constants.BgColor, Constants.TextColor);
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Today.Xaml";
                addlogFile.ExceptionEventName = "yogaposesBtnClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }
        private void LoadInitialData()
        {
            string session = Settings.GeneralSettings;
            SessionEmail = session;

            #region Adding test data in Topics
            Topics top1 = new Topics();
            top1.TopicName = "Yoga";
            App.DBIni.TopicsDB.AddTopics(top1);
            Topics top2 = new Topics();
            top2.TopicName = "Meditation";
            App.DBIni.TopicsDB.AddTopics(top2);
            Topics top3 = new Topics();
            top3.TopicName = "Pilates";
            App.DBIni.TopicsDB.AddTopics(top3);
            Topics top4 = new Topics();
            top4.TopicName = "Martial Arts";
            App.DBIni.TopicsDB.AddTopics(top4);
            Topics top5 = new Topics();
            top5.TopicName = "Dance";
            App.DBIni.TopicsDB.AddTopics(top5);
            Topics top6 = new Topics();
            top6.TopicName = "Excercise";
            App.DBIni.TopicsDB.AddTopics(top6);
            //var data = ageDB.GetAgeData();
            #endregion
            var alltopics = App.DBIni.TopicsDB.GetTopicsData();
            var topicsList = new List<Topics>();
            foreach (var topicsinfo in alltopics)
            {
                Topics topics = new Topics();
                topics.TopicName = topicsinfo.TopicName;
                topicsList.Add(topics);
            }
            FollowTopicsList.ItemsSource = topicsList;

            #region UI for iOS
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    todayLbl.Margin = new Thickness(20, 20, 0, 0);
                    helloLbl.Margin = new Thickness(29, 20, 20, 0);
                    topicsLbl.Padding = new Thickness(20, 20, 0, 0);
                    hashtagsLbl.Padding = new Thickness(30, 20, 0, 0);
                    break;

            }
            #endregion
        }

    }
}