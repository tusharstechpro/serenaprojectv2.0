﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Serena.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileMaster : MasterDetailPage
    {
        public ProfileMaster()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);
        }
    }
}