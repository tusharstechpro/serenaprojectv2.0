﻿using Serena.Model;
using SQLite;
using Syncfusion.DataSource.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class Serena_PostDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static readonly object collisionLock = new object(); //amd 2nd feb

        public Serena_PostDB()
        {
            //added discard for sql connections amd 2nd feb

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _ = _SQLiteConnection.CreateTable<Serena_Post>();
            _ = _SQLiteConnection.CreateTable<PostByHashTag>();

        }
        public IEnumerable<Serena_Post> GetSerenaPosts()
        {
              return (from u in _SQLiteConnection.Table<Serena_Post>()
                        select u).ToList();
            
        }
        public IEnumerable<Serena_Post> GetAllSerenaPosts(string sessionEmailId)
        {
            

                return (from usr in _SQLiteConnection.Table<Serena_Post>()
                        select usr).Where(x => x.UserEmailId != sessionEmailId)
                    .OrderBy(x => x.Id).ToList();
            
        }
        public IEnumerable<Serena_Post> GetAllSerenaPostsBySessionId(string sessionEmailId)
        {
            
                return (from usr in _SQLiteConnection.Table<Serena_Post>()
                        select usr).Where(x => x.UserEmailId == sessionEmailId)
                    .OrderBy(x => x.Id).ToList();
            
        }
        public IEnumerable<PostByHashTag> GetAllPostByHashTags()
        {
            
                return (from usr in _SQLiteConnection.Table<PostByHashTag>()
                        select usr)
                    .OrderBy(x => x.Id).ToList();
            
        }
        public Serena_Post GetSpecificPost(string id)
        {
              return (from usr in _SQLiteConnection.Table<Serena_Post>()
                        select usr).Where(x => x.UserEmailId == id)
                   .OrderByDescending(x => x.Id).FirstOrDefault();
            
        }
       
        public Serena_Post GetSpecificPostByGuid(string guid)
        {
            

                return (from usr in _SQLiteConnection.Table<Serena_Post>()
                        select usr).Where(x => x.Guid == guid)
                    .OrderBy(x => x.Id).FirstOrDefault();
            
        }

        public void DeleteUser(string id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Delete<Serena_Post>(id);
            }
        }
        //public string AddPost(Serena_Post post)
        //{
        //        _SQLiteConnection.Insert(post);
        //        return "Sucessfully Posted.";
        //}
        public string AddPost(Serena_Post post)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<Serena_Post>();
                var d1 = data.Where(x => x.Guid == post.Guid).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(post);
                    return "Sucessfully Added";
                }
                else
                {
                    //    post.Guid = d1.Guid;
                    //    //UpdatePost(post);
                        return null;

                }

            }
        }
        public string AddHashTagPost(PostByHashTag post)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<PostByHashTag>();
                var d1 = data.Where(x => x.Guid == post.Guid).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(post);
                    return "Sucessfully Added";
                }
                else
                    return "Sucessfully Updated";
            }
        }
        public void DeletePostByHashTag()
        {
            _SQLiteConnection.DeleteAll<PostByHashTag>();
        }
        public bool UpdatePost(Serena_Post postdata)
        {

            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _SQLiteConnection.Update(postdata);
                return true;
            }
        }
        public bool UpdateFavouritePost(string guid,int favourite)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                var data = _SQLiteConnection.Table<Serena_Post>().Where(x => x.Guid == guid).FirstOrDefault();
                if (data != null)
                {
                    data.Favourite = favourite;
                    _SQLiteConnection.Update(data);
                    return true;
                }
                else
                    return false;

            }
        }
      
        //public bool UpdatePost(string emailid)
        //{
        //    var data = _SQLiteConnection.Table<Serena_Post>();
        //    var d1 = (from values in data
        //              where values.UserEmailId == emailid
        //              select values).Single();
        //    if (true)
        //    {
        //        _SQLiteConnection.Update(d1);
        //        return true;
        //    }

        //}

    }
} 