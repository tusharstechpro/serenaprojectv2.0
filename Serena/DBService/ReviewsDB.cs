﻿using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class ReviewsDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb

        public ReviewsDB()
        {

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<SerenaReviews>();
            _SQLiteConnection.CreateTable<SerenaReviews_Out>();

        }
        public IEnumerable<SerenaReviews> GetReviewsData()
        {


            return (from u in _SQLiteConnection.Table<SerenaReviews>()
                    select u).ToList();

        }
        public IEnumerable<SerenaReviews> GetAllReviewsByGuid(string guid)
        {

            return (from usr in _SQLiteConnection.Table<SerenaReviews>()
                    select usr).Where(x => x.Guid == guid)
                .OrderByDescending(x => x.Id).ToList();

        }
        public void DeleteReviews(int id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Delete<SerenaReviews>(id);
            }
        }
        public void DeleteAllReviews()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.DeleteAll<SerenaReviews>();
            }
        }
        public string AddReview(SerenaReviews reviews)
        {
            //var data = _SQLiteConnection.Table<SerenaReviews>();
            //var d1 = data.Where(x => x.Guid == reviews.Guid).FirstOrDefault();
            //if (d1 == null)
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Insert(reviews);
                return "Sucessfully Added";
            }
            //}
            //else
            //{
            //    reviews.Id = d1.Id;
            //    UpdateReview(reviews);
            //    return "Review Already Exist";
            //}
        }
        public bool UpdateReview(SerenaReviews reviews)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(reviews);
                return true;
            }
        }

        public string AddReview_Out(SerenaReviews_Out reviews)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Insert(reviews);
                return "Sucessfully Added";
            }

        }

        public IEnumerable<SerenaReviews_Out> GetAllReviews_Out(DateTime out_sync_date)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                return (from u in _SQLiteConnection.Table<SerenaReviews_Out>()
                        select u).Where(x => x.LastSyncDate > out_sync_date).ToList();//pending
            }
        }

    }
}
