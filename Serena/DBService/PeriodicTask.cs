﻿using System;
using Matcha.BackgroundService;
using System.Threading.Tasks;
using Xamarin.Forms;
using Serena.Helpers;
using Serena.Model;
using System.Collections.Generic;
using Xamarin.Forms.Maps;
using Serena.Views;
using Xamarin.Essentials;

namespace Serena.DBService
{
    public class PeriodicTask : IPeriodicTask
    {
        public PeriodicTask(int interval)
        {
            Interval = TimeSpan.FromSeconds(interval);

        }

        public TimeSpan Interval { get; set; }


        public Task<bool> StartJob()
        {
            // aws data sync
            //To get current emailId from session

            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    //session email not required for below api call

                    SerenaDataSync serenaDataSync = new SerenaDataSync();
                    Task.Run(() => serenaDataSync.GetTopPeopleExplore()).Wait();

                    Task.Run(() => serenaDataSync.GetSerenaUsersAsync()).Wait();

                    #region class api moved here from explore 12march


                    Task.Run(() => serenaDataSync.GetProclassForAll()).Wait();

                    Task.Run(() => serenaDataSync.GetSerenaExploreTopHasTagCount()).Wait();

                    #endregion

                    //end  session email not required

                    string sessionEmailId = Settings.GeneralSettings;
                    if (!string.IsNullOrEmpty(sessionEmailId))
                    {
                        
                        Task.Run(() => serenaDataSync.GetUsersdataAsync()).Wait();
                        Task.Run(() => serenaDataSync.GetSerenaPostAsync()).Wait();
                        Task.Run(() => serenaDataSync.GetSerenaUpcomingNotifications()).Wait();

                        //12march change
                        Task.Run(() => serenaDataSync.GetProSocial(sessionEmailId).Wait());
                        Task.Run(() => serenaDataSync.GetSerenaNotifications()).Wait();

                    }
                }
                return Task.FromResult(true);
            }
            catch (Exception)
            {
                // CrossLocalNotifications.Current.Show("Test", "background");
                return Task.FromResult(false);
            }

        }



    }
}
