﻿using Serena.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Serena.DBService
{
    public class DBInitialization
    {
        public AgeRangeDB AgeDB = new AgeRangeDB();
        public ExceptionLogDB LogFileDB = new ExceptionLogDB();
        public Serena_NotificationDB NotificationDB = new Serena_NotificationDB();
        public TophashtagDB TophashtagDB = new TophashtagDB();
        public TopicsDB TopicsDB = new TopicsDB();
        public WatchPartyDB WatchPartyDB = new WatchPartyDB();

        public ReviewsDB ReviewsDB = new ReviewsDB();
        public ProfileDB ProfileDB = new ProfileDB();
        public UserProfileDataDB UserProfileDataDB = new UserProfileDataDB();
        public UserProfileDataTranDB UserProfileDataTranDB = new UserProfileDataTranDB();
        public SignUpDB SignUpDB = new SignUpDB();
        public Serena_PostDB SerenaPostDB = new Serena_PostDB();
        public SerenaDataSync serenaDataSync = new SerenaDataSync();
    }
}
