﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace Serena.DBService
{
    public class SerenaDataSync
    {
        public static string FilePath;
        string sessionEmailId;

        public SerenaDataSync()
        {
            sessionEmailId = Settings.GeneralSettings;
        }

        #region Change user status

        public async Task InsertUserProfileTran(string sessionEmailId, string RequestUserEmail, string Guidstr, string status)//0-sendrequest, 1-acceptrequest
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                            {"UserEmailId", sessionEmailId},
                            {"RequestTo", RequestUserEmail},
                            {"Status", status},
                            {"Guid", Guidstr},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://nwy2dc4yjc.execute-api.ap-southeast-2.amazonaws.com/serena/insertuptranrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "InsertUserProfileTran";

            }
        }

        //For Reject User
        public async Task AcceptRejectUserRequest(string Guid, string status)
        {
            try
            {
                var payload = new Dictionary<string, string>
                         {
                            {"Guid", Guid},
                             {"Status", status}
                            };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://q9xs4q70n9.execute-api.ap-southeast-2.amazonaws.com/serena/updateuptranrdsnew";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "InsertUserProfileTran";

            }
        }

        #endregion

        #region Add Login, SignUp, UserProfile data

        public async Task SaveSerenaUserData(SignUpModel_Out signUp)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"EMAIL", signUp.EMAIL},
                        {"Password", signUp.PASSWORD},
                        {"FullName", signUp.FULLNAME},
                        {"UserName", signUp.USERNAME},
                        {"Place", signUp.PLACE},
                        {"UserCategory", signUp.UserCategory},
                        {"UserLoginType", signUp.UserLoginType},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://819hbsml6h.execute-api.ap-southeast-2.amazonaws.com/serena/saveserenauserrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
                //Saving data in Serena_User table of mysql using API
                //Uri u = new Uri("https://cp6mnav4u3.execute-api.us-east-2.amazonaws.com/serena/saveserenauserrds");

                //var payload = new Dictionary<string, string>
                //        {
                //        {"EmailId", EMAIL.Text},
                //        {"Password", encPassword},
                //        {"FullName", FULLNAME.Text},
                //        {"UserName", USERNAME.Text},
                //        {"Place", PLACE.Text},
                //        {"UserCategory", SwitchValue},
                //        {"UserLoginType", LoginTypes},
                //        //{"UserStatus", "0"},
                //        };
                //string strPayload = JsonConvert.SerializeObject(payload);
                //HttpContent c = new StringContent(strPayload, Encoding.UTF8, "application/json");
                //var t = Task.Run(() => SendURI(u, c));
                //Console.WriteLine(t.Result);
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }

        public async Task SaveSerenaUserProfileData(UserProfileData_Out profileData)
        {
            try
            {
                var payload = new Dictionary<string, string>
                            {
                            {"UserEmailId", profileData.UserEmailId},
                            {"UserAccountType", "PUBLIC"},
                            {"UserName", profileData.UserName},
                            };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://mooeqagtlh.execute-api.ap-southeast-2.amazonaws.com/serena/insertuserprofilerds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }

        public async Task UpdateUserProfileData(UserProfileData_Out uProfileData)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", uProfileData.UserEmailId},
                    {"UserProfileImage", uProfileData.UserProfileImage},
                    {"UserAccountType", uProfileData.UserAccountType},
                    {"UserBio", uProfileData.UserBio},
                    {"UserWebsite", uProfileData.UserWebsite},
                    {"UserLocation", uProfileData.UserLocation},
                    {"UserAgeRange", uProfileData.UserAgeRange},
                    {"UserName", uProfileData.UserName}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://grh01wkgr9.execute-api.ap-southeast-2.amazonaws.com/serena/updateuserprofilerds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveSerenaUsers";

            }
        }

        #endregion

        #region Create Post

        public async Task SavePost(Serena_Post post)
        {
            try
            {

                var serializeJson = JsonConvert.SerializeObject(post);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://itao111vv5.execute-api.ap-southeast-2.amazonaws.com/serena/saveserenapostrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync";
                addlogFile.ExceptionEventName = "SaveImagePost";

            }
        }

        #endregion

        #region activity tabs

        public async Task GetSerenaNotifications()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://c0xx4vpe2i.execute-api.ap-southeast-2.amazonaws.com/serena/getnotifications";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Serena_Notifications sn = new Serena_Notifications();
                                dynamic item1 = item.CountORRequestBy;
                                string CountORRequestBy = item1;
                                sn.CountORRequestBy = CountORRequestBy;

                                dynamic item2 = item.Guid;
                                string Guid = item2;
                                sn.Guid = Guid;

                                dynamic item3 = item.ImageUrl;
                                string ImageUrl = item3;
                                sn.ImageUrl = ImageUrl;

                                dynamic item4 = item.PostText;
                                string PostText = item4;
                                sn.PostText = PostText;

                                dynamic item5 = item.PostColor;
                                string PostColor = item5;
                                sn.PostColor = PostColor;

                                dynamic item6 = item.BackgroundColor;
                                string BackgroundColor = item6;
                                sn.BackgroundColor = BackgroundColor;

                                dynamic item7 = item.PostType;
                                string PostType = item7;
                                sn.PostType = PostType;


                                dynamic item8 = item.category;
                                string category = item8;
                                sn.category = category;

                                dynamic item9 = item.UserEmailId;
                                string UserEmailId = item9;
                                sn.UserEmailId = UserEmailId;

                                dynamic item10 = item.LastUpdated;
                                string LastUpdated = item10;
                                sn.LastUpdated = LastUpdated;

                                App.DBIni.NotificationDB.AddNotifications(sn);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Notification.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaNotifications";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaUpcomingNotifications()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://y9rzgdlj37.execute-api.ap-southeast-2.amazonaws.com/serena/getupcommingactivities";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Upcoming_Notifications sn = new Upcoming_Notifications();
                                sn.UserEmailId = sessionEmailId;
                                sn.ClassGuid = item.ClassGuid;
                                sn.MediaUrl = item.MediaUrl;
                                sn.CoverPhotoUrl = item.CoverPhotoUrl;
                                sn.Title = item.Title;
                                sn.About = item.About;
                                sn.Category = item.Category;
                                sn.Tags = item.Tags;
                                sn.Mentions = item.Mentions;
                                sn.Duration = item.Duration;
                                sn.WatchPartyGuid = item.WatchPartyGuid;
                                sn.ScheduledDate = item.ScheduledDate;
                                sn.ScheduledTime = item.ScheduledTime;
                                sn.AMPM = item.AMPM;
                                sn.HostEmailID = item.HostEmailID;
                                sn.Accepted = item.Accepted;
                                sn.AvgRating = item.AvgRating;
                                sn.TotalReviews = item.TotalReviews;
                                sn.Members = item.Members;

                                var notifyData = App.DBIni.NotificationDB.AddUpcomingNotifications(sn);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "Activity.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaUpcomingNotifications";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }



        public async Task AcceptWatchPartyNotification(string accepted, string watchPartyGuid, string members)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Accepted", accepted},
                        {"Guid", watchPartyGuid},
                        {"Members", members}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://z4jvo1rzae.execute-api.ap-southeast-2.amazonaws.com/serena/acceptserenawatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        #endregion

        #region methods from app.xaml

        public async Task GetUsersdataAsync()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://2uhfsh20u2.execute-api.ap-southeast-2.amazonaws.com/serena/getuserslistprofilepage";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                UserProfileData userProfileData = new UserProfileData();

                                userProfileData.UserEmailId = item.UserEmailId;
                                userProfileData.UserPostJson = item.UserPostJson;
                                userProfileData.UserProfileImage = item.UserProfileImage;
                                userProfileData.UserAccountType = item.UserAccountType;
                                userProfileData.UserBio = item.UserBio;
                                userProfileData.UserWebsite = item.UserWebsite;
                                userProfileData.UserLocation = item.UserLocation;
                                userProfileData.UserAgeRange = item.UserAgeRange;
                                userProfileData.UserName = item.UserName;

                                userProfileData.ContactPhone = item.ContactPhone;
                                userProfileData.ContactEmail = item.ContactEmail;
                                userProfileData.AllowContact = item.AllowContact;
                                userProfileData.IsPro = item.IsPro;
                                var retrunvalue = App.DBIni.UserProfileDataDB.AddUser(userProfileData);

                                UserProfileData_Tran userProileDataTran = new UserProfileData_Tran();

                                userProileDataTran.UserEmailId = item.UserEmailId;
                                userProileDataTran.Status = item.TrequestStatus;
                                userProileDataTran.Guid = item.Guid;
                                var retrunvalue1 = App.DBIni.UserProfileDataTranDB.AddUser(userProileDataTran);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetUsersdataAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaUsersAsync()
        {
            try
            {
                var url = "https://n1f29fg8qk.execute-api.ap-southeast-2.amazonaws.com/serena/getallserenauserrds";
                var client = new HttpClient();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                SignUpModel sm = new SignUpModel();

                                sm.FULLNAME = item.FullName;
                                sm.EMAIL = item.EMAIL;
                                sm.LastCreated = item.LastCreated;
                                sm.LastLoginTime = item.LastLoginTime;
                                sm.LastUpdated = item.LastUpdated;
                                sm.PASSWORD = item.Password;
                                sm.PLACE = item.Place;
                                sm.UserCategory = item.UserCategory;
                                sm.UserLoginType = item.UserLoginType;
                                sm.USERNAME = item.UserName;
                                App.DBIni.SignUpDB.AddUser(sm);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaUsersAsync";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                //   logFile.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaPostAsync()
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://s8kjad0jw5.execute-api.ap-southeast-2.amazonaws.com/serena/getallserenapostrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Serena_Post sm = new Serena_Post();
                                dynamic item14 = item.Opacity;
                                string Opacity = item14;

                                dynamic item16 = item.Longitude;
                                string Longitude = item16;

                                dynamic item17 = item.Latitude;
                                string Latitude = item17;

                                dynamic item20 = item.Favourite;
                                string Favourite = item20;

                                sm.UserEmailId = item.UserEmailId;
                                sm.Description = item.Description;
                                sm.LastCreated = item.LastCreated;
                                sm.ShareType = item.ShareType;
                                sm.LastUpdated = item.LastUpdated;
                                sm.Location = item.Location;
                                sm.Topics = item.Topics;
                                sm.ImageUrl = item.ImageUrl;
                                sm.Tags = item.Tags;
                                sm.HashTags = item.HashTags;
                                sm.LastCreated = item.LastCreated;
                                sm.LastUpdated = item.LastUpdated;
                                sm.PostText = item.PostText;
                                sm.PostColor = item.PostColor;
                                sm.PostBackgroundColor = item.BackgroundColor;
                                sm.Opacity = Convert.ToDouble(Opacity);
                                sm.PlaceAddress = item.PlaceAddress;
                                sm.Longitude = Convert.ToDouble(Longitude);
                                sm.Latitude = Convert.ToDouble(Latitude);
                                sm.PostType = item.PostType;
                                sm.Guid = item.Guid;
                                sm.Favourite = Convert.ToInt32(Favourite);
                                sm.FavCount = item.FavCount;

                                App.DBIni.SerenaPostDB.AddPost(sm);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaPostAsync";
                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
                //logFile.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaAgeRangeAsync()
        {
            try
            {
                var dataage1 = App.DBIni.AgeDB.GetAgeData();
                foreach (var item in dataage1)
                {
                    App.DBIni.AgeDB.DeleteAge(item.Id);
                }
                //var dataage = ageDB.GetAgeData();

                var url = "https://5e9nxuy1x7.execute-api.ap-southeast-2.amazonaws.com/serena/getallagerangerds";
                var client = new HttpClient();
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                AgeRange sm = new AgeRange();
                                dynamic item1 = item.AgeRange;
                                string AgeRangeName = item1;


                                sm.AgeRangeName = AgeRangeName;
                                App.DBIni.AgeDB.AddAge(sm);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaAgeRangeAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        public async Task GetSerenaTopicsAsync()
        {
            try
            {
                var url = "https://hdz0eb79z0.execute-api.ap-southeast-2.amazonaws.com/serena/get-all-topics";
                var client = new HttpClient();
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                Topics sm = new Topics();
                                dynamic item1 = item.Id;
                                string topicId = item1;

                                dynamic item2 = item.TopicName;
                                string TopicName = item2;

                                sm.TopicName = TopicName;
                                sm.Id = Convert.ToInt32(topicId);

                                App.DBIni.TopicsDB.AddTopics(sm);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaAgeRangeAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        #endregion

        #region explore api

        public async Task GetSerenaExploreTopHasTagCount()
        {
            try
            {
                #region deleting previous trending each time

                App.DBIni.TophashtagDB.DeleteTophashtag();//delete all records from Tophashtags
                App.DBIni.TophashtagDB.DeleteSearchTophashtag();//delete all records from SearchHashTag

                #endregion

                var url = "https://fkk3rybfna.execute-api.ap-southeast-2.amazonaws.com/serena/gettophashtagscnt";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {
                                    var tophashTags = new Tophashtags();

                                    tophashTags.Hashtags = item.Hashtags;
                                    tophashTags.Count = item.Count;
                                    tophashTags.Img1 = item.Img1;
                                    tophashTags.Img2 = item.Img2;
                                    tophashTags.Img3 = item.Img3;

                                    App.DBIni.TophashtagDB.AddTophashtag(tophashTags);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        public async Task GetSerenaExploreSearchHashTag(string currentLocation)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Location", currentLocation}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://71mbmq3ebh.execute-api.ap-southeast-2.amazonaws.com/serena/searchhashtagsrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                SearchHashTag tophashTags = new SearchHashTag();
                                dynamic item1 = item.Hashtags;
                                string Hashtags = item1;

                                dynamic item2 = item.Count;
                                string Count = item2;

                                dynamic item3 = item.HashType;
                                string HashType = item3;

                                dynamic item4 = item.Location;
                                string Location = item4;

                                tophashTags.Hashtags = Hashtags;
                                tophashTags.Count = Count;
                                tophashTags.HashType = HashType;
                                tophashTags.Location = Location;

                                App.DBIni.TophashtagDB.AddSearchHashTags(tophashTags);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreSearchHashTag";

            }
        }

        public async Task GetSerenaExplorePostsByHashTag(string hashTag)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", sessionEmailId},
                        {"Hashtags", hashTag}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://7dg41f3e6g.execute-api.ap-southeast-2.amazonaws.com/serena/getpostsbyhashtagsrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                PostByHashTag sm = new PostByHashTag();
                                dynamic item1 = item.UserEmailId;
                                string UserEmailId = item1;

                                dynamic item2 = item.Description;
                                string Description = item2;

                                dynamic item3 = item.ShareType;
                                string ShareType = item3;

                                dynamic item4 = item.Location;
                                string Location = item4;

                                dynamic item5 = item.Topics;
                                string Topics = item5;

                                dynamic item6 = item.ImageUrl;
                                string ImageUrl = item6;

                                dynamic item7 = item.Tags;
                                string Tags = item7;

                                dynamic item8 = item.HashTags;
                                string HashTags = item8;

                                dynamic item9 = item.LastCreated;
                                string LastCreated = item9;

                                dynamic item10 = item.LastUpdated;
                                string LastUpdated = item10;

                                dynamic item11 = item.PostText;
                                string PostText = item11;

                                dynamic item12 = item.PostColor;
                                string PostColor = item12;

                                dynamic item13 = item.BackgroundColor;
                                string BackgroundColor = item13;

                                dynamic item14 = item.Opacity;
                                string Opacity = item14;

                                dynamic item15 = item.PlaceAddress;
                                string PlaceAddress = item15;

                                dynamic item16 = item.Longitude;
                                string Longitude = item16;

                                dynamic item17 = item.Latitude;
                                string Latitude = item17;

                                dynamic item18 = item.PostType;
                                string PostType = item18;

                                dynamic item19 = item.Guid;
                                string Guidstr = item19;

                                dynamic item20 = item.Favourite;
                                string Favourite = item20;

                                dynamic item21 = item.FavCount;
                                string FavCount = item21;

                                sm.UserEmailId = UserEmailId;
                                sm.Description = Description;
                                sm.LastCreated = LastCreated;
                                sm.ShareType = ShareType;
                                sm.LastUpdated = LastUpdated;
                                sm.Location = Location;
                                sm.Topics = Topics;
                                sm.ImageUrl = ImageUrl;
                                sm.Tags = Tags;
                                sm.HashTags = HashTags;
                                sm.LastCreated = LastCreated;
                                sm.LastUpdated = LastUpdated;
                                sm.PostText = PostText;
                                sm.PostColor = PostColor;
                                sm.PostBackgroundColor = BackgroundColor;
                                sm.Opacity = Convert.ToDouble(Opacity);
                                sm.PlaceAddress = PlaceAddress;
                                sm.Longitude = Convert.ToDouble(Longitude);
                                sm.Latitude = Convert.ToDouble(Latitude);
                                sm.PostType = PostType;
                                sm.Guid = Guidstr;
                                sm.Favourite = Convert.ToInt32(Favourite);
                                sm.FavCount = FavCount;

                                Serena_PostDB sbdb = new Serena_PostDB();
                                sbdb.AddHashTagPost(sm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExplorePostsByHashTag";

            }
        }

        #endregion

        #region reviews and top people api
        public async Task PostSerenaReviewsByGuid_Class(SerenaReviews_Out serenaReviews)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", serenaReviews.Guid},
                        {"UserEmailId", serenaReviews.UserEmailId},
                        {"Review", serenaReviews.Review},
                        {"Rating", Convert.ToString(serenaReviews.Rating)}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://e8u6fgnoci.execute-api.ap-southeast-2.amazonaws.com/serena/savepostreviewrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task PostSerenaReviewsByGuid(SerenaReviews serenaReviews)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", serenaReviews.Guid},
                        {"UserEmailId", serenaReviews.UserEmailId},
                        {"Review", serenaReviews.Review},
                        {"Rating", Convert.ToString(serenaReviews.Rating)}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://e8u6fgnoci.execute-api.ap-southeast-2.amazonaws.com/serena/savepostreviewrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task GetTopPeopleExplore()
        {
            try
            {
                //deleteing each record everytime
                // App.DBIni.TophashtagDB.DeleteAllTopPeople();
                var url = "https://0fk4gf2lpf.execute-api.ap-southeast-2.amazonaws.com/serena/gettoppeople";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {
                                    var topPeople = new People();


                                    dynamic item4 = item.AvgRating;
                                    double rating = 0;
                                    dynamic item11 = item.AvgRating;
                                    if (item11 == "" || item11 == null)
                                        rating = 0;
                                    else
                                        rating = Math.Round(Convert.ToDouble(item11), 1);


                                    topPeople.UserEmailId = item.UserEmailId;
                                    topPeople.UserName = item.UserName;
                                    topPeople.UserProfileImage = item.UserProfileImage;
                                    topPeople.AvgRating = rating;
                                    topPeople.TotalReviews = item.TotalReviews;
                                    topPeople.Topics = item.Topics;

                                    App.DBIni.TophashtagDB.AddTopPeople(topPeople);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        public async Task GetSerenaReviews(string revGuid)
        {
            try
            {
                App.DBIni.ReviewsDB.DeleteAllReviews();

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", revGuid}
                        };

                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://b42vumkv1k.execute-api.ap-southeast-2.amazonaws.com/serena/getpostreviewsrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                var sm = new SerenaReviews();

                                double avgrating = 0;
                                dynamic Rating = item.Rating;
                                if (Rating == "" || Rating == null)
                                    avgrating = 0;
                                else
                                    avgrating = Math.Round(Convert.ToDouble(Rating), 1);

                                DateTime DateAdded = item.DateAdded;

                                sm.Guid = revGuid;
                                sm.UserEmailId = item.UserEmailId;
                                sm.UserName = item.FullName;
                                sm.Review = item.Review;
                                sm.Rating = avgrating;
                                sm.DateAdded = DateAdded.ToString("MM/dd/yyyy");

                                App.DBIni.ReviewsDB.AddReview(sm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExplorePostsByHashTag";
            }
        }

        #endregion

        #region fav functionality 

        public async Task PostSerenaFavourite(Serena_Post serenaReviews)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", serenaReviews.UserEmailId},
                        {"Guid", serenaReviews.Guid},
                        {"Favourite", Convert.ToString(serenaReviews.Favourite)}

                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://0muvc0hjqd.execute-api.ap-southeast-2.amazonaws.com/serena/favpostsrds";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        #endregion

        #region social profile 

        public async Task PostSocialProfileData(UserProfileData userProfileData, string FullName)
        {
            try
            {
                var userProfilePayload = new Dictionary<string, string>
                            {
                            {"UserEmailId", userProfileData.UserEmailId},
                            {"FullName",  FullName},
                            {"UserName", userProfileData.UserName},
                            {"UserWebsite", userProfileData.UserWebsite},
                            {"UserLocation", userProfileData.UserLocation},
                            {"AllowContact", userProfileData.AllowContact},
                            {"ContactEmail", userProfileData.ContactEmail},
                            {"ContactPhone", userProfileData.ContactPhone}
                            };
                var serializeJson = JsonConvert.SerializeObject(userProfilePayload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://532h1zkkb6.execute-api.ap-southeast-2.amazonaws.com/serena/updateuserprofilepro";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task PostUpdatedPassword(string EmailId, string Password)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Password", Password},
                        {"EMAIL", EmailId},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://0xehe1f0ve.execute-api.ap-southeast-2.amazonaws.com/serena/updatepassword";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task SaveSocialPostMemberTools(ProMemberToolsModel proMemberToolsModel)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"UserEmailId", proMemberToolsModel.UserEmailId},
                        {"Categories", proMemberToolsModel.Categories},
                        {"Tagline", proMemberToolsModel.Tagline},
                        {"About", proMemberToolsModel.About},
                        {"Coverphoto", proMemberToolsModel.Coverphoto},

                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://o1e80whgkk.execute-api.ap-southeast-2.amazonaws.com/serena/saveprosocial";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "PostSerenaReviewsByGuid";

            }
        }

        public async Task GetProSocial(string userEmailId)
        {
            try
            {
                //deleteing all social profile from session user
                //App.DBIni.UserProfileDataDB.DeleteAllSocial();

                var payload = new Dictionary<string, string>
                        {
                         {"UserEmailId",   userEmailId},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://4apm2vxav3.execute-api.ap-southeast-2.amazonaws.com/serena/getprosocial";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                var proMember = new ProMemberToolsModel();

                                proMember.UserEmailId = item.UserEmailId;
                                proMember.Categories = item.Categories;
                                proMember.Tagline = item.Tagline;
                                proMember.About = item.About;
                                proMember.Coverphoto = item.Coverphoto;

                                App.DBIni.UserProfileDataDB.AddPromemberTool_Social(proMember);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        #endregion

        #region social class -media section

        public async Task SaveProClass(UploadMediaModel_Out uploadMedia)
        {
            try
            {

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", uploadMedia.Guid},
                        {"UserEmailId", uploadMedia.UserEmailId},
                        {"MediaUrl", uploadMedia.MediaUrl},
                        {"CoverPhotoUrl", uploadMedia.CoverPhotoUrl},
                        {"Title", uploadMedia.Title},
                        {"About", uploadMedia.About},
                        {"PlayList", uploadMedia.PlayList},
                        {"Category", uploadMedia.Category},
                        {"Tags", uploadMedia.Tags},
                        {"Mentions", uploadMedia.Mentions},
                        {"Duration", uploadMedia.Duration},

                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://w70k0qq0lh.execute-api.ap-southeast-2.amazonaws.com/serena/saveproclass";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveProClass";

            }
        }

        public async Task GetProClass(string userEmailId)
        {
            try
            {
                //deleteing all classes from session user
                App.DBIni.UserProfileDataDB.DeleteAllClass();

                //readding the data
                var payload = new Dictionary<string, string>
                        {
                         {"UserEmailId",   userEmailId},
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://rl36np5gdc.execute-api.ap-southeast-2.amazonaws.com/serena/getproclass";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {

                                UploadMediaModel proMember = new UploadMediaModel();

                                dynamic item0 = item.Guid;
                                string Guid = item0;

                                dynamic item1 = item.UserEmailId;
                                string UserEmailId = item1;

                                dynamic item2 = item.MediaUrl;
                                string MediaUrl = item2;

                                dynamic item3 = item.CoverPhotoUrl;
                                string CoverPhotoUrl = item3;

                                dynamic item4 = item.Title;
                                string Title = item4;

                                dynamic item5 = item.About;
                                string About = item5;

                                dynamic item6 = item.PlayList;
                                string PlayList = item6;

                                dynamic item7 = item.Category;
                                string Category = item7;

                                dynamic item8 = item.Tags;
                                string Tags = item7;

                                dynamic item9 = item.Mentions;
                                string Mentions = item9;

                                dynamic item10 = item.Duration;
                                string Duration = item10;

                                proMember.Guid = Guid;
                                proMember.UserEmailId = UserEmailId;
                                proMember.MediaUrl = MediaUrl;
                                proMember.CoverPhotoUrl = CoverPhotoUrl;
                                proMember.Title = Title;
                                proMember.About = About;
                                proMember.PlayList = PlayList;
                                proMember.Category = Category;
                                proMember.Tags = Tags;
                                proMember.Mentions = Mentions;//level of difficulties
                                proMember.Duration = Duration;

                                App.DBIni.UserProfileDataDB.AddProClass(proMember);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSerenaExploreTopHasTagCount";

            }
        }

        public async Task GetProclassForAll()
        {
            try
            {
                //deleteing all classes from session user
                //App.DBIni.UserProfileDataDB.DeleteFindAllClass();

                //reading the data

                var url = "https://kz8s1kcc7j.execute-api.ap-southeast-2.amazonaws.com/serena/getproclassforall";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                        foreach (var getBody in deserializeJson)
                        {
                            if (getBody.Key == "body")
                            {
                                string getValue = getBody.Value.ToString();
                                string replaceData1 = getValue.Replace("\n", "");

                                dynamic PL = JArray.Parse(replaceData1);

                                foreach (var item in PL)
                                {

                                    var proMember = new FindUploadClassModel();

                                    double rating = 0;
                                    dynamic item11 = item.AvgRating;
                                    if (item11 == "" || item11 == null)
                                        rating = 0;
                                    else
                                        rating = Math.Round(Convert.ToDouble(item11), 1);


                                    proMember.Guid = item.Guid;
                                    proMember.UserEmailId = item.UserEmailId;
                                    proMember.MediaUrl = item.MediaUrl;
                                    proMember.CoverPhotoUrl = item.CoverPhotoUrl;
                                    proMember.Title = item.Title;
                                    proMember.About = item.About;
                                    proMember.PlayList = "5";
                                    proMember.Category = item.Category;
                                    proMember.Tags = item.Tags;
                                    proMember.Mentions = item.Mentions == "" ? "EASY" : item.Mentions;
                                    proMember.Duration = item.Duration;
                                    proMember.AvgRating = rating;
                                    proMember.TotalReviews = item.TotalReviews;

                                    App.DBIni.UserProfileDataDB.AddFindClass(proMember);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetProclassForAll";

            }
        }

        public async Task GetSearchClassExplore()
        {
            try
            {
                //deleteing all searched classes from session user
                App.DBIni.UserProfileDataDB.DeleteSearchAllClass();

                var url = "https://k4cue7nyg5.execute-api.ap-southeast-2.amazonaws.com/serena/searchclass-explore";
                var client = new HttpClient();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                SearchClassModel topSearch = new SearchClassModel();

                                //                        public string AvgRating { get; set; }
                                //public string TotalReviews { get; set; }
                                //public string guid { get; set; }
                                //public string Title { get; set; }
                                //public string CoverPhotoUrl { get; set; }
                                //public string Mentions { get; set; }

                                dynamic item0 = !string.IsNullOrEmpty(item.AvgRating) ? "0" : item.AvgRating;
                                string AvgRating = item0;

                                dynamic item1 = item.TotalReviews;
                                string TotalReviews = item1;

                                dynamic item2 = item.Title;
                                string Title = item2;

                                dynamic item3 = item.guid;
                                string Guid = item3;

                                dynamic item4 = item.CoverPhotoUrl;
                                string CoverPhotoUrl = item4;

                                dynamic item5 = item.Mentions;
                                string Mentions = item5;

                                topSearch.AvgRating = AvgRating;
                                topSearch.TotalReviews = TotalReviews;
                                topSearch.Title = Title;
                                topSearch.Guid = Guid;
                                topSearch.CoverPhotoUrl = CoverPhotoUrl;
                                topSearch.Mentions = Mentions;

                                App.DBIni.UserProfileDataDB.AddSearchClass(topSearch);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "GetSearchClassExplore";

            }
        }

        #endregion

        #region schedule Watch Party

        public async Task SaveWatchParty(WatchParty_Out watchParty)
        {
            try
            {

                var serializeJson = JsonConvert.SerializeObject(watchParty);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://019fabi9p9.execute-api.ap-southeast-2.amazonaws.com/serena/inserwatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        public async Task SaveWatchPartyMembers(WatchPartyTran_Out watchPartyTrn)
        {
            try
            {

                var payload = new Dictionary<string, string>
                        {
                        {"Guid", watchPartyTrn.WatchPartyGuid},
                        {"Members", watchPartyTrn.Members}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://id2hunla57.execute-api.ap-southeast-2.amazonaws.com/serena/insertwatchpartymembers";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "App.Xaml";
                addlogFile.ExceptionEventName = "SaveWatchParty";

            }
        }

        public async Task DeleteWatchParty(string WatchPartyGuid)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", WatchPartyGuid}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://opr6l6yvp9.execute-api.ap-southeast-2.amazonaws.com/serena/deletewatchparty";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "DeleteWatchParty";

            }
        }

        public async Task UpdateWatchParty(DateTime SHDate, string SHTime, string WPGuid)
        {
            try
            {
                var payload = new Dictionary<string, object>
                        {
                        {"Guid", WPGuid},
                      {"ScheduleDate", SHDate},
                      {"ScheduleTime", SHTime}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");


                var url = "https://427yvjzy46.execute-api.ap-southeast-2.amazonaws.com/serena/updatewatchpartydates";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "UpdateWatchParty";

            }
        }

        public async Task GetWatchPartyDetails(string WPGuid)
        {
            try
            {
                var payload = new Dictionary<string, string>
                        {
                        {"Guid", WPGuid}
                        };
                var serializeJson = JsonConvert.SerializeObject(payload);
                var content = new StringContent(serializeJson, Encoding.UTF8, "application/json");

                var url = "https://ub2pwaj7uh.execute-api.ap-southeast-2.amazonaws.com/serena/getwatchpartydetails";
                var client = new HttpClient();

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    var deserializeJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                    foreach (var getBody in deserializeJson)
                    {
                        if (getBody.Key == "body")
                        {
                            string getValue = getBody.Value.ToString();
                            string replaceData1 = getValue.Replace("\n", "");

                            dynamic PL = JArray.Parse(replaceData1);

                            foreach (var item in PL)
                            {
                                WatchPartyTran WPT = new WatchPartyTran();
                                WPT.WatchPartyGuid = item.Guid;
                                WPT.Members = item.Members;

                                var notifyData = App.DBIni.WatchPartyDB.AddWatchPartyTran(WPT);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SerenaDataSync.cs";
                addlogFile.ExceptionEventName = "GetWatchPartyDetails";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }


        #endregion

    }
}
