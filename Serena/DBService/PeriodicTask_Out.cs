﻿using Matcha.BackgroundService;
using Plugin.Toast;
using Serena.Data;
using Serena.Helpers;
using Serena.Model;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Serena.DBService
{
    public class PeriodicTask_Out : IPeriodicTask
    {
        public PeriodicTask_Out(int interval)
        {
            Interval = TimeSpan.FromSeconds(interval);

        }
        public TimeSpan Interval { get; set; }



        public Task<bool> StartJob()
        {

            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    SerenaDataSync serenaDataSync = new SerenaDataSync();
                    string sessionEmailId = Settings.GeneralSettings;
                    if (!string.IsNullOrEmpty(sessionEmailId))
                    {
                        #region registration - signup
                        var test = App.DBIni.UserProfileDataTranDB.GetData();
                        var signup_Out = App.DBIni.AgeDB.GetSyncDataByTableName("SignUpModel_Out");
                        var signup_OutData = App.DBIni.SignUpDB.GetAllUser_Out(signup_Out.Out_last_sync, "0");//save
                        if (signup_OutData.Count() != 0)
                        {
                            foreach (var regsignup_OutData in signup_OutData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.SaveSerenaUserData(regsignup_OutData)).Wait();
                                }
                            }
                        }
                        #endregion

                        #region Change User Password

                        var signup1_Out = App.DBIni.AgeDB.GetSyncDataByTableName("SignUpModel_Out");
                        //var test = App.DBIni.UserProfileDataTranDB.GetData();
                        var signup1_OutData = App.DBIni.SignUpDB.GetAllUser_Out(signup1_Out.Out_last_sync, "1");//Change pwd
                        if (signup1_OutData.Count() != 0)
                        {
                            foreach (var regsignup1_OutData in signup1_OutData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.PostUpdatedPassword(regsignup1_OutData.EMAIL, regsignup1_OutData.PASSWORD)).Wait();
                                }
                            }
                        }
                        if (signup_OutData.Count() != 0 || signup1_OutData.Count() != 0)
                        {
                            //Update Last_Out_Time in LastSync Table  for UserProfileData_Tran
                            var lstsignup = new LastSync();
                            lstsignup.TableName = "SignUpModel_Out";
                            lstsignup.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(lstsignup);
                        }
                        #endregion

                        #region save  userprofile data

                        var userprofile_Out = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileData_Out");

                        var userprofile_OutData = App.DBIni.UserProfileDataDB.GetAllUPData_Out(userprofile_Out.Out_last_sync, "0");
                        if (userprofile_OutData.Count() != 0)
                        {
                            foreach (var up_OutData in userprofile_OutData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.SaveSerenaUserProfileData(up_OutData)).Wait();
                                }
                            }
                        }
                        #endregion

                        #region update UserProfileData
                        //To get Last_Out_Time value for WatchParty table
                        var syncUPData = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileData_Out");
                        //To get all watchparty data where Schedule datetime value is greater than Last_Out_Time
                        var UPLatestdata = App.DBIni.UserProfileDataDB.GetAllUPData_Out(syncUPData.Out_last_sync, "1");
                        if (UPLatestdata.Count() != 0)
                        {
                            //Insert watchparty one at a time in MySQL
                            foreach (var UPData in UPLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.UpdateUserProfileData(UPData)).Wait();
                                }
                            }
                        }
                        if (userprofile_OutData.Count() != 0 || UPLatestdata.Count() != 0)
                        {
                            //Update Last_Out_Time in LastSync UserProfileData_Out Table 
                            var ls = new LastSync();
                            ls.TableName = "UserProfileData_Out";
                            ls.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(ls);
                        }
                        #endregion

                        #region WatchParty save API call
                        //To get Last_Out_Time value for WatchParty table
                        var syncData = App.DBIni.AgeDB.GetSyncDataByTableName("WatchParty_Out");
                        //To get all watchparty data where Schedule datetime value is greater than Last_Out_Time
                        var WPLatestdata = App.DBIni.WatchPartyDB.GetAllWatchPartyOut(syncData.Out_last_sync, "0");
                        if (WPLatestdata.Count() != 0)
                        {
                            //Insert watchparty one at a time in MySQL
                            foreach (var WPData in WPLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.SaveWatchParty(WPData)).Wait();
                                }
                            }
                        }
                        #endregion//

                        #region WatchpartyTran save API call
                        //To get Last_Out_Time value for WatchPartyTran table
                        var syncTranData = App.DBIni.AgeDB.GetSyncDataByTableName("WatchPartyTran_Out");
                        //To get all WatchPartyTran data where Schedule datetime value is greater than Last_Out_Time
                        var WPTLatestdata = App.DBIni.WatchPartyDB.GetAllWatchPartyTranOutByStatus(syncTranData.Out_last_sync, "0");
                        if (WPTLatestdata.Count() != 0)
                        {
                            //Insert WatchPartyTran one at a time in MySQL
                            foreach (var WPTData in WPTLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.SaveWatchPartyMembers(WPTData)).Wait();
                                }
                            }
                        }
                        #endregion//

                        #region Edit Watch party API
                        //To get Last_Out_Time value for WatchParty table
                        var syncData1 = App.DBIni.AgeDB.GetSyncDataByTableName("WatchParty_Out");
                        //To get all watchparty data where Schedule datetime value is greater than Last_Out_Time
                        var WPLatestdata1 = App.DBIni.WatchPartyDB.GetAllWatchPartyOut(syncData1.Out_last_sync, "2");
                        if (WPLatestdata1.Count() != 0)
                        {
                            //Insert watchparty one at a time in MySQL
                            foreach (var WPData1 in WPLatestdata1)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.UpdateWatchParty(WPData1.ScheduleDate, WPData1.ScheduleTime, WPData1.Guid)).Wait();
                                }
                            }
                        }
                        #endregion//

                        #region Delete Watch Party API
                        //To get Last_Out_Time value for WatchParty table
                        var syncData2 = App.DBIni.AgeDB.GetSyncDataByTableName("WatchParty_Out");
                        //To get all watchparty data where Schedule datetime value is greater than Last_Out_Time
                        var WPLatestdata2 = App.DBIni.WatchPartyDB.GetAllWatchPartyOut(syncData2.Out_last_sync, "1");
                        if (WPLatestdata2.Count() != 0)
                        {
                            //Insert watchparty one at a time in MySQL
                            foreach (var WPData2 in WPLatestdata2)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.DeleteWatchParty(WPData2.Guid)).Wait();
                                }
                            }
                        }

                        if (WPLatestdata.Count() != 0 || WPLatestdata1.Count() != 0 || WPLatestdata2.Count() != 0)
                        {
                            //Update Last_Out_Time in LastSync WatchParty_Out Table 
                            var ls = new LastSync();
                            ls.TableName = "WatchParty_Out";
                            ls.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(ls);
                            //var test = App.DBIni.AgeDB.GetLastSyncData();
                        }
                        #endregion//

                        #region Accept WatchParty invitation API call
                        //To get Last_Out_Time value for WatchPartyTran table
                        var syncWTranData = App.DBIni.AgeDB.GetSyncDataByTableName("WatchPartyTran_Out");
                        //To get all WatchPartyTran data by accepted status where Schedule datetime value is greater than Last_Out_Time
                        var WPTranLatestdata = App.DBIni.WatchPartyDB.GetAllWatchPartyTranOutByStatus(syncWTranData.Out_last_sync, "1");
                        //var tstdata = App.DBIni.WatchPartyDB.GetWatchpartytran_Out();
                        if (WPTranLatestdata.Count() != 0)
                        {
                            foreach (var WPTranData in WPTranLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.AcceptWatchPartyNotification("1", WPTranData.WatchPartyGuid, sessionEmailId)).Wait();
                                }
                            }
                        }
                        #endregion

                        #region Reject WatchParty invitation API call
                        //To get Last_Out_Time value for WatchPartyTran table
                        var syncWTranData1 = App.DBIni.AgeDB.GetSyncDataByTableName("WatchPartyTran_Out");
                        //To get all WatchPartyTran data by accepted status where Schedule datetime value is greater than Last_Out_Time
                        var WPTranLatestdata1 = App.DBIni.WatchPartyDB.GetAllWatchPartyTranOutByStatus(syncWTranData1.Out_last_sync, "2");
                        //var tstdata1 = App.DBIni.WatchPartyDB.GetWatchpartytran_Out();
                        if (WPTranLatestdata1.Count() != 0)
                        {
                            foreach (var WPTranData1 in WPTranLatestdata1)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.AcceptWatchPartyNotification("2", WPTranData1.WatchPartyGuid, sessionEmailId)).Wait();
                                }
                            }


                        }
                        if (WPTranLatestdata1.Count() != 0 || WPTranLatestdata.Count() != 0 || WPTLatestdata.Count() != 0)
                        {
                            //Update Last_Out_Time in LastSync WatchPartyTran_Out Table 
                            var ls = new LastSync();
                            ls.TableName = "WatchPartyTran_Out";
                            ls.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(ls);
                            //var test = App.DBIni.AgeDB.GetLastSyncData();
                        }
                        #endregion

                        #region Follow user insert API call
                        //To get LastSync_Time value for UserProfileData_Tran table
                        var syncUserProfileData_TranData = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileDataTran_Out");
                        //var test = App.DBIni.UserProfileDataTranDB.GetUserProfileData_Tran();
                        //To get all WatchPartyTran data where Schedule datetime value is greater than Last_Out_Time
                        int statusPending = 0;
                        var UPTTLatestdata = App.DBIni.UserProfileDataTranDB.GetAllUserProfileData_TranOut(syncUserProfileData_TranData.Out_last_sync, statusPending);
                        if (UPTTLatestdata.Count() != 0)
                        {
                            //Insert WatchPartyTran one at a time in MySQL
                            foreach (var UPTData in UPTTLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    //call from Profile.cs--> Event FollowBtnClicked
                                    Task.Run(() => serenaDataSync.InsertUserProfileTran(sessionEmailId, UPTData.RequestTo, UPTData.Guid, "0")).Wait();
                                }
                            }
                        }
                        #endregion

                        #region UNFollow user  API call

                        //To get LastSync_Time value for UserProfileData_Tran table
                        var syncUPD_Unfollow_TranData = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileDataTran_Out");
                        //To get all WatchPartyTran data where Schedule datetime value is greater than Last_Out_Time
                        int statusUnfollow = 3;

                        var UPTT_UnfollowLatestdata = App.DBIni.UserProfileDataTranDB.GetAllUserProfileData_TranOut(syncUserProfileData_TranData.Out_last_sync, statusUnfollow);
                        if (UPTT_UnfollowLatestdata.Count() != 0)
                        {
                            //Update unfollow user status one at a time in MySQL
                            foreach (var UPT_UnfollowData in UPTT_UnfollowLatestdata)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    //call from Profile.cs--> Event FollowingBtnClicked
                                    //Task.Run(() => serenaDataSync.UnfollowUser(sessionEmailId, UPT_UnfollowData.RequestTo, "3")).Wait();
                                    Task.Run(() => serenaDataSync.AcceptRejectUserRequest(UPT_UnfollowData.Guid, "3")).Wait();
                                }
                            }
                        }
                        #endregion

                        #region Reject user API call

                        //To get LastSync_Time value for UserProfileData_Tran table
                        var RejectUPDData = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileDataTran_Out");
                        //To get all WatchPartyTran data where Schedule datetime value is greater than Last_Out_Time
                        int statusReject = 2;
                        var UPTT_RejectUPDData = App.DBIni.UserProfileDataTranDB.GetAllUserProfileData_TranOut(syncUserProfileData_TranData.Out_last_sync, statusReject);
                        if (UPTT_RejectUPDData.Count() != 0)
                        {
                            //Update unfollow user status one at a time in MySQL
                            foreach (var upt_RejectUPDData in UPTT_RejectUPDData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    //call from Profile.cs--> Event RejectbtnClicked
                                    Task.Run(() => serenaDataSync.AcceptRejectUserRequest(upt_RejectUPDData.Guid, "2")).Wait();
                                }
                            }

                        }
                        #endregion

                        #region Accept user API call

                        //To get LastSync_Time value for UserProfileData_Tran table
                        var AcceptedUPDData = App.DBIni.AgeDB.GetSyncDataByTableName("UserProfileDataTran_Out");
                        //To get all WatchPartyTran data where Schedule datetime value is greater than Last_Out_Time
                        int statusAccept = 1;
                        var UPTT_AcceptedUPDData = App.DBIni.UserProfileDataTranDB.GetAllUserProfileData_TranOut(syncUserProfileData_TranData.Out_last_sync, statusAccept);
                        if (UPTT_AcceptedUPDData.Count() != 0)
                        {
                            //Update accept request for user status one at a time in MySQL
                            foreach (var upt_AcceptedUPDData in UPTT_AcceptedUPDData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    //call from Profile.cs--> Event AcceptbtnClicked
                                    Task.Run(() => serenaDataSync.AcceptRejectUserRequest(upt_AcceptedUPDData.Guid, "1")).Wait();
                                }
                            }

                        }

                        if (UPTTLatestdata.Count() != 0 || UPTT_UnfollowLatestdata.Count() != 0 || UPTT_RejectUPDData.Count() != 0 || UPTT_AcceptedUPDData.Count() != 0)
                        {
                            //Update Last_Out_Time in LastSync Table  for UserProfileDataTran_Out
                            var ls = new LastSync();
                            ls.TableName = "UserProfileDataTran_Out";
                            ls.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(ls);
                        }
                        #endregion

                        #region Create pro Class

                        var uploadMediaModel_Out = App.DBIni.AgeDB.GetSyncDataByTableName("UploadMediaModel_Out");

                        var uploadMediaModel_OutData = App.DBIni.UserProfileDataDB.GetProClass_Out(uploadMediaModel_Out.Out_last_sync);
                        if (uploadMediaModel_OutData.Count() != 0)
                        {
                            foreach (var uploadMediaModel_OutDataDetails in uploadMediaModel_OutData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.SaveProClass(uploadMediaModel_OutDataDetails)).Wait();
                                }
                            }
                            //Update Last_Out_Time in LastSync Table  for UploadMediaModel_Out
                            var lst = new LastSync();
                            lst.TableName = "UploadMediaModel_Out";
                            lst.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(lst);
                        }

                        #endregion

                        #region reviews

                        var reviews_Out = App.DBIni.AgeDB.GetSyncDataByTableName("SerenaReviews_Out");

                        var reviews_OutData = App.DBIni.ReviewsDB.GetAllReviews_Out(reviews_Out.Out_last_sync);
                        if (reviews_OutData.Count() != 0)
                        {
                            foreach (var sreviews_OutDataDetails in reviews_OutData)
                            {
                                if (current == NetworkAccess.Internet)
                                {
                                    Task.Run(() => serenaDataSync.PostSerenaReviewsByGuid_Class(sreviews_OutDataDetails)).Wait();
                                }
                            }
                            //Update Last_Out_Time in LastSync Table  for SerenaReviews_Out
                            var lstReview = new LastSync();
                            lstReview.TableName = "SerenaReviews_Out";
                            lstReview.Out_last_sync = DateTime.Now;
                            App.DBIni.AgeDB.AddLastSync(lstReview);
                        }
                        #endregion

                    }
                }
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                return Task.FromResult(false);
            }

        }


    }
}
