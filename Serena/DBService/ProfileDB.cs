﻿using Serena.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class ProfileDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static readonly object collisionLock = new object(); //amd 2nd feb

        public ProfileDB()
        {
          
                    _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
                    _SQLiteConnection.CreateTable<UserProfile>();

        }

        public IEnumerable<UserProfile> GetUsers()
        {
           
                return (from u in _SQLiteConnection.Table<UserProfile>()
                        select u).ToList();
            
        }
        //Get all users except loggedin user  
        public IEnumerable<UserProfile> GetAllUserProfile(string sessionEmailId)
        {
           

                return (from usr in _SQLiteConnection.Table<UserProfile>()
                        select usr).Where(x => x.UserEmailId != sessionEmailId)
                    .OrderBy(x => x.Id).ToList();
            
        }

        public UserProfile GetSpecificUser(string id)
        {
           

                return _SQLiteConnection.Table<UserProfile>().FirstOrDefault(t => t.UserEmailId == id);
            
        }

        public UserProfile GetSpecificUserByUserName(string UserName)
        {
            
                return _SQLiteConnection.Table<UserProfile>().FirstOrDefault(t => t.UserName == UserName);
            
        }

        public void DeleteUser(string id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Delete<UserProfile>(id);
            }
        }
        public string AddUser(UserProfile profile)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<UserProfile>();
                var d1 = data.Where(x => x.UserEmailId == profile.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(profile);
                    return "Sucessfully Added";
                }
                else
                {
                    profile.Id = d1.Id;
                    updateUsers(profile);
                    return "Already Mail id Exist";

                }
            }
        }

        public bool updateUser(string emailid)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                var data = _SQLiteConnection.Table<UserProfile>();
                var d1 = (from values in data
                          where values.UserEmailId == emailid
                          select values).Single();
                if (true)
                {
                    //d1.PASSWORD = pwd;
                    _SQLiteConnection.Update(d1);
                    return true;
                }
            }
        }
        public bool updateUsers(UserProfile profiledata)
        {
            //var data = _SQLiteConnection.Table<UserProfile>();
            //var d1 = (from values in data
            //          where values.Id == id
            //          select values).Single();
            //if (true)
            //{
            //d1.PASSWORD = pwd;
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _SQLiteConnection.Update(profiledata);
                return true;
            }
            //}
            //else
            //    return false;
        }
    }
}
