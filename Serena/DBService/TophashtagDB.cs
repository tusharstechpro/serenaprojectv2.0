﻿using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Serena.DBService
{
    public class TophashtagDB
    {
        private SQLiteConnection _SQLiteConnection;
        private static object collisionLock = new object(); //amd 2nd feb
        public TophashtagDB()
        {

            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _ = _SQLiteConnection.CreateTable<Tophashtags>();
            _ = _SQLiteConnection.CreateTable<TophashtagsImg>();
            _ = _SQLiteConnection.CreateTable<SearchHashTag>();
            _ = _SQLiteConnection.CreateTable<People>();

        }
        public IEnumerable<Tophashtags> GetTophashtagData()
        {

                return (from u in _SQLiteConnection.Table<Tophashtags>()
                        select u).ToList();
            
        }
     
        public IEnumerable<SearchHashTag> GetSearchTophashtagDataToDelete()
        {

            return (from u in _SQLiteConnection.Table<SearchHashTag>()
                        select u).ToList();
            
        }
        public IEnumerable<SearchHashTag> GetSearchTophashtagData(string hashTags)
        {
            

                var stag = (from u in _SQLiteConnection.Table<SearchHashTag>()
                            select u).ToList();
                stag.Where(x => x.Hashtags.Contains(hashTags) && x.HashType == "A");
                return stag;
            
  
        }
        //public IEnumerable<TophashtagsImg> GetTophashtagimgData()
        //{
        //    return (from u in _SQLiteConnection.Table<TophashtagsImg>()
        //            select u).ToList();
        //}

        public void DeleteTophashtagById(int id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Delete<Tophashtags>(id);
            }
        }
        public void DeleteTophashtag()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.DeleteAll<Tophashtags>();
            }
        }
        public void DeleteSearchTophashtag()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.DeleteAll<SearchHashTag>();
            }
        }
      
        public void DeleteSearchTophashtagById(int id)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                _SQLiteConnection.Delete<SearchHashTag>(id);
            }
        }

        //public void DeleteTophashtagimg(int id)
        //{
        //    _SQLiteConnection.Delete<TophashtagsImg>(id);
        //}
        public string AddTophashtag(Tophashtags top)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {


                var data = _SQLiteConnection.Table<Tophashtags>();
                var d1 = data.Where(x => x.Hashtags == top.Hashtags).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(top);
                    return "Sucessfully Added";
                }
                else
                {
                    updateTophashtag(top);
                    return "hashtag Already Exist";
                }
            }
        }
        public string AddSearchHashTags(SearchHashTag top)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {



                var data = _SQLiteConnection.Table<SearchHashTag>();
                var d1 = data.Where(x => x.Hashtags == top.Hashtags).FirstOrDefault();
                if (d1 == null)
                {
                    _SQLiteConnection.Insert(top);
                    return "Sucessfully Added";
                }
                else
                {
                    updateSearchHashTags(top);
                    return "hashtag Already Exist";
                }
            }
        }
        //public string AddTophashtagimg(TophashtagsImg top)
        //{
        //    var data = _SQLiteConnection.Table<TophashtagsImg>();
        //    var d1 = data.Where(x => x.Guid == top.Guid).FirstOrDefault();
        //    if (d1 == null)
        //    {
        //        _SQLiteConnection.Insert(top);
        //        return "Sucessfully Added";
        //    }
        //    else
        //    {
        //        updateTophashtagimg(top);
        //        return "hashtagimg Already Exist";
        //    }
            
        //}
        public bool updateSearchHashTags(SearchHashTag topData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _ = _SQLiteConnection.Update(topData);
                return true;
            }
        }
        public bool updateTophashtag(Tophashtags topData)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                _ = _SQLiteConnection.Update(topData);
                return true;
            }
        }
   

        #region toppeople

        public IEnumerable<People> GetTopPeopleData()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {

                return (from u in _SQLiteConnection.Table<People>()
                        select u).ToList();
            }
        }
        public void DeleteAllTopPeople()
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                _ = _SQLiteConnection.DeleteAll<People>();
            }
        }
        public string AddTopPeople(People people)
        {
            lock (collisionLock) // amd 2nd feb db lock issue fix
            {
                var data = _SQLiteConnection.Table<People>();
                var d1 = data.Where(x => x.UserEmailId == people.UserEmailId).FirstOrDefault();
                if (d1 == null)
                {
                    _ = _SQLiteConnection.Insert(people);
                    return "Sucessfully Added";
                }
                else
                    return "Sucessfully Added";

            }
        }

        #endregion
    }
}
