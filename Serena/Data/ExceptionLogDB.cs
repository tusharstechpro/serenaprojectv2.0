﻿using Serena.Model;
using Serena.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Serena.DBService
{
    public class ExceptionLogDB
    {
        private SQLiteConnection _SQLiteConnection;
        public ExceptionLogDB()
        {
            _SQLiteConnection = DependencyService.Get<ISQLite>().GetConnection();
            _SQLiteConnection.CreateTable<ExceptionLog>();
        }
        public string AddExceptionLog(ExceptionLog elog)
        {
            var device = DeviceInfo.Model;
            var version = DeviceInfo.VersionString;
            var platform = DeviceInfo.Platform;

            elog.DeviceModel = device;
            elog.DeviceVersion = version;
            elog.OSType = platform.ToString();
            elog.ErrorLogTime= DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            _SQLiteConnection.Insert(elog);
            return "Sucessfully Added";
        }
        public IEnumerable<ExceptionLog> GetExceptionLogData()
        {
            return (from u in _SQLiteConnection.Table<ExceptionLog>()
                    select u).ToList();
        }

    }
}
