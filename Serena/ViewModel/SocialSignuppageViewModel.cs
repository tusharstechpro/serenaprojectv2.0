﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.GoogleClient.Shared;
using Refit;
using Serena.Model;
using Serena.DBService;
using Serena.Views;
using Xamarin.Forms;
using Serena.Data;
using Xamarin.Auth;
using System.Net.Http;
using Serena.Helpers;
using System.Linq;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Plugin.Toast;

namespace Serena.ViewModel
{
    public class SocialSignuppageViewModel
    {
        string LoginType;
        public ICommand OnLoginCommand { get; set; }

        IFacebookClient _facebookService = CrossFacebookClient.Current;
        
        IOAuth2Service _oAuth2Service;

        //For apple signup
        public bool IsAppleSignInAvailable { get { return appleSignInService?.IsAvailable ?? false; } }
        public ICommand SignInWithAppleCommand { get; set; }

        public event EventHandler OnSignIn = delegate { };

        IAppleSignInService appleSignInService;


        //To create UI button for social media SignUp
        public ObservableCollection<AuthNetwork> AuthenticationNetworksiOS { get; set; } = new ObservableCollection<AuthNetwork>()
        {
            new AuthNetwork()
            {
                Name = "Apple",
                Icon = "appleicon",
                Foreground = "Black",
                Background = "#e6f3ff"
            },
            new AuthNetwork()
            {
                Name = "Facebook",
                Icon = "fb",
                Foreground = "White",
                Background = "#0069cc"
            },
             new AuthNetwork()
            {
                Name = "Google",
                Icon = "gmail",
                Foreground = "White",
                Background = "#ff4d4d"
            },
              new AuthNetwork()
            {
                Name = "your email",
                Icon = "Mail",
                Foreground = "White",
                Background ="#9900CC"
            }
        };
        //To create UI button for social media SignUp
        public ObservableCollection<AuthNetwork> AuthenticationNetworksDroid { get; set; } = new ObservableCollection<AuthNetwork>()
        {
            new AuthNetwork()
            {
                Name = "Facebook",
                Icon = "fb",
                Foreground = "White",
                Background = "#0069cc"
            },
             new AuthNetwork()
            {
                Name = "Google",
                Icon = "gmail",
                Foreground = "White",
                Background = "#ff4d4d"
            },
              new AuthNetwork()
            {
                Name = "your email",
                Icon = "Mail",
                Foreground = "White",
                Background ="#9900CC"
            }
        };

        public SocialSignuppageViewModel(IOAuth2Service oAuth2Service)
        {
            _oAuth2Service = oAuth2Service;

            OnLoginCommand = new Command<AuthNetwork>(async (data) => await SignupAsync(data));
            //For apple signup
            if (Device.RuntimePlatform == Device.iOS)
            {
                appleSignInService = DependencyService.Get<IAppleSignInService>();
                OnLoginCommand = new Command(SignUpAppleAsync);
            }
        }
        async Task SignupAsync(AuthNetwork authNetwork)
        {
            //Button click events
            try
            {
                switch (authNetwork.Name)
                {
                    //Sign Up with Apple
                    case "Apple":
                        if (Device.RuntimePlatform == Device.iOS)
                            SignUpAppleAsync();
                        break;
                    //Sign Up with Facebook
                    case "Facebook":
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            await SignupFacebookAsync(authNetwork);
                        }
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            OnFacebookSignupClicked();
                        }

                        break;
                    //Sign Up with Google
                    case "Google":
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            await SignupGoogleAsync(authNetwork);

                        }
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            OnGoogleSignupClicked();
                        }
                        break;
                    //Sign Up with your email
                    case "your email":
                        await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail("", "", "0"));
                        break;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialSignupPageViewModel.cs";
                addlogFile.ExceptionEventName = "SignupAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //SignUpWithApple for iOS
        async void SignUpAppleAsync()
        {
            var account = await appleSignInService.SignInAsync();
            if (account != null)
            {
                Preferences.Set(App.LoggedInKey, true);
                await SecureStorage.SetAsync(App.AppleUserIdKey, account.UserId);
                System.Diagnostics.Debug.WriteLine($"Signed in!\n  Name: {account?.Name ?? string.Empty}\n  Email: {account?.Email ?? string.Empty}\n  UserId: {account?.UserId ?? string.Empty}");
                OnSignIn?.Invoke(this, default(EventArgs));

                LoginType = "3";//For Apple
                //add user to session
                string AppleEmail = account.UserId+ "@serena.com";
                Settings.GeneralSettings = "";
                Settings.GeneralSettings = AppleEmail;
                //Navigate to SignUpEmail Page
                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(AppleEmail, account.Name, LoginType));
            }

        }

        //SignUpWithFb for iOS
        async Task SignupFacebookAsync(AuthNetwork authNetwork)
        {
            try
            {

                if (_facebookService.IsLoggedIn)
                {
                    _facebookService.Logout();
                }

                EventHandler<FBEventArgs<string>> userDataDelegate = null;

                userDataDelegate = async (object sender, FBEventArgs<string> e) =>
                {
                    switch (e.Status)
                    {
                        case FacebookActionStatus.Completed:
                            var facebookProfile = await Task.Run(() => JsonConvert.DeserializeObject<FacebookProfile>(e.Data));
                            var socialLoginData = new NetworkAuthData
                            {
                                Id = facebookProfile.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = facebookProfile.Picture.Data.Url,
                                Name = $"{facebookProfile.FirstName} {facebookProfile.LastName}",
                            };
                            LoginType = "1";//For Facebook
                            //add user to session
                            Settings.GeneralSettings = "";
                            Settings.GeneralSettings = facebookProfile.Email;
                            var fullName = facebookProfile.FirstName + " " + facebookProfile.LastName;
                            //Navigate to SignUpEmail Page
                            await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(facebookProfile.Email, fullName, LoginType));
                            break;
                        case FacebookActionStatus.Canceled:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Canceled", "Ok");
                            break;
                        case FacebookActionStatus.Error:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Error", "Ok");
                            break;
                        case FacebookActionStatus.Unauthorized:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Unauthorized", "Ok");
                            break;
                    }

                    _facebookService.OnUserData -= userDataDelegate;
                };

                _facebookService.OnUserData += userDataDelegate;

                string[] fbRequestFields = { "email", "first_name", "picture", "gender", "last_name" };
                string[] fbPermisions = { "email" };
                await _facebookService.RequestUserDataAsync(fbRequestFields, fbPermisions);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //SignUpWithGmail for iOS
        async Task SignupGoogleAsync(AuthNetwork authNetwork)
        {
            try
            {
                IGoogleClientManager _googleService = CrossGoogleClient.Current;
                if (!string.IsNullOrEmpty(_googleService.AccessToken))
                {
                    //Always require user authentication
                    _googleService.Logout();
                }

                EventHandler<GoogleClientResultEventArgs<GoogleUser>> userLoginDelegate = null;
                userLoginDelegate = async (object sender, GoogleClientResultEventArgs<GoogleUser> e) =>
                {
                    switch (e.Status)
                    {
                        case GoogleActionStatus.Completed:
#if DEBUG
                            var googleUserString = JsonConvert.SerializeObject(e.Data);
                            Debug.WriteLine($"Google Logged in succesfully: {googleUserString}");
#endif

                            var socialLoginData = new NetworkAuthData
                            {
                                Id = e.Data.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = e.Data.Picture.AbsoluteUri,
                                Name = e.Data.Name,
                            };
                            LoginType = "2";//For Gmail
                            //add user to session
                            Settings.GeneralSettings = "";
                            Settings.GeneralSettings = e.Data.Email;
                            //Navigate to SignUpEmail Page
                            await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(e.Data.Email, e.Data.Name, LoginType));
                            break;
                        case GoogleActionStatus.Canceled:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Canceled", "Ok");
                            break;
                        case GoogleActionStatus.Error:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Error", "Ok");
                            break;
                        case GoogleActionStatus.Unauthorized:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Unauthorized", "Ok");
                            break;
                    }

                    _googleService.OnLogin -= userLoginDelegate;
                };

                _googleService.OnLogin += userLoginDelegate;

                await _googleService.LoginAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //SignUpWithGmail for Android
        public void OnGoogleSignupClicked()
        {
            try
            {
                string clientId = string.Empty;
                string redirectUri = string.Empty;

                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        clientId = Constants.GoogleiOSClientId;
                        redirectUri = Constants.GoogleiOSRedirectUrl;
                        break;

                    case Device.Android:
                        clientId = Constants.GoogleAndroidClientId;
                        redirectUri = Constants.GoogleAndroidRedirectUrl;
                        break;
                }
                if (Device.RuntimePlatform == Device.Android)
                {
                    var authenticator = new OAuth2Authenticator(
                    clientId,
                    null,
                    Constants.GoogleScope,
                    new Uri(Constants.GoogleAuthorizeUrl),
                    new Uri(redirectUri),
                    new Uri(Constants.GoogleAccessTokenUrl),
                    null,
                    true);

                    authenticator.Completed += OnAuthCompleted;
                    authenticator.Error += OnAuthError;

                    AuthenticationState.Authenticator = authenticator;

                    var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                    presenter.Login(authenticator);
                }
                else if (Device.RuntimePlatform == Device.iOS)
                {
                    var authenticator = new OAuth2Authenticator(
                    clientId: clientId,
                    clientSecret: string.Empty,
                    scope: Constants.GoogleScope,
                    authorizeUrl: new Uri(Constants.GoogleAuthorizeUrl),
                    redirectUrl: new Uri(redirectUri),
                    accessTokenUrl: new Uri(Constants.GoogleAccessTokenUrl),
                    getUsernameAsync: null,
                    isUsingNativeUI: true
                    );

                    authenticator.Completed += OnAuthCompleted;
                    authenticator.Error += OnAuthError;
                    //authenticator.IsLoadableRedirectUri = true;

                    AuthenticationState.Authenticator = authenticator;

                    var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                    presenter.Login(authenticator);

                }

            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SignUpPage.Xaml";
                addlogFile.ExceptionEventName = "OnGoogleSignupClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //SignUpWithFb for Android
        public void OnFacebookSignupClicked()
        {
            try
            {
                string clientId = string.Empty;
                string redirectUri = string.Empty;

                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        clientId = Constants.FacebookiOSClientId;
                        redirectUri = Constants.FacebookiOSRedirectUrl;
                        break;

                    case Device.Android:
                        clientId = Constants.FacebookAndroidClientId;
                        redirectUri = Constants.FacebookAndroidRedirectUrl;
                        break;
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    var authenticator = new OAuth2Authenticator(
                    clientId,
                    Constants.FacebookScope,
                    new Uri(Constants.FacebookAuthorizeUrl),
                    new Uri(Constants.FacebookAccessTokenUrl),
                    null);

                    authenticator.Completed += OnAuthCompleted;
                    authenticator.Error += OnAuthError;

                    authenticator.IsLoadableRedirectUri = true;

                    AuthenticationState.Authenticator = authenticator;

                    var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                    presenter.Login(authenticator);
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SignUpPage.Xaml";
                addlogFile.ExceptionEventName = "OnFacebookLoginClicked";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //Successful SignUp for Facebook and Gmail for Andoid
        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {

            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }


            if (e.IsAuthenticated)
            {
                if (authenticator.AuthorizeUrl.Host == "www.facebook.com")
                {
                    //this.IsBusy = true;

                    FacebookEmail facebookEmail = null;

                    var httpClient = new HttpClient();

                    // If the user is authenticated, request their basic user data from Facebook
                    var json = await httpClient.GetStringAsync($"https://graph.facebook.com/me?fields=id,name,first_name,last_name,email,picture.type(large)&access_token=" + e.Account.Properties["access_token"]);

                    facebookEmail = JsonConvert.DeserializeObject<FacebookEmail>(json);

                    LoginType = "1";
                    //add user to session
                    Settings.GeneralSettings = "";
                    Settings.GeneralSettings = facebookEmail.Email;
                    //Navigate to SignUpEmail Page
                    //await Navigation.PushAsync(new SignUpEmail(facebookEmail.Email, facebookEmail.Name, LoginType));
                    await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(facebookEmail.Email, facebookEmail.Name, LoginType));
                }
                else
                {
                    //this.IsBusy = true;
                    User user = null;

                    // If the user is authenticated, request their basic user data from Google
                    var request = new OAuth2Request("GET", new Uri(Constants.GoogleUserInfoUrl), null, e.Account);
                    var response = await request.GetResponseAsync();
                    if (response != null)
                    {
                        // Deserialize the data and store it in the account store
                        // The users email address will be used to identify data in SimpleDB
                        string userJson = await response.GetResponseTextAsync();
                        user = JsonConvert.DeserializeObject<User>(userJson);
                    }
                    LoginType = "2";
                    //add user in session
                    Settings.GeneralSettings = "";
                    Settings.GeneralSettings = user.Email;
                    //Navigate to SignUpEmail Page
                    //await Navigation.PushAsync(new SignUpEmail(user.Email, user.Name, LoginType));
                    await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(user.Email, user.Name, LoginType));
                }
            }
        }

        //If error found in Facebook and Gmail SignUp for Android 
        void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            Debug.WriteLine("Authentication error: " + e.Message);
        }

    }
}