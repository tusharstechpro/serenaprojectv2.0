﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Serena
{
    public interface VideoUploadInterface
    {
        string getVideoLength(string url);
        ImageSource GenerateThumbImage(string url, long usecond);
    }
}
