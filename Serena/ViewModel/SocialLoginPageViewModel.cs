﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.GoogleClient.Shared;
using Refit;
using Serena.Model;
using Serena.DBService;
using Serena.Views;
using Xamarin.Forms;
using Serena.Data;
using Xamarin.Auth;
using System.Net.Http;
using Serena.Helpers;
using System.Linq;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Plugin.Toast;

namespace Serena.ViewModels
{
    public class SocialLoginPageViewModel
    {
        SignUpDB signupDB = new SignUpDB();
        public ICommand OnLoginCommand { get; set; }
        IFacebookClient _facebookService = CrossFacebookClient.Current;
        IOAuth2Service _oAuth2Service;
        string LoginType;


        //For apple signIn
        public bool IsAppleSignInAvailable { get { return appleSignInService?.IsAvailable ?? false; } }
        public ICommand SignInWithAppleCommand { get; set; }

        public event EventHandler OnSignIn = delegate { };

        IAppleSignInService appleSignInService;


        //To create UI button for social media login
        public ObservableCollection<AuthNetwork> AuthenticationNetworksiOS { get; set; } = new ObservableCollection<AuthNetwork>()
        {
            new AuthNetwork()
            {
                Name = "Apple",
                Icon = "appleicon.png",
                Foreground = "Black",
                Background = "#e6f3ff"
            },
            new AuthNetwork()
            {
                Name = "Facebook",
                Icon = "fb",
                Foreground = "White",
                Background = "#0069cc"
            },
             new AuthNetwork()
            {
                Name = "Google",
                Icon = "gmail",
                Foreground = "White",
                Background = "#ff4d4d"
            },
              new AuthNetwork()
            {
                Name = "your email",
                Icon = "Mail",
                Foreground = "White",
                Background ="#9900CC"
            }
        };

        //To create UI button for social media login
        public ObservableCollection<AuthNetwork> AuthenticationNetworksDroid { get; set; } = new ObservableCollection<AuthNetwork>()
        {
            new AuthNetwork()
            {
                Name = "Facebook",
                Icon = "fb",
                Foreground = "White",
                Background = "#0069cc"
            },
             new AuthNetwork()
            {
                Name = "Google",
                Icon = "gmail",
                Foreground = "White",
                Background = "#ff4d4d"
            },
              new AuthNetwork()
            {
                Name = "your email",
                Icon = "Mail",
                Foreground = "White",
                Background ="#9900CC"
            }
        };

        public SocialLoginPageViewModel(IOAuth2Service oAuth2Service)
        {
            _oAuth2Service = oAuth2Service;

            OnLoginCommand = new Command<AuthNetwork>(async (data) => await LoginAsync(data));

            //For apple signIn
            if (Device.RuntimePlatform == Device.iOS)
            {
                appleSignInService = DependencyService.Get<IAppleSignInService>();
                SignInWithAppleCommand = new Command(LoginAppleAsync);
            }
        }

      
        async Task LoginAsync(AuthNetwork authNetwork)
        {
            //Button click events
            try
            {
                switch (authNetwork.Name)
                {
                    //Sign In with Apple
                    case "Apple":
                        if (Device.RuntimePlatform == Device.iOS)
                            LoginAppleAsync();
                        break;
                    //Sign In with Facebook
                    case "Facebook":
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            await LoginFacebookAsync(authNetwork);
                        }
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            OnFacebookLoginClicked();
                        }

                        break;
                    //Sign In with Google
                    case "Google":
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            await LoginGoogleAsync(authNetwork);
                        }
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            OnGoogleLoginClicked();
                        }

                        break;
                    //Sign In with your email
                    case "your email":
                        await App.Current.MainPage.Navigation.PushModalAsync(new LoginUser());
                        break;
                }
            }
            catch (Exception ex)
            {
                var addlogFile = new ExceptionLog();
                addlogFile.ExceptionDetails = ex.Message;
                addlogFile.ExceptionCategoryScreenName = "SocialLoginPageViewModel.cs";
                addlogFile.ExceptionEventName = "LoginAsync";

                App.DBIni.LogFileDB.AddExceptionLog(addlogFile);
            }
        }

        //SignInWithApple for iOS
        async void LoginAppleAsync()
        {
            var account = await appleSignInService.SignInAsync();
            if (account != null)
            {
                Preferences.Set(App.LoggedInKey, true);
                await SecureStorage.SetAsync(App.AppleUserIdKey, account.UserId);
                System.Diagnostics.Debug.WriteLine($"Signed in!\n  Name: {account?.Name ?? string.Empty}\n  Email: {account?.Email ?? string.Empty}\n  UserId: {account?.UserId ?? string.Empty}");
                OnSignIn?.Invoke(this, default(EventArgs));
                //Check if user is already registered
                string AppleEmail = account.UserId + "@serena.com";
                var sourcedata = signupDB.GetSpecificUserByEmail(AppleEmail);
                if (sourcedata == null)
                {
                    LoginType = "3";//For Apple
                    await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(AppleEmail, account.Name, LoginType));

                }
                else
                {
                    Settings.GeneralSettings = "";
                    Settings.GeneralSettings = AppleEmail;
                    //Navigate to Todays Page
                    await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                }
            }

        }

        //SignInWithFb for iOS
        async Task LoginFacebookAsync(AuthNetwork authNetwork)
        {
            try
            {

                if (_facebookService.IsLoggedIn)
                {
                    _facebookService.Logout();
                }
                EventHandler<FBEventArgs<string>> userDataDelegate = null;

                userDataDelegate = async (object sender, FBEventArgs<string> e) =>
                {
                    switch (e.Status)
                    {
                        case FacebookActionStatus.Completed:
                            var facebookProfile = await Task.Run(() => JsonConvert.DeserializeObject<FacebookProfile>(e.Data));
                            var socialLoginData = new NetworkAuthData
                            {
                                Id = facebookProfile.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = facebookProfile.Picture.Data.Url,
                                Name = $"{facebookProfile.FirstName} {facebookProfile.LastName}",
                            };
                            var fullName = facebookProfile.FirstName + "_" + facebookProfile.LastName;
                            // Get specific user data from SQLite to check Email is already exists
                            var sourcedata = signupDB.GetSpecificUserByEmail(facebookProfile.Email);
                            if (sourcedata == null)
                            {
                                LoginType = "1";
                                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(facebookProfile.Email, fullName, LoginType));

                            }
                            else
                            {
                                Settings.GeneralSettings = "";
                                Settings.GeneralSettings = facebookProfile.Email;
                                //Navigate to Todays Page
                                await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                            }
                            break;
                        case FacebookActionStatus.Canceled:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Canceled", "Ok");
                            break;
                        case FacebookActionStatus.Error:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Error", "Ok");
                            break;
                        case FacebookActionStatus.Unauthorized:
                            await App.Current.MainPage.DisplayAlert("Facebook Auth", "Unauthorized", "Ok");
                            break;
                    }

                    _facebookService.OnUserData -= userDataDelegate;
                };

                _facebookService.OnUserData += userDataDelegate;

                string[] fbRequestFields = { "email", "first_name", "picture", "gender", "last_name" };
                string[] fbPermisions = { "email" };
                await _facebookService.RequestUserDataAsync(fbRequestFields, fbPermisions);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //SignInWithGmail for iOS
        async Task LoginGoogleAsync(AuthNetwork authNetwork)
        {
            try
            {
                IGoogleClientManager _googleService = CrossGoogleClient.Current;

                if (!string.IsNullOrEmpty(_googleService.AccessToken))
                {
                    //Always require user authentication
                    _googleService.Logout();
                }

                EventHandler<GoogleClientResultEventArgs<GoogleUser>> userLoginDelegate = null;
                userLoginDelegate = async (object sender, GoogleClientResultEventArgs<GoogleUser> e) =>
                {
                    switch (e.Status)
                    {
                        case GoogleActionStatus.Completed:
#if DEBUG
                            var googleUserString = JsonConvert.SerializeObject(e.Data);
                            Debug.WriteLine($"Google Logged in succesfully: {googleUserString}");
#endif

                            var socialLoginData = new NetworkAuthData
                            {
                                Id = e.Data.Id,
                                Logo = authNetwork.Icon,
                                Foreground = authNetwork.Foreground,
                                Background = authNetwork.Background,
                                Picture = e.Data.Picture.AbsoluteUri,
                                Name = e.Data.Name,
                            };

                            //await App.Current.MainPage.Navigation.PushModalAsync(new HomePage(socialLoginData));
                            // Get specific user data from SQLite to check Email is already exists
                            var sourcedata = signupDB.GetSpecificUserByEmail(e.Data.Email);
                            if (sourcedata == null)//if user is new
                            {
                                LoginType = "2";//For Direct Login
                                await App.Current.MainPage.Navigation.PushModalAsync(new SignUpEmail(e.Data.Email, e.Data.Name, LoginType));
                            }
                            else
                            {
                                Settings.GeneralSettings = "";
                                Settings.GeneralSettings = e.Data.Email;
                                //Navigate to Todays Page
                                await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                            }
                            break;
                        case GoogleActionStatus.Canceled:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Canceled", "Ok");
                            break;
                        case GoogleActionStatus.Error:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Error", "Ok");
                            break;
                        case GoogleActionStatus.Unauthorized:
                            await App.Current.MainPage.DisplayAlert("Google Auth", "Unauthorized", "Ok");
                            break;
                    }

                    _googleService.OnLogin -= userLoginDelegate;
                };

                _googleService.OnLogin += userLoginDelegate;

                await _googleService.LoginAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //SignInWithFb for Android
        public void OnFacebookLoginClicked()
        {

            string clientId = string.Empty;
            string redirectUri = string.Empty;


            clientId = Constants.FacebookAndroidClientId;
            redirectUri = Constants.FacebookAndroidRedirectUrl;

            //Retrieve  AppName from  Facebook Developer Console
            //  account = store.FindAccountsForService(Constants.AppName).FirstOrDefault();
            //To check valid User
            var authenticator = new OAuth2Authenticator(
                clientId,
                Constants.FacebookScope,
                new Uri(Constants.FacebookAuthorizeUrl),
                new Uri(Constants.FacebookAccessTokenUrl),
                null);

            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;

            authenticator.IsLoadableRedirectUri = true;

            AuthenticationState.Authenticator = authenticator;

            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);


        }

        //SignInWithGmail for Android
        public void OnGoogleLoginClicked()
        {
            string clientId = string.Empty;
            string redirectUri = string.Empty;


            clientId = Constants.GoogleAndroidClientId;
            redirectUri = Constants.GoogleAndroidRedirectUrl;
            //Retrieve  AppName from  Gmail Developer Console
            //  account = store.FindAccountsForService(Constants.AppName).FirstOrDefault();

            //To check valid User
            var authenticator = new OAuth2Authenticator(
                clientId,
                null,
                Constants.GoogleScope,
                new Uri(Constants.GoogleAuthorizeUrl),
                new Uri(redirectUri),
                new Uri(Constants.GoogleAccessTokenUrl),
                null,
                true);

            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;

            // authenticator.IsLoadableRedirectUri = true;

            AuthenticationState.Authenticator = authenticator;

            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);
        }

        //Successful SignIn for Facebook and Gmail for Android
        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {

            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }


            if (e.IsAuthenticated)
            {
                if (authenticator.AuthorizeUrl.Host == "www.facebook.com")
                {
                    // this.IsBusy = true;
                    FacebookEmail facebookEmail = null;

                    var httpClient = new HttpClient();

                    // If the user is authenticated, request their basic user data from Facebook
                    var json = await httpClient.GetStringAsync($"https://graph.facebook.com/me?fields=id,name,first_name,last_name,email,picture.type(large)&access_token=" + e.Account.Properties["access_token"]);

                    facebookEmail = JsonConvert.DeserializeObject<FacebookEmail>(json);

                    //await store.SaveAsync(account = e.Account, Constants.AppName);

                    Application.Current.Properties.Remove("Id");
                    Application.Current.Properties.Remove("FirstName");
                    Application.Current.Properties.Remove("LastName");
                    Application.Current.Properties.Remove("DisplayName");
                    Application.Current.Properties.Remove("EmailAddress");

                    Application.Current.Properties.Add("Id", facebookEmail.Id);
                    Application.Current.Properties.Add("FirstName", facebookEmail.First_Name);
                    Application.Current.Properties.Add("LastName", facebookEmail.Last_Name);
                    Application.Current.Properties.Add("DisplayName", facebookEmail.Name);
                    Application.Current.Properties.Add("EmailAddress", facebookEmail.Email);

                    // Getspecific user data from SQLite to check Email is already exists
                    //var data = signupDB.GetUsers();
                    var sourcedata = signupDB.GetSpecificUserByEmail(facebookEmail.Email);
                    if (sourcedata == null)
                    {
                        await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());

                    }
                    else
                    {
                        Settings.GeneralSettings = "";
                        Settings.GeneralSettings = facebookEmail.Email;
                        //Navigate to Todays Page
                        await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                    }
                }
                else
                {
                    /// this.IsBusy = true;
                    User user = null;

                    // If the user is authenticated, request their basic user data from Google
                    var request = new OAuth2Request("GET", new Uri(Constants.GoogleUserInfoUrl), null, e.Account);
                    var response = await request.GetResponseAsync();
                    if (response != null)
                    {
                        // Deserialize the data and store it in the account store
                        // The users email address will be used to identify data in SimpleDB
                        string userJson = await response.GetResponseTextAsync();
                        user = JsonConvert.DeserializeObject<User>(userJson);
                    }

                    //if (account != null)
                    //{
                    //    store.Delete(account, Constants.AppName);
                    //}

                    //await store.SaveAsync(account = e.Account, Constants.AppName);

                    Application.Current.Properties.Remove("Id");
                    Application.Current.Properties.Remove("FirstName");
                    Application.Current.Properties.Remove("FamilyName");
                    Application.Current.Properties.Remove("DisplayName");
                    Application.Current.Properties.Remove("EmailAddress");

                    Application.Current.Properties.Add("Id", user.Id);
                    Application.Current.Properties.Add("FirstName", user.GivenName);
                    Application.Current.Properties.Add("FamilyName", user.FamilyName);
                    Application.Current.Properties.Add("DisplayName", user.Name);
                    Application.Current.Properties.Add("EmailAddress", user.Email);

                    // Getspecific user data from SQLite to check Email is already exists
                    //var data = signupDB.GetUsers();
                    var sourcedata = signupDB.GetSpecificUserByEmail(user.Email);
                    if (sourcedata == null)
                    {
                        await App.Current.MainPage.Navigation.PushModalAsync(new SignUpPage());

                    }
                    else
                    {
                        Settings.GeneralSettings = "";
                        Settings.GeneralSettings = user.Email;
                        //Navigate to Todays Page
                        await App.Current.MainPage.Navigation.PushModalAsync(new TodayTabbedPage());
                    }
                }
            }
        }

        //If error found in Facebook and Gmail SignIn for Android
        void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            Debug.WriteLine("Authentication error: " + e.Message);
        }

    }
}
