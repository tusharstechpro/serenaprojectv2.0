﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Serena.Droid;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Droid))]
namespace Serena.Droid
{
    public class SQLite_Droid : ISQLite
    {
        public static SQLiteConnection con;
        public SQLiteConnection GetConnection()
        {
            if(con == null)
            {
                var dbName = "SerenaDB.db";
                var dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
                var path = Path.Combine(dbPath, dbName);
                con = new SQLiteConnection(path);
                return con;
            }
            else
            {
                return con;
            }
        }
    }
}