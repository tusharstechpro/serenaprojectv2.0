﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using CarouselView.FormsPlugin.Android;
using Matcha.BackgroundService.Droid;
using Plugin.CurrentActivity;
using Android.Content;
using Xamarin.Forms;
using Serena.Interface;
using Serena.Droid.LocalNotification;

namespace Serena.Droid
{
    [Activity(Label = "Serena", Icon = "@drawable/PurpleIcon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            //Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjgxNjQwQDMxMzgyZTMxMmUzMGNoR2ZzVGQ0a0R2a0xIa2h6WTlQdndqYXA4YjdMNG1kQkJEMHJGYmE5WTQ9");For older version of syncfusion.xamarin.core
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjgxNzQyQDMxMzgyZTMyMmUzMG9tSFhmYjVqak9DNzlyQnZpNjBVVHJkT1FXdURDdlhsMTIyQVQ4Rk1CQU09");
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

             
            //NuGet Initializations for corrasal view
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            CarouselViewRenderer.Init();
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: false);
            //Xamarians.MediaPlayer.Droid.VideoPlayerRenderer.Init(this);
            Xam.Forms.VideoPlayer.Android.VideoPlayerRenderer.Init();

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

           
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            Xamarin.FormsMaps.Init(this, savedInstanceState);

            global::Xamarin.Auth.Presenters.XamarinAndroid.AuthenticationConfiguration.Init(this, savedInstanceState);
            Syncfusion.XForms.Android.PopupLayout.SfPopupLayoutRenderer.Init();
            BackgroundAggregator.Init(this);

            LoadApplication(new App());
            //To create Notification for android
            CreateNotificationFromIntent(Intent);
        }
        #region Local Notifiaction
        protected override void OnNewIntent(Intent intent)
        {
            try
            {
                CreateNotificationFromIntent(intent);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        void CreateNotificationFromIntent(Intent intent)
        {
            if (intent?.Extras != null)
            {
                try
                {
                    string title = intent.Extras.GetString(AndroidNotificationManager.TitleKey);
                    string message = intent.Extras.GetString(AndroidNotificationManager.MessageKey);

                    DependencyService.Get<INotificationManager>().ReceiveNotification(title, message);
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
    }
}