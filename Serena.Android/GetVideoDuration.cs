﻿using Serena.Droid;
using System;
using Xamarin.Forms;
using Android.Media;
using System.Collections.Generic;
using Android.Graphics;
using System.IO;

[assembly: Dependency(typeof(GetVideoDuration))]
namespace Serena.Droid
{
    public class GetVideoDuration : VideoUploadInterface
    {
        public string getVideoLength(string url)
        {
            var retriever = new MediaMetadataRetriever();
            retriever.SetDataSource(url);
            var length = retriever.ExtractMetadata(MetadataKey.Duration);
            var lengthseconds = Convert.ToInt32(length) / 1000;
            var t = TimeSpan.FromSeconds(lengthseconds);
            var timeformat = t.ToString();
            return timeformat;
        }
        public ImageSource GenerateThumbImage(string url, long usecond)
        {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            //retriever.SetDataSource(url, new Dictionary<string, string>());
            retriever.SetDataSource(url);
            Bitmap bitmap = retriever.GetFrameAtTime(usecond);
            if (bitmap != null)
            {
                MemoryStream stream = new MemoryStream();
                bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                byte[] bitmapData = stream.ToArray();
                return ImageSource.FromStream(() => new MemoryStream(bitmapData));
            }
            return null;
        }
    }
}