﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Serena;
using Serena.Droid;
using Serena.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MyStackLayout), typeof(CustomViewRenderer))]
namespace Serena.Droid
{
    public class CustomViewRenderer : ViewRenderer<MyStackLayout, Android.Views.View>
    {
        public CustomViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<MyStackLayout> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                MyStackLayout layout = e.NewElement as MyStackLayout;
                layout.OnDrawing += NewElement_OnDrawing;
            }
        }

        private void NewElement_OnDrawing(Action<byte[]> action)
        {
            if (this.ViewGroup != null)
            {
                int width = ViewGroup.Width;
                int height = ViewGroup.Height;

                //create and draw the bitmap
                Bitmap bmp = Bitmap.CreateBitmap(width, height, Bitmap.Config.Argb8888);
                Canvas c = new Canvas(bmp);
                ViewGroup.Draw(c);

                MemoryStream stream = new MemoryStream();
                bmp.Compress(Bitmap.CompressFormat.Png, 100, stream);
                byte[] byteArray = stream.ToArray();
                action(byteArray);
            }
        }
    }
}