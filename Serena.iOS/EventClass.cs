﻿using EventKit;

namespace Serena.iOS
{
    public class EventClass
    {
        public static EventClass Current
        {
            get { return current; }
        }
        private static EventClass current;

        public EKEventStore EventStore
        {
            get { return eventStore; }
        }
        protected EKEventStore eventStore;

        static EventClass()
        {
            current = new EventClass();
        }
        protected EventClass()
        {
            eventStore = new EKEventStore();
        }
    }
}