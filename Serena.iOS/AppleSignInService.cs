﻿using System;
using System.Threading.Tasks;
using AuthenticationServices;
using Foundation;
using Serena.iOS.Services;
using Serena.Model;
using Serena.DBService;
using UIKit;
using Serena.iOS;
using Serena.Data;

[assembly: Xamarin.Forms.Dependency(typeof(AppleSignInService))]
namespace Serena.iOS
{
    public class AppleSignInService : NSObject, IAppleSignInService, IASAuthorizationControllerDelegate, IASAuthorizationControllerPresentationContextProviding
    {

        public bool IsAvailable => UIDevice.CurrentDevice.CheckSystemVersion(13, 0);

        TaskCompletionSource<ASAuthorizationAppleIdCredential> tcsCredential;

        public async Task<AppleSignInCredentialState> GetCredentialStateAsync(string userId)
        {
            try
            {
                var appleIdProvider = new ASAuthorizationAppleIdProvider();
                var credentialState = await appleIdProvider.GetCredentialStateAsync(userId);
                switch (credentialState)
                {
                    case ASAuthorizationAppleIdProviderCredentialState.Authorized:
                        // The Apple ID credential is valid.
                        return AppleSignInCredentialState.Authorized;
                    case ASAuthorizationAppleIdProviderCredentialState.Revoked:
                        // The Apple ID credential is revoked.
                        return AppleSignInCredentialState.Revoked;
                    case ASAuthorizationAppleIdProviderCredentialState.NotFound:
                        // No credential was found, so show the sign-in UI.
                        return AppleSignInCredentialState.NotFound;
                    default:
                        return AppleSignInCredentialState.Unknown;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<AppleAccount> SignInAsync()
        {
            try
            {
                var appleIdProvider = new ASAuthorizationAppleIdProvider();
                var request = appleIdProvider.CreateRequest();
                request.RequestedScopes = new[] { ASAuthorizationScope.Email, ASAuthorizationScope.FullName };

                var authorizationController = new ASAuthorizationController(new[] { request });
                authorizationController.Delegate = this;
                authorizationController.PresentationContextProvider = this;
                authorizationController.PerformRequests();

                tcsCredential = new TaskCompletionSource<ASAuthorizationAppleIdCredential>();

                var creds = await tcsCredential.Task;

                if (creds == null)
                    return null;

                var appleAccount = new AppleAccount();
                appleAccount.Token = new NSString(creds.IdentityToken, NSStringEncoding.UTF8).ToString();
                appleAccount.Email = creds.Email;
                appleAccount.UserId = creds.User;
                appleAccount.Name = NSPersonNameComponentsFormatter.GetLocalizedString(creds.FullName, NSPersonNameComponentsFormatterStyle.Default, NSPersonNameComponentsFormatterOptions.Phonetic);
                appleAccount.RealUserStatus = creds.RealUserStatus.ToString();

                return appleAccount;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region IASAuthorizationController Delegate

        [Export("authorizationController:didCompleteWithAuthorization:")]
        public void DidComplete(ASAuthorizationController controller, ASAuthorization authorization)
        {
            try
            {
                var creds = authorization.GetCredential<ASAuthorizationAppleIdCredential>();
                tcsCredential?.TrySetResult(creds);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [Export("authorizationController:didCompleteWithError:")]
        public void DidComplete(ASAuthorizationController controller, NSError error)
        {
            try
            {
                // Handle error
                tcsCredential?.TrySetResult(null);
                Console.WriteLine(error);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region IASAuthorizationControllerPresentation Context Providing

        public UIWindow GetPresentationAnchor(ASAuthorizationController controller)
        {
            try
            {
                return UIApplication.SharedApplication.KeyWindow;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
    }
}