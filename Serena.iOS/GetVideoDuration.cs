﻿using AVFoundation;
using CoreGraphics;
using CoreMedia;
using Foundation;
using Serena.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(GetVideoDuration))]
namespace Serena.iOS
{
   public class GetVideoDuration :VideoUploadInterface
    {
        public string getVideoLength(string url)
        {
            AVAsset avasset = AVAsset.FromUrl((new Foundation.NSUrl(url)));
            var length = avasset.Duration.Seconds.ToString();
            var lengthseconds = Convert.ToInt32(length) / 1000;
            TimeSpan t = TimeSpan.FromSeconds(lengthseconds);
            var timeformat = t.ToString();
            return timeformat.ToString();

        }

        public ImageSource GenerateThumbImage(string url, long usecond)
        {
            AVAssetImageGenerator imageGenerator = new AVAssetImageGenerator(AVAsset.FromUrl((new Foundation.NSUrl(url))));
            imageGenerator.AppliesPreferredTrackTransform = true;
            CMTime actualTime;
            NSError error;
            CGImage cgImage = imageGenerator.CopyCGImageAtTime(new CMTime(usecond, 1000000), out actualTime, out error);
            return ImageSource.FromStream(() => new UIImage(cgImage).AsPNG().AsStream()); ImageSource.FromStream(() => new UIImage(cgImage).AsPNG().AsStream());
        }
    }
}