﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Serena;
using Serena.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Serena.Helpers;

[assembly:ExportRenderer(typeof(MyStackLayout), typeof(CustomViewRenderer))]
namespace Serena.iOS
{
    public class CustomViewRenderer : ViewRenderer<StackLayout, UIView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                MyStackLayout layout = e.NewElement as MyStackLayout;
                layout.OnDrawing += NewElement_OnDrawing;
            }
        }

        private void NewElement_OnDrawing(Action<byte[]> action)
        {
            UIGraphics.BeginImageContext(NativeView.Frame.Size);
            NativeView.DrawViewHierarchy(NativeView.Frame, true);
            var image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            using (var imageData = image.AsPNG())
            {
                var bytes = new byte[imageData.Length];
                System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, bytes, 0, Convert.ToInt32(imageData.Length));
                action(bytes);
            }
        }
    }
}