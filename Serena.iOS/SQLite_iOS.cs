﻿using System;
using System.IO;
using Serena.iOS;
using SQLite;
using Xamarin.Forms;


[assembly: Dependency(typeof(SQLite_iOS))]
namespace Serena.iOS
{
    public class SQLite_iOS : ISQLite
    {
        public static SQLiteConnection conn;
        public SQLiteConnection GetConnection()
        {
            if(conn == null)
            {
                var dbName = "SerenaDB.db";
                // Get an absolute path to the database file
                var databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "SerenaDB.db");
                // var dbPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                //  var path = Path.Combine(databasePath, dbName);
                conn = new SQLiteConnection(databasePath);
                return conn;
            }
            else
            {
                return conn;
            }
           
        }
    }
}